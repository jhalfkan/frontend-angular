import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LosingPasswordRoutingModule } from './losingpassword-routing.module';
import { LosingPasswordComponent } from './losingpassword.component';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    LosingPasswordRoutingModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LosingPasswordComponent]
})
export class LosingPasswordModule { }
