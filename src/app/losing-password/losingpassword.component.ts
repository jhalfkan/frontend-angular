import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from '../_repositories';
import {first, map} from 'rxjs/operators';

@Component({
    selector: 'app-losingpassword',
    templateUrl: './losingpassword.component.html',
    styleUrls: ['./losingpassword.component.scss'],
    animations: [routerTransition()]
})
export class LosingPasswordComponent implements OnInit {
    translate;
    email: string;
    emailsend: boolean = false;
    error: boolean = false;

    constructor(translate: TranslateService, private loginService: LoginService) {
        this.translate = translate;
    //    this.translate.setDefaultLang('en'); // this language will be used as a fallback when a translation isn't found in the current language
    }

    public sendMail() {
        this.emailsend = false;
        console.log('send mail');
console.log({email: this.email});
        this.loginService.losingPassword({ email : this.email }).pipe(first())
            .subscribe(
                data => {

                    if (typeof(data.result) !== 'undefined' && data.result === true) {
                        this.emailsend = true;
                    }
                    else {
                        this.error = true;
                    }

                    console.log(data);

                },
                error => {
                    this.error = true;
                });
    }

    ngOnInit() {}
}
