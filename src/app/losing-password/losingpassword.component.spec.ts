import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { LosingPasswordComponent } from './losingpassword.component'
import { LosingPasswordModule } from './losingpassword.module'

describe('LosingPasswordComponent', () => {
  let component: LosingPasswordComponent
  let fixture: ComponentFixture<LosingPasswordComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        LosingPasswordModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LosingPasswordComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
