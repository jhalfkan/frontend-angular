import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LosingPasswordComponent } from './losingpassword.component';

const routes: Routes = [
    {
        path: '', component: LosingPasswordComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LosingPasswordRoutingModule {
}
