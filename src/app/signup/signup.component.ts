import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Qualif} from '../_models/qualif';
import {QualifService} from '../_repositories';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    providers: [Qualif]
})
export class SignupComponent implements OnInit {
    translate: TranslateService;
    qualifModel;
    qualifService;
    activatedRoute;
    hash;
    alertType = '';
    alert = '';
    public alerts: Array<any> = [];

    constructor(private router: Router, translate: TranslateService, qualifModel: Qualif, qualifService: QualifService, activatedRoute: ActivatedRoute) {
       // document.location.href = environment.REDIRECTION_SUBSCRIBE;
        this.qualifModel = qualifModel;
        this.translate = translate;
        this.qualifService = qualifService;
        this.router = router;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        let hash = '';

        this.activatedRoute.queryParams.subscribe((params: Params) => {
            hash = params['id'];
            this.hash = hash;
        });
    }

    qualification() {
        let qualif = {
            company: this.qualifModel.company,
            maturity: this.qualifModel.maturity,
            externalisation: this.qualifModel.externalisation,
            nb_semelle: this.qualifModel.nb_semelle,
            profil: ((this.qualifModel.profil == 'Autres') ?  this.qualifModel.profilAlter : this.qualifModel.profil),
            free_text: this.qualifModel.freetext

        };
        this.alerts = new Array<any>();

        this.qualifService.createQualif(qualif, this.hash).subscribe((response) => {
            this.alertType = 'success';
            this.alert = this.translate.instant('Vos données ont bien été envoyées');
            setTimeout(() => {
                this.alert = '';
            }, 5000);

            window.location.href = "https://www.gespodo.com/mobilescan";
        }, (error) => {
            this.alertType = 'danger';
            this.alert = this.translate.instant("ERROR: Une erreur s'est produite lors de l'envoi des données.");
            setTimeout(() => {
                this.alert = '';
            }, 5000);
        });
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
