import { Injectable } from "@angular/core";
@Injectable()
export class Pathology {
    id: number;
    name: string;
}