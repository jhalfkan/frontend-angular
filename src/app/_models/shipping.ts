import { Injectable } from "@angular/core";
@Injectable()
export class Shipping {
    id: number;
    name: string;
    isActive: boolean;
    isDeleted: boolean;
}