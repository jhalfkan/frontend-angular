import { Injectable } from "@angular/core";
@Injectable()
export class Recovery {
    id: number;
    name: string;
    price: number;
}