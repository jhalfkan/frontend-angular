import { Injectable } from "@angular/core";
@Injectable()
export class Gabarit {
    id: number;
    name: string;
}