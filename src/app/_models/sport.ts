import { Injectable } from "@angular/core";
@Injectable()
export class Sport {
    id: number;
    name: string;
}