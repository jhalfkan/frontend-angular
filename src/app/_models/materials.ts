import { Injectable } from "@angular/core";
@Injectable()
export class Materials {
    id: number;
    name: string;
    stock: number;
    stockMin: number;
    isPosturo = false;
    isTong = false;
    position: number;
    activated = true;
    groupe: string;
}