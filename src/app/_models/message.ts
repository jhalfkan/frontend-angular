import { Injectable } from "@angular/core";
@Injectable()
export class Message {
    id: number;
    title: string;
    message: string;
    active = false;
    colorPolice = '#000000';
    colorBack = '#ffffff';
}