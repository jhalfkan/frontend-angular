import { Injectable } from "@angular/core";
@Injectable()
export class Status {
    id: number;
    name: string;
}