import { Injectable } from "@angular/core";
@Injectable()
export class FlangesColor {
    id: number;
    name: string;
    activated = true;
}