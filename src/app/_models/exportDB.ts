import { Injectable } from "@angular/core";
@Injectable()
export class ExportDB {
    id: number;
    name: string;
}