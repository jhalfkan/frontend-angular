import { Injectable } from "@angular/core";
@Injectable()
export class File {
    name: string;
    hash: string;
}
