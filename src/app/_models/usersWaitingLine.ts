import { Injectable } from "@angular/core";
@Injectable()
export class UsersWaitingLine {
    hash: string;
    name: string;
    type: string;
    size: string;
    dateInsert: string;
}