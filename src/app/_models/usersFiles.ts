import { Injectable } from "@angular/core";
@Injectable()
export class UsersFiles {
    id: number;
    user_id: number;
    patient_id: number;
    card_data_id: number;
}