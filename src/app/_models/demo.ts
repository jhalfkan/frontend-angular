import { Injectable } from "@angular/core";
@Injectable()
export class Demo {
    id: number;
    name: string;
    surname: string;
    email: string;
    accept_condition: boolean;
}