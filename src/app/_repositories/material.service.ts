import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Materials } from '../_models';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Router} from '@angular/router';
import {Cacheable} from 'ngx-cacheable';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    cache: true
};

@Injectable({ providedIn: 'root' })

export class MaterialService {
    public static readonly BASEURL = '/materials';
    router;

    constructor(private http: HttpClient, routeur: Router) {
        this.router = routeur;
    }

    @Cacheable()
    getAll(): Observable<Materials> {
        let url = environment.URLAPI + MaterialService.BASEURL + '/getAll/';
        return this.http.get<Materials[]>(url)
            .pipe(
                catchError(this.handleError)
            );
    }

    getAllByPosition(): Observable<Materials> {
        let url = environment.URLAPI + MaterialService.BASEURL + '/getAllByPosition/';
        return this.http.get<Materials[]>(url)
            .pipe(
                catchError(this.handleError)
            );
    }

    createMaterial(material: Materials): Observable<Materials> {
        let url = environment.URLAPI + MaterialService.BASEURL + '/add/';
        return this.http.post<Materials>(url, material, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    editMaterial(material: Materials): Observable<Materials> {
        let id = material.id;
        let url = environment.URLAPI + MaterialService.BASEURL + '/edit/' + id;
        return this.http.post<Materials>(url, material, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    getMaterial(id): Observable<Materials> {
        let url = environment.URLAPI + MaterialService.BASEURL + '/get/' + id;
        return this.http.get<Materials>(url)
            .pipe(
                catchError(this.handleError)
            );
    }

    getPositionsMaterial(): Observable<Materials> {
        let url = environment.URLAPI + MaterialService.BASEURL + '/getPositions/';
        return this.http.get<Materials>(url)
            .pipe(
                catchError(this.handleError)
            );
    }

    deleteMaterial(material: Materials): Observable<Materials[]> {
        let id = material.id;
        let url = environment.URLAPI + MaterialService.BASEURL + '/delete/' + id;
        return this.http.delete( url, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
