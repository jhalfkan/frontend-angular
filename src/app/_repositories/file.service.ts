import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { File } from '../_models/file';

@Injectable({ providedIn: 'root' })

export class FileService {
    public static readonly BASEURL = '/files';

    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<File[]>(environment.URLAPI + FileService.BASEURL);
    }

    delete(hash) {
        return this.http.delete(environment.URLAPI + FileService.BASEURL + '/' + hash + '/delete');
    }

    download(hash) {
        return this.http.get(environment.URLAPI + FileService.BASEURL + '/' + hash + '/download', {observe: 'response', responseType: 'blob'});
    }

    urlUpload() {
        return environment.URLAPI + FileService.BASEURL + '/' + 'upload';
    }

    upload(formData) {
        return this.http.post(this.urlUpload(), formData, {reportProgress: true, observe: 'events'});
    }

    attachToCard(hash, hashCard) {
        return this.http.put(environment.URLAPI + FileService.BASEURL + '/' + hash + '/attachtocard', {'card_hash': hashCard});
    }

}
