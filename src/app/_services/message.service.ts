import {Observable, Subject} from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MessagesDisplayService {
    constructor() {
    }

    private _listners = new Subject<any>();

    getListObservers() {
        return this._listners.observers;
    }

    listen(): Observable<any> {
        return this._listners.asObservable();
    }

    filter(filterBy: string, val: any) {
        this._listners.next({key: filterBy, value: val});
    }

}