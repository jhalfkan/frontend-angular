import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationServiceAlert {
    constructor(private http: HttpClient) { }

    addalert($code, $error, $message) {
        var error = {
            'code' : $code,
            'error' : $error,
            'message' : $message
        };

        var listError = null;
        listError = localStorage.getItem('error');

        if(listError == null)
        {
            listError = new Array();
        }
        else{
            listError = JSON.parse(listError);
        }

            listError.push(error);


        localStorage.setItem('error', listError);

    }

    getError() {
        // remove user from local storage to log user out
        localStorage.removeItem('error');
    }
}