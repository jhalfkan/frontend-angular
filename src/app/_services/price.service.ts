import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  private shippings = [];
  private finishings = [];
  private shoeModels = [];
  private materials = [];
  private recoveries = [];
  private selectedShippingId = null;
  private selectedFinishingId = null;
  private selectedShoeModelId = null;
  private selectedMaterialId = null;
  private selectedRecoveryId = null;
  private selectedShoeTypeId = null;
  private footSpecs = [];
  private therapeuticFieldPrices = [];

  constructor() { }

  setTherapeuticFieldsTables(footSpecs, therapeuticFieldPrices) {
    this.footSpecs = footSpecs;
    this.therapeuticFieldPrices = therapeuticFieldPrices;
  }

  setSelectedResources (shippingId, finishingId, shoeModelId, materialId, recoveryId, shoeTypeId) {
    this.selectedShippingId = shippingId;
    this.selectedFinishingId = finishingId;
    this.selectedShoeModelId = shoeModelId;
    this.selectedMaterialId = materialId;
    this.selectedRecoveryId = recoveryId;
    this.selectedShoeTypeId = shoeTypeId;
  }

  setResourceLists (shippings, finishings, shoeModels, materials, recoveries) {
    this.shippings = shippings;
    this.finishings = finishings;
    this.shoeModels = shoeModels;
    this.materials = materials;
    this.recoveries = recoveries;
  }


  findResource(id, list, attribute) {
    if (!list) {
      return null
    }
    for (let i = 0; i < list.length; ++i) {
      if (id == list[i].id) {
        if (list[i][attribute]) {
          return list[i][attribute];
        }
      }
    }
    return null;
  }

  getShippingPrice () {
    return this.findResource(this.selectedShippingId, this.shippings, 'price');
  }

  getShippingName () {
    return this.findResource(this.selectedShippingId, this.shippings, 'name');
  }

  isShippingNeeded () {
    return [1, 3, 4, 6].includes(parseInt(this.selectedShoeTypeId));
  }

  getFinishingPrice () {
    return this.findResource(this.selectedFinishingId, this.finishings, 'price');
  }

  getFinishingName () {
    return this.findResource(this.selectedFinishingId, this.finishings, 'name');
  }

  isFinishingNeeded () {
    return [1, 6].includes(parseInt(this.selectedShoeTypeId));
  }

  getShoeModelPrice () {
    return this.findResource(this.selectedShoeModelId, this.shoeModels, 'price');
  }

  getShoeModelName () {
    return this.findResource(this.selectedShoeModelId, this.shoeModels, 'name');
  }

  getMaterialPrice () {
    return this.findResource(this.selectedMaterialId, this.materials, 'price');
  }

  getMaterialName () {
    return this.findResource(this.selectedMaterialId, this.materials, 'name');
  }

  isMaterialNeeded () {
    return [1, 2, 3, 4, 6].includes(parseInt(this.selectedShoeTypeId))
  }

  getRecoveryPrice () {
    return this.findResource(this.selectedRecoveryId, this.recoveries, 'price');
  }

  getRecoveryName () {
    return this.findResource(this.selectedRecoveryId, this.recoveries, 'name');
  }

  isRecoveryNeeded () {
    if(![1, 2, 5, 6].includes(parseInt(this.selectedShoeTypeId))) {
      return false;
    };
    if ([1, 2].includes(parseInt(this.selectedFinishingId))) {
      return false;
    }
    return true;
  }

  calculatePrice () {
    if (this.getPayingElements().length === 0) {
      return 0;
    }
    const therapeuticFieldsPrice = this.computeTherapeuticFieldsPrice();
    return this.getPayingElements().reduce((sum, item) => sum + item) + therapeuticFieldsPrice;
  }

  getPayingElements () {
    if (this.selectedShoeTypeId == 6) {
      return [
        this.getShoeModelPrice(),
        this.getShippingPrice(),
        this.getFinishingPrice(),
        this.getMaterialPrice(),
        this.getRecoveryPrice()
      ];
    }
    if (this.selectedShoeTypeId == 5) {
      return [this.getShoeModelPrice(), this.getRecoveryPrice()];
    }
    if (this.selectedShoeTypeId == 4) {
      return [this.getShoeModelPrice(), this.getShippingPrice(), this.getMaterialPrice()];
    }
    if (this.selectedShoeTypeId == 3) {
      return [this.getShoeModelPrice(), this.getShippingPrice(), this.getMaterialPrice()];
    }
    if (this.selectedShoeTypeId == 2) {
      return [this.getShoeModelPrice(), this.getMaterialPrice(), this.getRecoveryPrice()];
    }
    if (this.selectedShoeTypeId == 1) {
      return [
        this.getShoeModelPrice(),
        this.getShippingPrice(),
        this.getFinishingPrice(),
        this.getMaterialPrice(),
        this.getRecoveryPrice()
      ];
    }
    return [];
  }

  computeTherapeuticFieldsPrice() {
    let price = 0;
    for (let i = 0; i < this.footSpecs.length; ++i) {
      price += this.getDechargePrice(this.footSpecs[i]);
      price += this.getCompensationPrice(this.footSpecs[i]);
    }
    return price;
  }

  getDechargePrice(footSpec) {
    if (footSpec['dechargeC3'] === 'technical_material_filling') {
        return this.getTechnicalMaterialFillingPrice();
    }
    return 0;
  }

  getCompensationPrice(footSpec) {
    let price = 0;
    if (footSpec['hauteurTalonC3'] === 'amovible') {
        price = this.getAmoviblePrice();
        if (footSpec['hauteurTalonC1'] <= 5) {
            return price;
        }
        return price + this.getCompensationDifferencePrice();
    }
    if (footSpec['hauteurTalonC3'] === 'collee') {
        price = this.getColleePrice();
        if (footSpec['hauteurTalonC1'] <= 5) {
            return price;
        }
        return price + this.getCompensationDifferencePrice();
    }
    return 0;
  }

  getTechnicalMaterialFillingPrice() {
    return this.findPriceByConceptAndValue('decharge_c3', 'technical_material_filling');
  }

  getAmoviblePrice() {
    return this.findPriceByConceptAndValue('hauteur_talon_c3', 'amovible');
  }

  getColleePrice() {
    return this.findPriceByConceptAndValue('hauteur_talon_c3', 'collee');
  }

  getCompensationDifferencePrice() {
    return this.findPriceByConceptAndValue('hauteur_talon_c1', '>5');
  }

  findPriceByConceptAndValue(concept, value) {
    for (let i = 0; i < this.therapeuticFieldPrices.length; ++i) {
      if (
        this.therapeuticFieldPrices[i].concept == concept
        && this.therapeuticFieldPrices[i].concept_value == value
      ) {
        return this.therapeuticFieldPrices[i].price
      }
    }
    return 0;
  }
}
