import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DemoComponent} from './demo.component';
import {DemoRoutingModule} from './demo-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    DemoRoutingModule,
    TranslateModule,
    FormsModule,
    QRCodeModule,
    NgbAlertModule
  ],
  declarations: [DemoComponent]
})
export class DemoModule { }
