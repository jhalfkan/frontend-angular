import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Demo} from '../_models/demo';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {DemoService} from '../_repositories';
import {environment} from '../../environments/environment';
import * as $ from 'jquery';

export class NgxQrCode {
    text: string;
}

@Component({
    selector: 'app-demo',
    templateUrl: './demo.component.html',
    styleUrls: ['./demo.component.scss'],
    providers: [Demo]
})
export class DemoComponent implements OnInit {
    translate: TranslateService;
    demoModel;
    router;
    activatedRoute;
    user;
    hash;
    demoService;
    urlDemoFile: string;
    urlPlaystore: string;
    urlContionGenerales: string;
    public alerts: Array<any> = [];

    @Input() scans;

    public qrdata: string = null;

    constructor(translate: TranslateService, demoModel: Demo, router: Router, activatedRoute: ActivatedRoute, demoService: DemoService) {
        this.translate = translate;
        this.translate.setDefaultLang('fr');

        this.demoModel = demoModel;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.demoService = demoService;
        this.urlDemoFile = environment.DEMO_DOWNLOAD;
        this.urlPlaystore = environment.PLAYSTORE_APP_URL;
        this.urlContionGenerales = environment.URL_CONDITION_GENERAL;
        this.user = new Demo;
    }

    ngOnInit() {
        let hash = '';

        this.activatedRoute.paramMap.subscribe(params => {
            this.hash = params.get("hash");
        });


        if (typeof(this.hash) === 'undefined' || this.hash == null) {
            this.activatedRoute.queryParams.subscribe((params: Params) => {
                hash = params['token'];
                this.hash = hash;
            });
        }

        if (this.router.url.indexOf('login') !== -1) {

     //       window.open(environment.PLAYSTORE_APP_URL, '_blank');

            // For other browsers:
            // Create a link pointing to the ObjectURL containing the blob.
    //        const data = window.URL.createObjectURL(response.body);
     /*       let link = document.createElement('a');
            link.href = environment.PLAYSTORE_APP_URL;
            link.target = "_blank";
            link.download = 'fiche_labo_' + hash + '.pdf';
            document.body.appendChild(link);
            link.click();
      */
            setTimeout(function() {
                console.log( $('#playstoreLink'));
                let myVar = setInterval( () => {document.getElementById("playstoreLink").click(); }, 500);
                setInterval( () => {clearInterval(myVar)}, 500);
                // For Firefox it is necessary to delay revoking the ObjectURL
          //      document.body.removeChild(link);
         //       window.URL.revokeObjectURL(data);
            },  100);
        }

        if (this.hash) {
            this.getUser(this.hash);
        }

        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            if (localStorage.getItem('status') === 'danger') {
                    localStorage.removeItem('status');
                    localStorage.removeItem('message');
                    this.router.navigate(['/demo']);
            }
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

   getFiles(token) {
        return this.demoService.getUserFilesDemo(token).subscribe((response) => {
            console.log(response.datas);
            if (response.datas.length !== 0) {
                this.scans = response.datas;
            }
        });
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    inscription() {
        let user = {
            lastname: this.demoModel.lastname,
            firstname: this.demoModel.firstname,
            email: this.demoModel.email,
            phone: this.demoModel.phone,
            job: ((this.demoModel.job == 'other') ? this.demoModel.jobAlter : this.demoModel.job ),
            country: (this.demoModel.country == 'other') ? this.demoModel.countryAlter : this.demoModel.country,
            accept_condition: this.demoModel.accept_condition
        };
        this.alerts = new Array<any>();

        if (this.demoModel.accept_condition !== true) {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', 'Veuillez accepter les conditions générales');
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            return false;

        }


        this.demoService.createDemo(user).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', 'Inscription réussie: Un email vous a été envoyé.');
            this.ngOnInit();
        }, (error) => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', "Une erreur s'est produite lors de l'envoi des données.");
            if (localStorage.getItem('status')) {
                this.alerts.push({
                    type: localStorage.getItem('status'),
                    message: localStorage.getItem('message')
                });
                if (localStorage.getItem('status') === 'danger') {
                    localStorage.removeItem('status');
                    localStorage.removeItem('message');
                    this.router.navigate(['/demo']);
                }
                localStorage.removeItem('status');
                localStorage.removeItem('message');
            }
        });
    }

    getUser(hash) {
        return this.demoService.getUserDemo(hash).subscribe((response) => {
            this.user = {
                lastname: response.user.lastname,
                firstname: response.user.firstname,
                hash: response.user.hash,
                token: response.token,
                code: response.user.code,
                demo: true
            };

            this.qrdata = JSON.stringify(this.user);

            this.getFiles(this.user.token);
            setInterval($.proxy(function () {
                this.getFiles(this.user.token);
            }, this), 10000);
        });
    }
}
