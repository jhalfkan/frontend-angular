import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';

import { AuthenticationService } from '../_services';
import * as $ from 'jquery';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    translate;

    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    password = '';
    email = '';

    constructor(public router: Router, private formBuilder: FormBuilder, private route: ActivatedRoute,
                private authenticationService: AuthenticationService, translate: TranslateService) {
        this.translate = translate;
        this.translate.addLangs(['en', 'fr']);
        this.translate.setDefaultLang('fr');
    }

    ngOnInit() {
        this.authenticationService.logout();
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

        $('form').find('input').keypress((e) => {
            if((e.which == 10 || e.which == 13) && ($('#email').val() !== '' && $('#password').val() !== '')) {
                this.onLoggedin();
            }
        });
    }

    onLoggedin() {
        this.submitted = false;
        this.loading = true;
        this.authenticationService.isLoginIn = true;
        this.authenticationService.login(this.email, this.password).subscribe(
            data => {
                this.authenticationService.isLoginIn = false;
                let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                if (!(currentUser && currentUser.token)) {
                    this.error = 'bad identifiant';
                    this.loading = false;
                }
                else{
                    this.router.navigate([this.returnUrl]);
                }
            },
            error => {
                this.authenticationService.isLoginIn = false;
                if (error.status === 403) {
                    this.error = 'bad identifiant';
                } else {
                    this.error = 'error server';
                }
                this.loading = false;
            });

    }
}
