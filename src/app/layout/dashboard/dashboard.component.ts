import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import {UserService, CardService, UsersCardsService} from '../../_repositories';
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService} from '../../_services';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    public totalUserActive: number = 0;
    public totalCardNotClosed: number = 0;
    public totalCardClosed: number = 0;
    public totalCardCad: number = 0;
    public totalCardCam: number = 0;
    public totalCardSended: number = 0;
    public totalCardBilling: number = 0;
    public totalUsersCardNotClosed: number = 0;
    public totalUsersCardClosed: number = 0;
    public totalUsersCardCad: number = 0;
    public totalUsersCardCam: number = 0;
    public totalUsersCardSended: number = 0;
    public totalUsersCardBilling: number = 0;
    userRoles: Array<string> = [];
    translate: TranslateService;

    constructor(private userService: UserService, private cardService: CardService, private usersCardsService: UsersCardsService, translate: TranslateService, authentificat: AuthenticationService) {
        this.translate = translate;
        this.userRoles = authentificat.getRoles();
    /*    this.translate.get('welcome_to_the_new_version_of_gespodo').subscribe((translated: string) => {
            this.alerts.push(
                {
                    id: 1,
                    type: 'success',
                    message: translated
                },
            );
        });
   */
    }

    ngOnInit() {
        this.userService.getCountActive().pipe(first()).subscribe(data => {
            if(data.result == true && typeof(data.datas.totalActive) !== 'undefined'){
                this.totalUserActive = data.datas.totalActive;
            }
        });

        this.cardService.getCountBystatus().pipe(first()).subscribe(data => {
            if(data.result == true && typeof(data.datas) !== 'undefined'){
                data.datas.forEach(element => {
                    switch (element.statut_id) {
                        case 1 :
                            this.totalCardNotClosed = element.total;
                            break;
                        case 2 :
                            this.totalCardClosed = element.total;
                            break;
                        case 3 :
                            this.totalCardCad = element.total;
                            break;
                        case 4 :
                            this.totalCardSended = element.total;
                            break;
                        case 5 :
                            this.totalCardBilling = element.total;
                            break;
                        case 6 :
                            this.totalCardCam = element.total;
                            break;
                    }
                });

            }
        });

        this.usersCardsService.getCountBystatus().pipe(first()).subscribe(data => {
            if(data.result == true && typeof(data.datas) !== 'undefined'){
                data.datas.forEach(element => {
                    switch (element.statut_id) {
                        case 1 :
                            this.totalUsersCardNotClosed = element.total;
                            break;
                        case 2 :
                            this.totalUsersCardClosed = element.total;
                            break;
                        case 3 :
                            this.totalUsersCardCad = element.total;
                            break;
                        case 4 :
                            this.totalUsersCardSended = element.total;
                            break;
                        case 5 :
                            this.totalUsersCardBilling = element.total;
                            break;
                        case 6 :
                            this.totalUsersCardCam = element.total;
                            break;
                    }
                });

            }
        });

    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
