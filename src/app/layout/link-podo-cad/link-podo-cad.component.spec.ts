import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkPodoCadComponent } from './link-podo-cad.component';

describe('LinkPodoCadComponent', () => {
  let component: LinkPodoCadComponent;
  let fixture: ComponentFixture<LinkPodoCadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkPodoCadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkPodoCadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
