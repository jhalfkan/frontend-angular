import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_repositories';
import { CardService } from '../../_repositories';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-link-podo-cad',
  templateUrl: './link-podo-cad.component.html',
  styleUrls: ['./link-podo-cad.component.scss']
})
export class LinkPodoCadComponent implements OnInit {

  public isLoading = true;
  public selectedHash = null;
  public card = null;
  public cardHash = null;
  public hasError = false;
  public cads = [];
  public isLinkLoading = false;
  public success = false;
  constructor(
    private userService: UserService,
    private cardService: CardService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.cardHash = this.route.snapshot.paramMap.get("hash");
    this.fetchCard();
  }

  fetchCard() {
    this.cardService.getCard(this.cardHash).subscribe(
      response => {
        this.card = response;
        this.selectedHash = response.podoCad;
        this.fetchPodoCad();
      },
      error => {
        this.isLoading = false;
        this.hasError = true;
      }
    );
  }

  fetchPodoCad() {
    this.userService.getPodoCad().subscribe(
      response => {
        this.cads = response.datas;
        this.isLoading = false;
      },
      error => {
        this.hasError = true;
        this.isLoading = false;
      }
    )
  }

  link() {
    if (!this.selectedHash || this.isLinkLoading) {
      return;
    }
    this.isLinkLoading = true;
    this.cardService.linkPodoCad(this.cardHash, this.selectedHash).subscribe(
      () => {
        this.success = true;
        setTimeout(() => {
          this.success = false;
        }, 5000);
        this.isLinkLoading = false;
      },
      () => {
        this.isLinkLoading = false;
        setTimeout(() => {
          this.hasError = false;
        }, 5000);
        this.hasError = true;
      }
    )
  }

  navigateBack() {
    this.router.navigate(['/cards']);
  }

}
