import { ShippingsModule } from './shippings.module';

describe('ShippingsModule', () => {
    let shippingModule: ShippingsModule;

    beforeEach(() => {
        shippingModule = new ShippingsModule();
    });

    it('should create an instance', () => {
        expect(shippingModule).toBeTruthy();
    });
});
