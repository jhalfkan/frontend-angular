import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ShippingService} from '../../../_repositories';

@Component({
    selector: 'app-messages',
    templateUrl: './shippings.component.html',
    styleUrls: ['./shippings.component.scss']
})
export class ShippingsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = ShippingService.BASEURL + '/';
        this.heading = new Array('id', 'name', 'isActive', 'price');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'shippings/shipping/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'shippings/shipping/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
