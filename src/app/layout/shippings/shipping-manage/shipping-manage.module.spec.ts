import { ShippingManageModule } from './shipping-manage.module';

describe('ShippingManageModule', () => {
  let shippingManageModule: ShippingManageModule;

  beforeEach(() => {
    shippingManageModule = new ShippingManageModule();
  });

  it('should create an instance', () => {
    expect(shippingManageModule).toBeTruthy();
  });
});
