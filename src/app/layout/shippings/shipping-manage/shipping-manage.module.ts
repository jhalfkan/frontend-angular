import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { ShippingManageRoutingModule } from './shipping-manage-routing.module';
import { ShippingManageComponent } from './shipping-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, ShippingManageRoutingModule, ReactiveFormsModule, FormsModule, UiSwitchModule.forRoot({
          size: 'medium',
          checkedLabel: 'oui',
          uncheckedLabel: 'non'
      })
  ],
  declarations: [ShippingManageComponent]
})
export class ShippingManageModule { }





