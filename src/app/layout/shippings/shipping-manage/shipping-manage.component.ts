import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ShippingService} from '../../../_repositories';
import {Shipping} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-shipping-manage',
    templateUrl: './shipping-manage.component.html',
    styleUrls: ['./shipping-manage.component.scss'],
    providers: [Shipping]
})
export class ShippingManageComponent implements OnInit {
    translate;
    baseURl;
    shippingService;
    shippingModel;
    router;
    activatedRoute;
    status: string;
    deleteAction;
    idShipping;
    shipping = new Shipping();
    nbreItem;
    count;

    constructor(translate: TranslateService, shipping: ShippingService, shippingModel: Shipping, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.shippingService = shipping;
        this.shippingModel = shippingModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idShipping = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (this.idShipping !== undefined) {
            this.shippingService.getShipping(this.idShipping).subscribe((response) => {
                this.shipping = response;
            });
        }
    }

    navigateBack() {
        this.router.navigate(['/shippings']);
    }

    gestionShipping(shipping) {
        if (typeof(shipping.id) === 'undefined') {
            this.shippingService.createShipping(shipping).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/shippings']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/shippings']);
            });
        } else {
            this.shippingService.editShipping(shipping).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/shippings']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/shippings']);
            });
        }
    }

    delete() {
        this.shippingService.deleteShipping(this.shipping).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/shippings']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/shippings']);
        });
    }
}
