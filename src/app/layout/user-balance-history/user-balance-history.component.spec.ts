import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBalanceHistoryComponent } from './user-balance-history.component';

describe('UserBalanceHistoryComponent', () => {
  let component: UserBalanceHistoryComponent;
  let fixture: ComponentFixture<UserBalanceHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBalanceHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBalanceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
