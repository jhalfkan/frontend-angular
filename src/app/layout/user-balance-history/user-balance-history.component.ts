import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-balance-history',
  templateUrl: './user-balance-history.component.html',
  styleUrls: ['./user-balance-history.component.scss']
})
export class UserBalanceHistoryComponent implements OnInit {
  public baseURl = 'balance-history';
  public heading = ['id', 'podo', 'amount', 'createdAt', 'cardOrBill', 'other'];
  public hash: string = 'test';
  constructor(private datePipe: DatePipe, private translateService: TranslateService, private router: ActivatedRoute) {
    this.router = router
  }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.hash = params['hash'];
    });
  }

  public formatDate = (balanceHistoryLines) => {
    for (let i = 0; i < balanceHistoryLines.length; ++i) {
      balanceHistoryLines[i].createdAt = this.datePipe.transform(
        balanceHistoryLines[i].createdAt, 'dd/MM/yyyy HH:mm'
      );
      balanceHistoryLines[i].cardOrBill = this.populateCardIdOrBill(balanceHistoryLines[i])
      balanceHistoryLines[i].amount = this.addAmountSign(balanceHistoryLines[i].amount)
      balanceHistoryLines[i].podo = balanceHistoryLines[i].firstname + " " + balanceHistoryLines[i].lastname
    }
    return balanceHistoryLines
  }

  public populateCardIdOrBill (balanceHistoryLine) {
    if (balanceHistoryLine.cardId) {
      return this.translateService.instant('cardId') + ': ' + balanceHistoryLine.cardId
    }
    return balanceHistoryLine.bill
  }

  public addAmountSign (value) {
    if (value < 0) {
      return value
    }
    return "+ " + value
  }

  public getModel () {
    return 'balance-history/' + this.hash
  }

}
