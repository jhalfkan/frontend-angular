import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CardService, FileService, ReportService, UserService} from '../../../_repositories';
import {Files, ListSelectField, Report} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as $ from 'jquery';
import {CardReportService} from '../../../_services/cardReportService';

@Component({
    selector: 'app-report-manage',
    templateUrl: './report-manage.component.html',
    styleUrls: ['./report-manage.component.scss'],
    providers: [Report]
})
export class ReportManageComponent implements OnInit {
    enabled = 1;
    translate;
    baseURl;
    reportModel;
    router;
    activatedRoute;
    hashReport;
    deleteAction = false;
    addressesType;
    report: Report = new Report();
    list_element_denomination: Array<ListSelectField> = new Array<ListSelectField>();
    listFiles: Array<Files> = new Array<Files>();
    listFilesnew: Array<Files> = new Array<Files>();
    listFilesReport: Array<any> = new Array<any>();
    statusText: string;

    constructor(translate: TranslateService, private _cardReportService: CardReportService, private fileService: FileService, private reportService: ReportService, private cardService: CardService, private userService: UserService, reportModel: Report, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.reportModel = reportModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;

     //   if(this._cardReportService.getListObservers().length === 0) {
            this._cardReportService.listen().subscribe((m: any) => {
                if (m.key === 'addValuesInGreetings') {
                    this.addValuesInGreetings(m.value);
                } else if (m.key === 'addValuesInConclusion') {
                    this.addValuesInConclusion(m.value);
                }
            });
     //   }
    }

    ngOnInit() {
        console.log(this.report);
        this.activatedRoute.params.subscribe((params: Params) => {
            const hash = params['hash'];
            this.hashReport = hash;

            this.reportService.get(this.hashReport).subscribe((response) => {
                if (typeof(response.result) !== 'undefined' && response.result) {
                    this.report = { ...this.report, ...response.data};

                    if (typeof(response['data']['card_report_email_patient']) === 'undefined' && typeof(response['data']['patient_email']) !== 'undefined') {
                        this.report.card_report_email_patient = response['data']['patient_email'];
                    }


                    // >>> LISTE cahier des charges
                    if (this.report.shoe_type_name !== 'Semmelles Posturo') {
                        this.list_element_denomination = [
                            new ListSelectField( 'largeur',   'Largeur', [],  'card_report_field_largeur' , false),
                            new ListSelectField( 'detorsion',   'Détorsion', [],  'card_report_field_detorsion' , false),
                            new ListSelectField( 'bardePronatrice',   'Bande pronatrice', [],  'card_report_field_bardePronatrice', false),
                            new ListSelectField( 'axe',   'Axe de détorsion', [],  'card_report_field_axe' , false),
                            new ListSelectField( 'courbure',   'Sommet courbure', [],  'card_report_field_courbure' , false),
                            new ListSelectField( 'hemicoupole',   'Hauteur hémicoupole', [],  'card_report_field_hemicoupole' , false),
                            new ListSelectField( 'cuvette',   'Hauteur cuvettes',  ['hauteurCuvettes'],   'card_report_field_cuvette' , false),
                            new ListSelectField( 'epaisseur',   'Epaisseur semelles',   [], 'card_report_field_epaisseur' , false),
                            new ListSelectField( 'talon',   'Compensation',  ['hauteurTalonC1'],   'card_report_field_talon' , false),
                            new ListSelectField( 'pronateur',   'Coin pronateur',  ['coinPronateur'],   'card_report_field_pronateur' , false),
                            new ListSelectField( 'supinateur',   'Coin supinateur',  ['coinSupinateur'],   'card_report_field_supinateur' , false),
                            new ListSelectField( 'cuboidien',   'Sous-cuboïdien',  ['coinSousCuboidien', 'coinSousCuboidien'],   'card_report_field_cuboidien' , false),
                            new ListSelectField( 'brc',   'Barre rétro-capitale', [],  'card_report_field_brc' , false),
                            new ListSelectField( 'brc23Ext',   'Barre rétro-captiale 2/3 ext', [],  'card_report_field_brc23Ext' , false),
                            new ListSelectField( 'extMorton',   'Extension de morton', [],  'card_report_field_extMorton' , false),
                            new ListSelectField( 'brc23Int',   'Barre rétro-captiale 2/3 int', [],  'card_report_field_brc23Int' , false),
                            new ListSelectField( 'appuiRC',   'Appui rétro-capitale',  ['arcC1'],   'card_report_field_appuiRC' , false),
                            new ListSelectField( 'sousAC',   'Sous antéro-capitale',  ['sacC1'],   'card_report_field_sousAC' , false),
                            new ListSelectField( 'diaphysaire',   'Sous diaphysaire',  ['sousDia'],   'card_report_field_diaphysaire' , false),
                            new ListSelectField( 'ruler',   'Ruler', [],  'card_report_field_ruler' , false),
                            new ListSelectField( 'posting',   'Posting', [],  'card_report_field_posting' , false),
                            new ListSelectField( 'firstRayon',   '1er rayon',  ['rayon'],   'card_report_field_firstRayon' , false),
                            new ListSelectField( 'decharge',   'Décharge',  ['dechargeC1'],   'card_report_field_decharge' , false),
                            new ListSelectField( 'cluffy',   'Cluffy Wedge', [],  'card_report_field_cluffy' , false),
                            new ListSelectField( 'arrPied',   'Arr-Pied',  ['arrPiedC1'],   'card_report_field_arrPied' , false),
                            new ListSelectField( 'avPied',   'Av-Pied',  ['avPiedC1', 'avPiedC2'],   'card_report_field_avPied' , false)
                        ];
                    }
                    // ####################

                    if (this.report.shoe_type_name === 'Sandales') {
                        this.list_element_denomination.slice(0, 1);
                       // unset(list_element_denomination['cx_largeur']);
                    }


                    // >>> Liste ELEMENT POSTURO <<<
                    const list_element_denomination_element_posturo = [
                        new ListSelectField( 'posturoBa',   'Posturo BA', [],  'card_report_field_posturoBa' , false),
                        new ListSelectField( 'posturoBi',   'Posturo BI', [],  'card_report_field_posturoBi' , false),
                        new ListSelectField( 'posturoBpi',   'Posturo BPI', [],  'card_report_field_posturoBpi' , false),
                        new ListSelectField( 'posturoEai',   'Posturo EAI', [],  'card_report_field_posturoEai' , false),
                        new ListSelectField( 'posturoEca',   'Posturo ECA', [],  'card_report_field_posturoEca' , false),
                        new ListSelectField( 'posturoEmi',   'Posturo EMI', [],  'card_report_field_posturoEmi' , false),
                        new ListSelectField( 'posturoEpi',   'Posturo EPI', [],  'card_report_field_posturoEpi' , false),
                    ];

                    this.list_element_denomination = this.list_element_denomination.concat(list_element_denomination_element_posturo);
                    // ####################

                    // >>liste EVALUATION <<<
                    const list_element_denomination_element_eva = [new ListSelectField( 'painlevel_1',   'EVA T0', [],  'painlevel_1' , false)];
                    this.list_element_denomination = this.list_element_denomination.concat(list_element_denomination_element_eva);
                    // ####################

                    this.userService.getFields(this.report.user_hash).subscribe(response => {
                            if (typeof(response.result) !== 'undefined' && response.result) {
                                let userField = response.data;

                                    Object.keys(userField).forEach(key => {
                                        if (userField[key] === true) {  // pas désactiver dans le profil utilisateur

                                            if (this.getNameFromListElementDenomination(key).length && typeof(this.getNameFromListElementDenomination(key)[0].references) !== 'undefined') { // groupe de champs
                                                 $(this.getNameFromListElementDenomination(key)[0].references).each( (index, element) => {

                                                  if ((typeof(this.report['foot_left_' + element]) === 'undefined' || this.report['foot_left_' + element] === '') && typeof(this.report['foot_right_' + element]) === 'undefined' || this.report['foot_right_' + element] === '') {
                                                        this.list_element_denomination.splice(this.list_element_denomination.indexOf(this.getNameFromListElementDenomination(key)[0]), 1);
                                                      return;
                                                    }
                                                });

                                            }
                                            else {   // champs unique

                                                if ((typeof(this.report['foot_left_' + key]) !== 'undefined' || typeof(this.report['foot_right_' + key]) !== 'undefined') && (this.report['foot_left_' + key] === '' && this.report['foot_right_' + key] === '') ) {
                                                    this.list_element_denomination.splice(this.list_element_denomination.indexOf(this.getNameFromListElementDenomination(key)[0]), 1);
                                                }
                                                else if ((typeof(this.report['card_data_' + key]) !== 'undefined') && (this.report['card_data_' + key] === '')) {
                                                    this.list_element_denomination.splice(this.list_element_denomination.indexOf(this.getNameFromListElementDenomination(key)[0]), 1);
                                                }
                                            }

                                            // >>valeur du champs<<<
                                            let value = ((typeof(this.report['card_report_field_' + key]) !== 'undefined') ? this.report['card_report_field_' + key] : true);


                                            if (typeof(this.getNameFromListElementDenomination(key)[0]) !== 'undefined') {
                                                this.list_element_denomination[this.list_element_denomination.indexOf(this.getNameFromListElementDenomination(key)[0])].value = value;
                                            }
                                            // ######################
                                        }
                                        else {   // désactiver par l'utilisateur dans son profil
                                            if (typeof(this.getNameFromListElementDenomination(key)) !== 'undefined') {
                                                this.list_element_denomination.splice(this.list_element_denomination.indexOf(this.getNameFromListElementDenomination(key)[0]), 1);
                                            }
                                        }
                                    });
                        }
                        console.log(this.list_element_denomination);
                    });
                }

                    this.cardService.getFiles(this.hashReport).subscribe(value => {
                        if (typeof(value.result) !== 'undefined' && value.result) {
                            this.listFiles = value.data;

                            this.reportService.getFiles(this.hashReport).subscribe(value => {
                                if(typeof(value.result) !== 'undefined' && value.result) {
                                this.listFilesReport = value.data;
                                    $(this.listFilesReport).each((index, element) => {
                                        if (element.cards_reports_files_type_name == 'Videos') {
                                            this.listFiles[this.listFiles.indexOf(this.findFileByFilesId(element.files_id)[0])].cards_reports_files_note_videos = element.cards_reports_files_note;
                                            this.listFiles[this.listFiles.indexOf(this.findFileByFilesId(element.files_id)[0])].cards_reports_files_videos_enabled = true;
                                        }
                                        else if (element.cards_reports_files_type_name == 'Files') {
                                            this.listFiles[this.listFiles.indexOf(this.findFileByFilesId(element.files_id)[0])].cards_reports_files_note_files = element.cards_reports_files_note;
                                            this.listFiles[this.listFiles.indexOf(this.findFileByFilesId(element.files_id)[0])].cards_reports_files_files_enabled = true;
                                        }
                                        // cards_reports_files_videos_enabled
                                    });
                                }
                            });
                        }


                    });



            });
        });
    }

    download(hash, fileName) {
        this.fileService.download(hash).subscribe(response => {
            if (response.ok) {
          //      var file = new Blob([response.body]);
                const data = window.URL.createObjectURL(response.body);
                var link = document.createElement('a');
                link.href = data;
                link.target = "_blank";
                link.download = fileName;
                document.body.appendChild(link);
                link.click();
                setTimeout(function() {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    document.body.removeChild(link);
                    window.URL.revokeObjectURL(data);

                    document.body.style.cursor = 'default';
                    $('.downloadFile').css('cursor', 'pointer');
                },  100);
                document.body.style.cursor = 'default';
                $('.btn').css('cursor', 'pointer');
            }

        }, error => {
            document.body.style.cursor = 'default';
            $('.downloadFile').css('cursor', 'pointer');
        });
    }

    save(showPdf = false, sendMail = false) {
        this.report.listFiles = this.listFiles;

        this.list_element_denomination.forEach((value, index) => {
            this.report[value.source] = value.value;
        });

        this.translate.get('Save in process').subscribe((trans) => {
            this.statusText = trans;
        });
        this.reportService.edit(this.hashReport, this.report).subscribe(value => {
            this.translate.get('Report Saved').subscribe((trans) => {
                this.statusText = trans;

                if (sendMail) {
                    this.sendMail();
                }
                if (showPdf) {
                    this.generate_report();
                }
            });
        }, error1 => {
            this.translate.get('error during process').subscribe((trans) => {
                this.statusText = trans;
            });
        });



    }

    sendMail() {
        this.translate.get('Send mail report in process').subscribe((trans) => {
            this.statusText = trans;
        });

        document.body.style.cursor = 'progress';
        $('.btn').css('cursor', 'progress');
        this.reportService.sendMail(this.hashReport).subscribe(response => {
            console.log(response);
            if (typeof(response.result) !== 'undefined' && response.result) {
                if (typeof(response.data) !== 'undefined' &&  typeof(response.data.success) !== 'undefined' &&  response.data.success) {
                    this.translate.get('The mail is sended at').subscribe((trans) => {
                        this.statusText = trans + ' ' + response.data.message + '.';
                    });
                } else {
                    this.translate.get('No email selected').subscribe((trans) => {
                        this.statusText = trans;
                    });
                }
            }

                document.body.style.cursor = 'default';
                $('.btn').css('cursor', 'pointer');
            },  error => {
                document.body.style.cursor = 'default';
                $('.btn').css('cursor', 'pointer');

                this.translate.get('error during process').subscribe((trans) => {
                    this.statusText = trans;
                });
            });
    }

    findFileByFilesId(files_id) {
        return this.listFiles.filter(
            function(data) { return data.files_id == files_id }
        );
    }

    getNameFromListElementDenomination(name) {
        return this.list_element_denomination.filter(
            function(data){ return data.name == name }
        );
    }

    navigateBack() {
        if (this.router.url.indexOf('users') !== -1) {
            this.router.navigate(['/users/cards']);
        } else {
            this.router.navigate(['/cards']);
        }
    }

    generate_report() {
        document.body.style.cursor = 'progress';
        $('.btn').css('cursor', 'progress');

        this.cardService.downloadResourceReport(this.hashReport).subscribe(response => {
            if(response.ok){
                var file = new Blob([response.body], {type: 'application/pdf'});
                let downloadUrl = URL.createObjectURL(response.body);
                window.open(downloadUrl);
                document.body.style.cursor = 'default';
                $('.btn').css('cursor', 'pointer');
            }

        }, error => {
            document.body.style.cursor = 'default';
            $('.downloadFile').css('cursor', 'pointer');
        });
    }

    addValuesInGreetings(value) {
        this.report.greetings +=  value.phrase;
    }

    addValuesInConclusion(value) {
        this.report.conclusion += value.phrase;
    }

    delete() {
    }

}
