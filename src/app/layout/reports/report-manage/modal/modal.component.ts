import {Component, Input} from '@angular/core';
import {NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {ReportService} from '../../../../_repositories';
import * as $ from 'jquery';
import {ModalUndoService} from '../../../../_services/modal/modalUndoService';
import {CardReportService} from '../../../../_services/cardReportService';
import {Phrases} from '../../../../_models';


@Component({
    selector: 'app-modal-conclusion',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
    @Input() type: string;
    closeResult: string;
    paramQuery: any;    // parameter from url
    listData: Array<Phrases> = Array<Phrases>();
    instanceModal;

    constructor(private modalService: NgbModal, private  reportService: ReportService, private _cardReportService: CardReportService) {

    }

    ngOnInit() {
        this.refresh();
    }


    add() {
        let phrase = '';

        $(this.listData).each((index, element) => {
            if (typeof(element['selected']) !== 'undefined' && typeof(element['phrase']) !== 'undefined' &&  element['selected']) {
                phrase += element['phrase'] + '\n';
            }
        });

        if (this.type === 'conclusions') {
            this._cardReportService.filter('addValuesInConclusion', {'phrase' : phrase});
        } else if (this.type === 'greetings') {
            this._cardReportService.filter('addValuesInGreetings', {'phrase' : phrase});
        }
    }

    open(content) {
        $(this.listData).each((index, element) => {
            element['selected'] = false;
        });

        this.instanceModal = this.modalService.open(content, { size: 'lg'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }


    refresh() {
        this.reportService.getPhrases(this.type).subscribe(value => {

            if (typeof(value.result) !== 'undefined' && value.result) {
                $(value.data).each((index, element) => {
                    element.selected = false;
                });

                this.listData = value.data;
      //          console.log(this.listData);
            } else {
                this.listData = [];
            }



        }, error => {
            console.log(error);
        });
    }

}
