import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { ReportManageRoutingModule } from './report-manage-routing.module';
import { ReportManageComponent } from './report-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import {ModalComponent} from './modal/modal.component';
import {ModalFileModule} from './modafile/modafile.module';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, ReportManageRoutingModule, ReactiveFormsModule, FormsModule, UiSwitchModule, ModalFileModule
  ],
  declarations: [ReportManageComponent, ModalComponent]
})
export class ReportManageModule { }





