import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { FileUploadModule } from 'ng2-file-upload';
import {ModalFileComponent} from './modafile.component';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, FormsModule, ReactiveFormsModule, FileUploadModule],
    declarations: [ModalFileComponent],
    exports: [ModalFileComponent]
})
export class ModalFileModule {}

