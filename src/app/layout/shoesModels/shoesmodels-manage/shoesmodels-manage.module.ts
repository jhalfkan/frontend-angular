import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { ShoesModelsManageRoutingModule } from './shoesmodels-manage-routing.module';
import { ShoesModelsManageComponent } from './shoesmodels-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, ShoesModelsManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [ShoesModelsManageComponent]
})
export class ShoesModelsManageModule { }





