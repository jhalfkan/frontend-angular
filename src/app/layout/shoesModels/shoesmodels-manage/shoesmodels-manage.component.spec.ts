import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoesModelsManageComponent } from './shoesmodels-manage.component';

describe('SportManageComponent', () => {
  let component: ShoesModelsManageComponent;
  let fixture: ComponentFixture<ShoesModelsManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoesModelsManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoesModelsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
