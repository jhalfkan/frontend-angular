import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ShoesModelsService} from '../../../_repositories';
import {Sport} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-shoesmodels-manage',
    templateUrl: './shoesmodels-manage.component.html',
    styleUrls: ['./shoesmodels-manage.component.scss'],
    providers: [Sport]
})
export class ShoesModelsManageComponent implements OnInit {
    translate;
    baseURl;
    shoesmodelsService;
    shoeModel;
    router;
    activatedRoute;
    id;
    shoesmodels;

    constructor(translate: TranslateService, shoeModelService: ShoesModelsService, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.shoesmodelsService = shoeModelService;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.id = id;
        });
        if (typeof(this.id) !== 'undefined') {
            this.shoesmodelsService.get(this.id).subscribe((response) => {
                this.shoesmodels = response;
            });
        }
    }

    navigateBack() {
        this.router.navigate(['/shoesmodels']);
    }

    edit() {
        this.shoesmodelsService.edit(this.shoesmodels).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item edited with success'));
            this.router.navigate(['/shoesmodels']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
            this.router.navigate(['/shoesmodels']);
        });
    }
}
