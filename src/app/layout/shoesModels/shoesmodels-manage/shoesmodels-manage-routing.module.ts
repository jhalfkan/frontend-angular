import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoesModelsManageComponent } from './shoesmodels-manage.component';

const routes: Routes = [
    {
        path: '',
        component: ShoesModelsManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShoesModelsManageRoutingModule {}
