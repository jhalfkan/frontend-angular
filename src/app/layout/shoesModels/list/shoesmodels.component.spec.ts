import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoesModelsComponent } from './shoesmodels.component';

describe('ShoesModelsComponent', () => {
  let component: ShoesModelsComponent;
  let fixture: ComponentFixture<ShoesModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoesModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoesModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
