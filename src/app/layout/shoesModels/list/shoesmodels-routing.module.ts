import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoesModelsComponent } from './shoesmodels.component';

const routes: Routes = [
    {
        path: '',
        component: ShoesModelsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShoesModelsRoutingModule {}
