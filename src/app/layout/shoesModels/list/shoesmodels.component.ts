import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ShoesModelsService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-shoesmodels',
    templateUrl: './shoesmodels.component.html',
    styleUrls: ['./shoesmodels.component.scss']
})
export class ShoesModelsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;
    router;
    activatedRoute;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, activatedRoute: ActivatedRoute, router: Router) {
        this.translate = translate;
        this.baseURl = ShoesModelsService.BASEURL + '/';
        this.heading = new Array('id', 'name', 'typeshoes', 'price');
        this.actions = new Array();
        this.activatedRoute = activatedRoute;
        this.router = router;

        this.actions.push({'icon': 'fa fa-edit', 'link': 'shoesmodels/shoesmodels/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
