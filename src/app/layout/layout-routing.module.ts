import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import {AdminGuard, HighlevelGuard} from '../shared/guard';
import { CurrentUserBalanceHistoryComponent } from
'./current-user-balance-history/current-user-balance-history.component';
import { AllUserBalanceHistoryComponent } from './all-user-balance-history/all-user-balance-history.component';
import { UserBalanceHistoryComponent } from './user-balance-history/user-balance-history.component';
import { ManagePodsComponent } from './manage-pods/manage-pods.component';
import { LinkPodoCadComponent } from './link-podo-cad/link-podo-cad.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [] },
            { path: 'exportdb', loadChildren: () => import('./exportDB/exportdb-manage/exportdb-manage.module').then(m => m.ExportDBManageModule), canActivate: [AdminGuard
                ] },

            { path: 'cards', loadChildren: () => import('./cards/list/cards.module').then(m => m.CardsModule), canActivate: [HighlevelGuard] },
            { path: 'cards/add', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [HighlevelGuard] },

            { path: 'cards/card/:hash/report', loadChildren: () => import('./reports/report-manage/report-manage.module').then(m => m.ReportManageModule), canActivate: [] },
            { path: 'cards/card/:hash', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [HighlevelGuard] },
            { path: 'cards/card/:hash/edit', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [HighlevelGuard] },
            { path: 'cards/card/:hash/delete', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [HighlevelGuard] },

            { path: 'cards/status/:id', loadChildren: () => import('./cards_status/list/cardsStatus.module').then(m => m.CardsStatusModule), canActivate: [AdminGuard] },

            { path: 'cards/templates', loadChildren: () => import('./cards_templates/list/cardsTemplates.module').then(m => m.CardsTemplatesModule), canActivate: [AdminGuard] },
            { path: 'cards/templates/add', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [AdminGuard] },
            { path: 'cards/templates/template/:hash/edit', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [AdminGuard] },
            { path: 'cards/templates/template/:hash/delete', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [AdminGuard] },

            { path: 'users/cards/card/:hash/report', loadChildren: () => import('./reports/report-manage/report-manage.module').then(m => m.ReportManageModule), canActivate: [] },

            { path: 'users/cards/templates', loadChildren: () => import('./users_cards_templates/list/usersCardsTemplates.module').then(m => m.UsersCardsTemplatesModule), canActivate: [] },
            { path: 'users/cards/templates/add', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/templates/template/:hash/edit', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/templates/template/:hash/delete', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },

            { path: 'users/cards/status/:id', loadChildren: () => import('./users_cards_status/list/usersCardsStatus.module').then(m => m.UsersCardsStatusModule), canActivate: [] },
            { path: 'users/cards', loadChildren: () => import('./users_cards/list/usersCards.module').then(m => m.UsersCardsModule), canActivate: [] },

            { path: 'users/cards/add', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/card/:hash', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/card/:hash/duplicate', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/card/:hash/edit', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },
            { path: 'users/cards/card/:hash/delete', loadChildren: () => import('./cards/card-manage/card-manage.module').then(m => m.CardManageModule), canActivate: [] },


            { path: 'users/waitingLine', loadChildren: () => import('./users_waiting_line/list/usersWaitingLine.module').then(m => m.UsersWaitingLineModule), canActivate: [] },
            { path: 'users/waitingLine/add', loadChildren: () => import('./users_waiting_line/usersWaitingLine-manage/usersWaitingLine-manage.module').then(m => m.UsersWaitingLineManageModule), canActivate: [] },
            { path: 'users/waitingLine/:hash', loadChildren: () => import('./users_waiting_line/usersWaitingLine-manage/usersWaitingLine-manage.module').then(m => m.UsersWaitingLineManageModule), canActivate: [] },
            { path: 'users/waitingLine/:hash/edit', loadChildren: () => import('./users_waiting_line/usersWaitingLine-manage/usersWaitingLine-manage.module').then(m => m.UsersWaitingLineManageModule), canActivate: [] },
            { path: 'users/waitingLine/:hash/delete', loadChildren: () => import('./users_waiting_line/usersWaitingLine-manage/usersWaitingLine-manage.module').then(m => m.UsersWaitingLineManageModule), canActivate: [] },

            { path: 'users/files', loadChildren: () => import('./users_files/list/usersFiles.module').then(m => m.UsersFilesModule), canActivate: [] },
            { path: 'users/files/add', loadChildren: () => import('./users_files/usersFiles-manage/usersFiles-manage.module').then(m => m.UsersFilesManageModule), canActivate: [] },
            { path: 'users/files/file/:hash', loadChildren: () => import('./users_files/usersFiles/usersFiles.module').then(m => m.UsersFilesModule), canActivate: [] },

            { path: 'users/patients', loadChildren: () => import('./users_patients/list/usersPatients.module').then(m => m.UsersPatientsModule), canActivate: [] },
            { path: 'users/patients/add', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [] },
            { path: 'users/patients/patient/:hash/edit', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [] },
            { path: 'users/patients/patient/:hash/delete', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [] },

            { path: 'users', loadChildren: () => import('./users/list/users.module').then(m => m.UsersModule), canActivate: [AdminGuard] },
            { path: 'users', loadChildren: () => import('./users/list/users.module').then(m => m.UsersModule), canActivate: [AdminGuard] },
            { path: 'users/add', loadChildren: () => import('./users/user-manage/user-manage.module').then(m => m.UserManageModule), canActivate: [AdminGuard] },
            { path: 'users/profil', loadChildren: () => import('./users_profil/usersProfil-manage/usersProfil-manage.module').then(m => m.UsersProfilManageModule), canActivate: [] },
            { path: 'users/user/:hash/delete', loadChildren: () => import('./users/user-manage/user-manage.module').then(m => m.UserManageModule), canActivate: [AdminGuard] },
            { path: 'users/user/:hash/edit', loadChildren: () => import('./users/user-manage/user-manage.module').then(m => m.UserManageModule), canActivate: [AdminGuard] },
            { path: 'users/user/:hash', loadChildren: () => import('./users/user/users.module').then(m => m.UsersModule), canActivate: [AdminGuard] },

            { path: 'sports', loadChildren: () => import('./sports/list/sports.module').then(m => m.SportsModule), canActivate: [AdminGuard] },
            { path: 'sports/add', loadChildren: () => import('./sports/sport-manage/sport-manage.module').then(m => m.SportManageModule), canActivate: [AdminGuard] },
            { path: 'sports/sport/:id/edit', loadChildren: () => import('./sports/sport-manage/sport-manage.module').then(m => m.SportManageModule), canActivate: [AdminGuard] },
            { path: 'sports/sport/:id/delete', loadChildren: () => import('./sports/sport-manage/sport-manage.module').then(m => m.SportManageModule), canActivate: [AdminGuard] },

            { path: 'shoesmodels', loadChildren: () => import('./shoesModels/list/shoesmodels.module').then(m => m.ShoesModelsModule), canActivate: [AdminGuard] },
            { path: 'shoesmodels/shoesmodels/:id/edit', loadChildren: () => import('./shoesModels/shoesmodels-manage/shoesmodels-manage.module').then(m => m.ShoesModelsManageModule), canActivate: [AdminGuard] },

            { path: 'files', loadChildren: () => import('./files/list/files.module').then(m => m.FilesModule), canActivate: [AdminGuard] },
            { path: 'files/add', loadChildren: () => import('./files/file-manage/file-manage.module').then(m => m.FileManageModule), canActivate: [AdminGuard] },
            { path: 'files/file/:id/edit', loadChildren: () => import('./files/file-manage/file-manage.module').then(m => m.FileManageModule), canActivate: [AdminGuard] },
            { path: 'files/file/:id/delete', loadChildren: () => import('./files/file/files.module').then(m => m.FilesModule), canActivate: [AdminGuard] },

            { path: 'patients', loadChildren: () => import('./patients/list/patients.module').then(m => m.PatientsModule), canActivate: [AdminGuard] },
            { path: 'patients/add', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [AdminGuard] },
            { path: 'patients/patient/:hash/edit', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [AdminGuard] },
            { path: 'patients/patient/:hash/delete', loadChildren: () => import('./patients/patient-manage/patient-manage.module').then(m => m.PatientManageModule), canActivate: [AdminGuard] },

            { path: 'materials', loadChildren: () => import('./materials/list/materials.module').then(m => m.MaterialsModule), canActivate: [AdminGuard] },
            { path: 'materials/add', loadChildren: () => import('./materials/material-manage/material-manage.module').then(m => m.MaterialManageModule), canActivate: [AdminGuard] },
            { path: 'materials/material/:id/edit', loadChildren: () => import('./materials/material-manage/material-manage.module').then(m => m.MaterialManageModule), canActivate: [AdminGuard] },
            { path: 'materials/material/:id/delete', loadChildren: () => import('./materials/material-manage/material-manage.module').then(m => m.MaterialManageModule), canActivate: [AdminGuard] },

            { path: 'flangesColors', loadChildren: () => import('./flangesColors/list/flangesColors.module').then(m => m.FlangesColorsModule), canActivate: [AdminGuard] },
            { path: 'flangesColors/add', loadChildren: () => import('./flangesColors/flangesColor-manage/flangesColor-manage.module').then(m => m.FlangesColorManageModule), canActivate: [AdminGuard] },
            { path: 'flangesColors/flangesColor/:id/edit', loadChildren: () => import('./flangesColors/flangesColor-manage/flangesColor-manage.module').then(m => m.FlangesColorManageModule), canActivate: [AdminGuard] },
            { path: 'flangesColors/flangesColor/:id/delete', loadChildren: () => import('./flangesColors/flangesColor-manage/flangesColor-manage.module').then(m => m.FlangesColorManageModule), canActivate: [AdminGuard] },

            { path: 'shippings', loadChildren: () => import('./shippings/list/shippings.module').then(m => m.ShippingsModule), canActivate: [AdminGuard] },
            { path: 'shippings/add', loadChildren: () => import('./shippings/shipping-manage/shipping-manage.module').then(m => m.ShippingManageModule), canActivate: [AdminGuard] },
            { path: 'shippings/shipping/:id/edit', loadChildren: () => import('./shippings/shipping-manage/shipping-manage.module').then(m => m.ShippingManageModule), canActivate: [AdminGuard] },
            { path: 'shippings/shipping/:id/delete', loadChildren: () => import('./shippings/shipping-manage/shipping-manage.module').then(m => m.ShippingManageModule), canActivate: [AdminGuard] },

            { path: 'finishings', loadChildren: () => import('./finishings/list/finishings.module').then(m => m.FinishingsModule), canActivate: [AdminGuard] },
            { path: 'finishings/finishing/:id/edit', loadChildren: () => import('./finishings/finishing-manage/finishing-manage.module').then(m => m.FinishingManageModule), canActivate: [AdminGuard] },

            { path: 'pathologies', loadChildren: () => import('./pathologies/list/pathologies.module').then(m => m.PathologiesModule), canActivate: [AdminGuard] },
            { path: 'pathologies/pathology/:id', loadChildren: () => import('./pathologies/pathology/pathologies.module').then(m => m.PathologiesModule), canActivate: [AdminGuard] },
            { path: 'pathologies/add', loadChildren: () => import('./pathologies/pathology-manage/pathology-manage.module').then(m => m.PathologyManageModule), canActivate: [AdminGuard] },
            { path: 'pathologies/pathology/:id/edit', loadChildren: () => import('./pathologies/pathology-manage/pathology-manage.module').then(m => m.PathologyManageModule), canActivate: [AdminGuard] },
            { path: 'pathologies/pathology/:id/delete', loadChildren: () => import('./pathologies/pathology-manage/pathology-manage.module').then(m => m.PathologyManageModule), canActivate: [AdminGuard] },

            { path: 'messages', loadChildren: () => import('./messages/list/messages.module').then(m => m.MessagesModule), canActivate: [AdminGuard] },
            { path: 'messages/add', loadChildren: () => import('./messages/message-manage/message-manage.module').then(m => m.MessageManageModule), canActivate: [AdminGuard] },
            { path: 'messages/message/:id/edit', loadChildren: () => import('./messages/message-manage/message-manage.module').then(m => m.MessageManageModule), canActivate: [AdminGuard] },
            { path: 'messages/message/:id/delete', loadChildren: () => import('./messages/message-manage/message-manage.module').then(m => m.MessageManageModule), canActivate: [AdminGuard] },

            { path: 'recoveries', loadChildren: () => import('./recoveries/list/recoveries.module').then(m => m.RecoveriesModule), canActivate: [AdminGuard] },
            { path: 'recoveries/recovery/:id', loadChildren: () => import('./recoveries/recovery/recoveries.module').then(m => m.RecoveriesModule), canActivate: [AdminGuard] },
            { path: 'recoveries/add', loadChildren: () => import('./recoveries/recovery-manage/recovery-manage.module').then(m => m.RecoveryManageModule), canActivate: [AdminGuard] },
            { path: 'recoveries/recovery/:id/edit', loadChildren: () => import('./recoveries/recovery-manage/recovery-manage.module').then(m => m.RecoveryManageModule), canActivate: [AdminGuard] },
            { path: 'recoveries/recovery/:id/delete', loadChildren: () => import('./recoveries/recovery-manage/recovery-manage.module').then(m => m.RecoveryManageModule), canActivate: [AdminGuard] },

            { path: 'gabarits', loadChildren: () => import('./gabarits/list/gabarits.module').then(m => m.GabaritsModule), canActivate: [AdminGuard] },
            { path: 'gabarits/gabarit/:id', loadChildren: () => import('./gabarits/gabarit/gabarits.module').then(m => m.GabaritsModule), canActivate: [AdminGuard] },
            { path: 'gabarits/add', loadChildren: () => import('./gabarits/gabarit-manage/gabarit-manage.module').then(m => m.GabaritManageModule), canActivate: [AdminGuard] },
            { path: 'gabarits/gabarit/:id/edit', loadChildren: () => import('./gabarits/gabarit-manage/gabarit-manage.module').then(m => m.GabaritManageModule), canActivate: [AdminGuard] },
            { path: 'gabarits/gabarit/:id/delete', loadChildren: () => import('./gabarits/gabarit-manage/gabarit-manage.module').then(m => m.GabaritManageModule), canActivate: [AdminGuard] },

            { path: 'status', loadChildren: () => import('./status/list/status.module').then(m => m.StatusModule), canActivate: [AdminGuard] },
            { path: 'status/status/:id', loadChildren: () => import('./status/status/status.module').then(m => m.StatusModule), canActivate: [AdminGuard] },
            { path: 'status/add', loadChildren: () => import('./status/status-manage/status-manage.module').then(m => m.StatusManageModule), canActivate: [AdminGuard] },
            { path: 'status/status/:id/edit', loadChildren: () => import('./status/status-manage/status-manage.module').then(m => m.StatusManageModule), canActivate: [AdminGuard] },
            { path: 'status/status/:id/delete', loadChildren: () => import('./status/status-manage/status-manage.module').then(m => m.StatusManageModule), canActivate: [AdminGuard] },


            { path: 'dev_tool/dashboard', loadChildren: () => import('./_devtools/dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [AdminGuard] },
            { path: 'charts', loadChildren: () => import('./_devtools/charts/charts.module').then(m => m.ChartsModule), canActivate: [AdminGuard] },
            { path: 'tables', loadChildren: () => import('./_devtools/tables/tables.module').then(m => m.TablesModule), canActivate: [AdminGuard] },
            { path: 'forms', loadChildren: () => import('./_devtools/form/form.module').then(m => m.FormModule), canActivate: [AdminGuard] },
            { path: 'bs-element', loadChildren: () => import('./_devtools/bs-element/bs-element.module').then(m => m.BsElementModule), canActivate: [AdminGuard] },
            { path: 'grid', loadChildren: () => import('./_devtools/grid/grid.module').then(m => m.GridModule), canActivate: [AdminGuard] },
            { path: 'components', loadChildren: () => import('./_devtools/bs-component/bs-component.module').then(m => m.BsComponentModule), canActivate: [AdminGuard] },
            { path: 'blank-page', loadChildren: () => import('./_devtools/blank-page/blank-page.module').then(m => m.BlankPageModule), canActivate: [AdminGuard] },
            {
                path: 'current-user-balance-history',
                component: CurrentUserBalanceHistoryComponent,
                canActivate: []
            },
            {
                path: 'all-users-balance-history',
                component: AllUserBalanceHistoryComponent,
                canActivate: [AdminGuard]
            },
            {
                path: 'user-balance-history/:hash',
                component: UserBalanceHistoryComponent,
                canActivate: [AdminGuard]
            },
            {
                path: 'manage-pods',
                component: ManagePodsComponent,
                canActivate: [AdminGuard]
            },
            {
                path: 'manage-pods/:hash',
                component: ManagePodsComponent,
                canActivate: [AdminGuard]
            },
            {
                path: 'link-podo-cad/:hash',
                component: LinkPodoCadComponent,
                canActivate: [AdminGuard]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
