import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersCardsRoutingModule } from './usersCards-routing.module';
import { UsersCardsComponent } from './usersCards.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersCardsRoutingModule, PageHeaderModule],
    declarations: [UsersCardsComponent]
})
export class UsersCardsModule {}
