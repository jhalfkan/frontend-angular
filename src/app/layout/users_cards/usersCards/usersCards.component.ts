import {Component, OnInit} from '@angular/core';
import {UsersCards} from '../../../_models';
import {UsersCardsService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-usersCards',
    templateUrl: './usersCards.component.html',
    styleUrls: ['./usersCards.component.scss']
})
export class UsersCardsComponent implements OnInit {
    usersCardsService;
    usersCardsModel;
    activatedRoute;
    status: string;

    constructor(usersCards: UsersCardsService, usersCardsModel: UsersCards, activatedRoute: ActivatedRoute) {
        this.usersCardsService = usersCards;
        this.usersCardsModel = usersCardsModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteUsersCards() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.usersCardsModel.name};

/*        this.usersCardsService.deleteUsersCards(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
