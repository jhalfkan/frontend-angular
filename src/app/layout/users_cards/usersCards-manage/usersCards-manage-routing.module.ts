import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersCardsManageComponent } from './usersCards-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersCardsManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersCardsManageRoutingModule {}
