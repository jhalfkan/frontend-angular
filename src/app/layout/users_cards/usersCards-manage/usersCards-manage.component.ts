import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersCardsService} from '../../../_repositories';
import {UsersCards} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-users-cards-manage',
    templateUrl: './usersCards-manage.component.html',
    styleUrls: ['./usersCards-manage.component.scss'],
    providers: [UsersCards]
})
export class UsersCardsManageComponent implements OnInit {
    translate;
    baseURl;
    usersCardsService;
    usersCardsModel;
    router;
    activatedRoute;
    status: string;
    idUsersCards;
    nameUsersCards;

    constructor(translate: TranslateService, usersCards: UsersCardsService, usersCardsModel: UsersCards, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.usersCardsService = usersCards;
        this.usersCardsModel = usersCardsModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idUsersCards = id;
        });
        if (this.idUsersCards !== undefined) {
            this.usersCardsService.getUsersCards(this.idUsersCards).subscribe((response) => {
                this.nameUsersCards = response.name;
            });
        }
    }

    gestionUsersCards(id) {
        if (id === undefined) {
            let usersCards = {name: this.usersCardsModel.name};
            this.usersCardsService.createUsersCards(usersCards).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/users/cards']);
            });
        } else {
            let usersCards = {id: id, name: this.usersCardsModel.name};
            console.log(usersCards);
            this.usersCardsService.editUsersCards(usersCards).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/users/cards']);
            });
        }
    }
}
