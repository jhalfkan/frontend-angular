import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersCardsManageComponent } from './usersCards-manage.component';

describe('UsersCardsManageComponent', () => {
  let component: UsersCardsManageComponent;
  let fixture: ComponentFixture<UsersCardsManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersCardsManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersCardsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
