import { UsersCardsManageModule } from './usersCards-manage.module';

describe('UsersCardsManageModule', () => {
  let usersCardsManageModule: UsersCardsManageModule;

  beforeEach(() => {
    usersCardsManageModule = new UsersCardsManageModule();
  });

  it('should create an instance', () => {
    expect(usersCardsManageModule).toBeTruthy();
  });
});
