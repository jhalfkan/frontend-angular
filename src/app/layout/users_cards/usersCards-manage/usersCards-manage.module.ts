import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersCardsManageRoutingModule } from './usersCards-manage-routing.module';
import { UsersCardsManageComponent } from './usersCards-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersCardsManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [UsersCardsManageComponent]
})
export class UsersCardsManageModule { }





