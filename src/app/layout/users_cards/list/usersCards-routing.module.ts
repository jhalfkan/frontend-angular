import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersCardsComponent } from './usersCards.component';

const routes: Routes = [
    {
        path: '',
        component: UsersCardsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersCardsRoutingModule {}
