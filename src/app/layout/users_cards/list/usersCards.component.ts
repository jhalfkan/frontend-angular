import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {StatusService, UsersCardsService} from '../../../_repositories';
import {first} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators, FormsModule} from '@angular/forms';
import {DatatableService} from '../../../shared/services/DatatableService';
import {AuthenticationService} from '../../../_services';

@Component({
    selector: 'app-users-cards',
    templateUrl: './usersCards.component.html',
    styleUrls: ['./usersCards.component.scss']
})
export class UsersCardsComponent implements OnInit {
    baseURl;
    actions;
    heading;
    order;
    filterStatus: any = "";
    paramQuery;
    status: Array<any> = [];
    public alerts: Array<any> = [];
    userRoles;

    constructor(private activatedRoute: ActivatedRoute, private authentificat: AuthenticationService, private _dataTableService: DatatableService, private statusService: StatusService, private router: Router) {
        this.baseURl = UsersCardsService.BASEURL + '/';
        this.userRoles = authentificat.getRoles();
        this.heading = new Array('id', 'patient', 'dateClosing', 'dateInprocess', 'dateSent', 'dateBilled');
        this.actions = new Array();
        this.order = [[ 0, 'DESC' ]];

        this.statusService.getAll().pipe(first())
            .subscribe(
                data => {
                    if (data.result === true) {
                        this.status = data.datas;
                        //      this.status.unshift({id: '', name: ''});
                    }

                    if (typeof(this.paramQuery.status) !== 'undefined') {
                        this.filterStatus = parseInt(this.paramQuery.status);
                    }
                },
                error => {

                });

        this.activatedRoute.queryParams.pipe(first()).subscribe(params => {
            this.paramQuery = params;
            //    console.log(this.paramQuery);
        });

        this.actions.push({'icon': 'fa fa-edit', 'link': 'users/cards/card/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-clone', 'link': 'users/cards/card/:hash/duplicate', 'button': 'btn btn-info btn-sm clone', 'action': 'duplicate'});
        this.actions.push({'icon': 'fa fa-eye', 'link': 'users/cards/card/:hash', 'button': 'btn btn-secondary btn-sm', 'action': 'view'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'users/cards/card/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
        this.actions.push({'icon': 'fa fa-file', 'link': 'users/cards/card/:hash/report', 'button': 'btn btn-fiche btn-sm report', 'action': 'report'});
        this.actions.push({'icon': 'fa fa-file-pdf-o', 'button': 'btn btn-fiche pdf btn-sm', 'action': 'pdf'});
        //   this.actions.push({'icon': 'fa fa-database', 'link': 'cards/card/:hash/delete', 'button': 'btn btn-sm', 'action': 'status'});
    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }


    filterCardByStatus() {
        if (typeof(this.filterStatus) !== 'undefined' && this.filterStatus !== '') {
            this.router.navigate(['/users/cards'], { queryParams: { status: this.filterStatus } });
        } else {
            this.router.navigate(['/users/cards'], {});
        }

        this._dataTableService.filter('realooadHeading', this.heading);

        if (this.filterStatus === '') {
            this._dataTableService.filter('Reinitialize', {});
        } else {
            this._dataTableService.filter('Reinitialize', {'status' : this.filterStatus});
        }

    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
