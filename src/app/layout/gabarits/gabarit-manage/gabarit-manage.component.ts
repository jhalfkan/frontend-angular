import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {GabaritService} from '../../../_repositories';
import {Gabarit} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-gabarit-manage',
    templateUrl: './gabarit-manage.component.html',
    styleUrls: ['./gabarit-manage.component.scss'],
    providers: [Gabarit]
})
export class GabaritManageComponent implements OnInit {
    translate;
    baseURl;
    gabaritService;
    gabaritModel;
    router;
    activatedRoute;
    idGabarit;
    gabarit;
    deleteAction;

    constructor(translate: TranslateService, gabarit: GabaritService, gabaritModel: Gabarit, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.gabaritService = gabarit;
        this.gabaritModel = gabaritModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idGabarit = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idGabarit) !== 'undefined') {
            this.gabaritService.getGabarit(this.idGabarit).subscribe((response) => {
                this.gabarit = response;
            });
        } else {
            this.gabarit = this.gabaritModel;
        }
    }

    navigateBack() {
        this.router.navigate(['/gabarits']);
    }

    gestionGabarit(gabarit) {
        if (typeof(gabarit.id) === 'undefined') {
            this.gabaritService.createGabarit(this.gabarit).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/gabarits']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/gabarits']);
            });
        } else {
            this.gabaritService.editGabarit(this.gabarit).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/gabarits']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/gabarits']);
            });
        }
    }

    delete() {
        this.gabaritService.deleteGabarit(this.gabarit).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/gabarits']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/gabarits']);
        });
    }
}
