import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { GabaritManageRoutingModule } from './gabarit-manage-routing.module';
import { GabaritManageComponent } from './gabarit-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, GabaritManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [GabaritManageComponent]
})
export class GabaritManageModule { }





