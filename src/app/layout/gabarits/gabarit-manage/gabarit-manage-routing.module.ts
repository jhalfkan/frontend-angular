import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GabaritManageComponent } from './gabarit-manage.component';

const routes: Routes = [
    {
        path: '',
        component: GabaritManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GabaritManageRoutingModule {}
