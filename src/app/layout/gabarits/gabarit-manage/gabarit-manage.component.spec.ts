import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GabaritManageComponent } from './gabarit-manage.component';

describe('GabaritManageComponent', () => {
  let component: GabaritManageComponent;
  let fixture: ComponentFixture<GabaritManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GabaritManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GabaritManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
