import { Component, OnInit } from '@angular/core';
import {GabaritService} from '../../../_repositories';
import {TranslateService} from '@ngx-translate/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-gabarits',
  templateUrl: './gabarits.component.html',
  styleUrls: ['./gabarits.component.scss']
})
export class GabaritsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    router;
    activatedRoute;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, activatedRoute: ActivatedRoute, router: Router) {
        this.translate = translate;
        this.baseURl = GabaritService.BASEURL + '/';
        this.heading = new Array('id', 'name');
        this.actions = new Array();
        this.activatedRoute = activatedRoute;
        this.router = router;

        this.actions.push({'icon': 'fa fa-edit', 'link': 'gabarits/gabarit/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'gabarits/gabarit/:id/delete', 'button': 'btn delete btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
