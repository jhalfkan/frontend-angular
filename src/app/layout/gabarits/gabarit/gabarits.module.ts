import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { GabaritsRoutingModule } from './gabarits-routing.module';
import { GabaritsComponent } from './gabarits.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, GabaritsRoutingModule, PageHeaderModule],
    declarations: [GabaritsComponent]
})
export class GabaritsModule {}
