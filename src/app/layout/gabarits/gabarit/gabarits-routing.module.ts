import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GabaritsComponent } from './gabarits.component';

const routes: Routes = [
    {
        path: '',
        component: GabaritsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GabaritsRoutingModule {}
