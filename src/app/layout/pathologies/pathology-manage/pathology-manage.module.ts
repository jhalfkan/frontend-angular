import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { PathologyManageRoutingModule } from './pathology-manage-routing.module';
import { PathologyManageComponent } from './pathology-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, PathologyManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [PathologyManageComponent]
})
export class PathologyManageModule { }





