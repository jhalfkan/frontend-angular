import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathologyManageComponent } from './pathology-manage.component';

const routes: Routes = [
    {
        path: '',
        component: PathologyManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PathologyManageRoutingModule {}
