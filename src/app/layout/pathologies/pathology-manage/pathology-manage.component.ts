import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {PathologyService} from '../../../_repositories';
import {Pathology} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-pathology-manage',
    templateUrl: './pathology-manage.component.html',
    styleUrls: ['./pathology-manage.component.scss'],
    providers: [Pathology]
})
export class PathologyManageComponent implements OnInit {
    translate;
    baseURl;
    pathologyService;
    pathologyModel;
    router;
    activatedRoute;
    idPathology;
    pathology;
    deleteAction;

    constructor(translate: TranslateService, pathology: PathologyService, pathologyModel: Pathology, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.pathologyService = pathology;
        this.pathologyModel = pathologyModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idPathology = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idPathology) !== 'undefined') {
            this.pathologyService.getPathology(this.idPathology).subscribe((response) => {
                this.pathology = response;
            });
        } else {
            this.pathology = this.pathologyModel;
        }
    }

    navigateBack() {
        this.router.navigate(['/pathologies']);
    }

    gestionPathology(pathology) {
        if (typeof(pathology.id) === 'undefined') {
            this.pathologyService.createPathology(this.pathology).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/pathologies']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/pathologies']);
            });
        } else {
            this.pathologyService.editPathology(this.pathology).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/pathologies']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/pathologies']);
            });
        }
    }

    delete() {
        this.pathologyService.deletePathology(this.pathology).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/pathologies']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/pathologies']);
        });
    }
}
