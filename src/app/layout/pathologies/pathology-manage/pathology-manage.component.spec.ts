import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PathologyManageComponent } from './pathology-manage.component';

describe('PathologyManageComponent', () => {
  let component: PathologyManageComponent;
  let fixture: ComponentFixture<PathologyManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PathologyManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PathologyManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
