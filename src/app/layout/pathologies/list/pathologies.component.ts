import { Component, OnInit } from '@angular/core';
import {PathologyService} from '../../../_repositories';
import {TranslateService} from '@ngx-translate/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-pathologies',
  templateUrl: './pathologies.component.html',
  styleUrls: ['./pathologies.component.scss']
})
export class PathologiesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    router;
    activatedRoute;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, activatedRoute: ActivatedRoute, router: Router) {
        this.translate = translate;
        this.baseURl = PathologyService.BASEURL + '/';
        this.heading = new Array('id', 'name');
        this.actions = new Array();
        this.activatedRoute = activatedRoute;
        this.router = router;

        this.actions.push({'icon': 'fa fa-edit', 'link': 'pathologies/pathology/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'pathologies/pathology/:id/delete', 'button': 'btn delete btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
