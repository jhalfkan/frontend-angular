import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PathologiesComponent } from './pathologies.component';

const routes: Routes = [
    {
        path: '',
        component: PathologiesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PathologiesRoutingModule {}
