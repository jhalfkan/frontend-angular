import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { PathologiesRoutingModule } from './pathologies-routing.module';
import { PathologiesComponent } from './pathologies.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, PathologiesRoutingModule, PageHeaderModule],
    declarations: [PathologiesComponent]
})
export class PathologiesModule {}
