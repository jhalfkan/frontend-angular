import {Component, OnInit} from '@angular/core';
import {UsersPatients} from '../../../_models';
import {UsersPatientsService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-usersPatients',
    templateUrl: './usersPatients.component.html',
    styleUrls: ['./usersPatients.component.scss']
})
export class UsersPatientsComponent implements OnInit {
    usersPatientsService;
    usersPatientsModel;
    activatedRoute;
    status: string;

    constructor(usersPatients: UsersPatientsService, usersPatientsModel: UsersPatients, activatedRoute: ActivatedRoute) {
        this.usersPatientsService = usersPatients;
        this.usersPatientsModel = usersPatientsModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteUsersPatients() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.usersPatientsModel.name};

/*        this.usersPatientsService.deleteUsersPatients(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
