import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersPatientsComponent } from './usersPatients.component';

const routes: Routes = [
    {
        path: '',
        component: UsersPatientsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersPatientsRoutingModule {}
