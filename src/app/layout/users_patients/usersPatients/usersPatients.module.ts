import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersPatientsRoutingModule } from './usersPatients-routing.module';
import { UsersPatientsComponent } from './usersPatients.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersPatientsRoutingModule, PageHeaderModule],
    declarations: [UsersPatientsComponent]
})
export class UsersPatientsModule {}
