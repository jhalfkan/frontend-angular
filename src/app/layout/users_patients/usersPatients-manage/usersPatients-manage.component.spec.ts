import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersPatientsManageComponent } from './usersPatients-manage.component';

describe('UsersPatientsManageComponent', () => {
  let component: UsersPatientsManageComponent;
  let fixture: ComponentFixture<UsersPatientsManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersPatientsManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersPatientsManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
