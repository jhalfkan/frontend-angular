import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersPatientsService} from '../../../_repositories';
import {UsersPatients} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-users-patients-manage',
    templateUrl: './usersPatients-manage.component.html',
    styleUrls: ['./usersPatients-manage.component.scss'],
    providers: [UsersPatients]
})
export class UsersPatientsManageComponent implements OnInit {
    translate;
    baseURl;
    usersPatientsService;
    usersPatientsModel;
    router;
    activatedRoute;
    status: string;
    idUsersPatients;
    nameUsersPatients;

    constructor(translate: TranslateService, usersPatients: UsersPatientsService, usersPatientsModel: UsersPatients, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.usersPatientsService = usersPatients;
        this.usersPatientsModel = usersPatientsModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idUsersPatients = id;
        });
        if (this.idUsersPatients !== undefined) {
            this.usersPatientsService.getUsersPatients(this.idUsersPatients).subscribe((response) => {
                this.nameUsersPatients = response.name;
            });
        }
    }

    gestionUsersPatients(id) {
        if (id === undefined) {
            let usersPatients = {name: this.usersPatientsModel.name};
            this.usersPatientsService.createUsersPatients(usersPatients).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersPatients']);
            });
        } else {
            let usersPatients = {id: id, name: this.usersPatientsModel.name};
            console.log(usersPatients);
            this.usersPatientsService.editUsersPatients(usersPatients).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersPatients']);
            });
        }
    }
}
