import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersPatientsManageRoutingModule } from './usersPatients-manage-routing.module';
import { UsersPatientsManageComponent } from './usersPatients-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersPatientsManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [UsersPatientsManageComponent]
})
export class UsersPatientsManageModule { }





