import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersPatientsManageComponent } from './usersPatients-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersPatientsManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersPatientsManageRoutingModule {}
