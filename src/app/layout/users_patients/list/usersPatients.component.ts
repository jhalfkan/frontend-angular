import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersPatientsService} from '../../../_repositories';

@Component({
    selector: 'app-users-patients',
    templateUrl: './usersPatients.component.html',
    styleUrls: ['./usersPatients.component.scss']
})
export class UsersPatientsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = UsersPatientsService.BASEURL + '/';
        this.heading = new Array('id', 'firstname', 'lastname', 'podo');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'users/patients/patient/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
      //  this.actions.push({'icon': 'fa fa-trash', 'link': 'users/patients/patient/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
        this.actions.push({'icon': 'fa fa-file', 'link': 'cards/add?patient=:hash', 'button': 'btn btn-warning btn-sm', 'action': 'createCard'});
    }

    ngOnInit() {
    }
}
