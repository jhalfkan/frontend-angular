import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MessageService} from '../../../_repositories';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = MessageService.BASEURL + '/';
        this.heading = new Array('id', 'title', 'message', 'active');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'messages/message/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'messages/message/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
