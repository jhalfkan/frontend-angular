import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { MessageManageRoutingModule } from './message-manage-routing.module';
import { MessageManageComponent } from './message-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, MessageManageRoutingModule, ReactiveFormsModule, FormsModule, HttpClientModule, AngularEditorModule, ColorPickerModule
  ],
  declarations: [MessageManageComponent]
})
export class MessageManageModule { }





