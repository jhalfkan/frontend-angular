import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MessageService} from '../../../_repositories';
import {Message} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {MessagesDisplayService} from '../../../_services/message.service';

@Component({
    selector: 'app-message-manage',
    templateUrl: './message-manage.component.html',
    styleUrls: ['./message-manage.component.scss'],
    providers: [Message]
})
export class MessageManageComponent implements OnInit {
    translate;
    baseURl;
    messageService;
    messageModel;
    router;
    activatedRoute;
    idMessage;
    deleteAction;
    htmlContent = '';
    message;

    constructor(translate: TranslateService, message: MessageService, private _messageService: MessagesDisplayService, messageModel: Message, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.messageService = message;
        this.messageModel = messageModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idMessage = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idMessage) !== 'undefined') {
            this.messageService.getMessage(this.idMessage).subscribe((response) => {
                this.message = response;
                this.htmlContent = response.message;
            });
        } else {
            this.message = new Message();
            this.htmlContent = '';
        }
    }

    config: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '10rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no'
    };

    navigateBack() {
        this.router.navigate(['/messages']);
    }

    gestionMessage(message) {
        if (typeof(message.id) === 'undefined') {
            this.message.text = this.htmlContent;
            this.messageService.createMessage(this.message).subscribe((response) => {
                this._messageService.filter('Reinitialize', {});
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/messages']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/messages']);
            });
        } else {
            this.message.text = this.htmlContent;
            this.messageService.editMessage(this.message).subscribe((response) => {
                this._messageService.filter('Reinitialize', {});
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/messages']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/messages']);
            });
        }
    }

    delete() {
        this.messageService.deleteMessage(this.message).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/messages']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/messages']);
        });
    }
}
