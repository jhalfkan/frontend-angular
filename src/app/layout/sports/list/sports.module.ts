import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { SportsRoutingModule } from './sports-routing.module';
import { SportsComponent } from './sports.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, SportsRoutingModule, PageHeaderModule, DatatableModule, NgbAlertModule],
    declarations: [SportsComponent]
})
export class SportsModule {}
