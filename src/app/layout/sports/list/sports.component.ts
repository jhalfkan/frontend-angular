import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SportService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-sports',
    templateUrl: './sports.component.html',
    styleUrls: ['./sports.component.scss']
})
export class SportsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;
    router;
    activatedRoute;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, activatedRoute: ActivatedRoute, router: Router) {
        this.translate = translate;
        this.baseURl = SportService.BASEURL + '/';
        this.heading = new Array('id', 'name');
        this.actions = new Array();
        this.activatedRoute = activatedRoute;
        this.router = router;

        this.actions.push({'icon': 'fa fa-edit', 'link': 'sports/sport/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'sports/sport/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
