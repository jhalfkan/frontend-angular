import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportManageComponent } from './sport-manage.component';

describe('SportManageComponent', () => {
  let component: SportManageComponent;
  let fixture: ComponentFixture<SportManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
