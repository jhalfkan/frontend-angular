import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SportService} from '../../../_repositories';
import {Sport} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-sport-manage',
    templateUrl: './sport-manage.component.html',
    styleUrls: ['./sport-manage.component.scss'],
    providers: [Sport]
})
export class SportManageComponent implements OnInit {
    translate;
    baseURl;
    sportService;
    sportModel;
    router;
    activatedRoute;
    idSport;
    sport;
    deleteAction;

    constructor(translate: TranslateService, sport: SportService, sportModel: Sport, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.sportService = sport;
        this.sportModel = sportModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idSport = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idSport) !== 'undefined') {
            this.sportService.getSport(this.idSport).subscribe((response) => {
                this.sport = response;
            });
        } else {
            this.sport = this.sportModel;
        }
    }

    navigateBack() {
        this.router.navigate(['/sports']);
    }

    gestionSport(status) {
        if (typeof(status.id) === 'undefined') {
            this.sportService.createSport(this.sport).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/sports']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/sports']);
            });
        } else {
            this.sportService.editSport(this.sport).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/sports']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/sports']);
            });
        }
    }

    delete() {
        this.sportService.deleteSport(this.sport).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/sports']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/sports']);
        });
    }
}
