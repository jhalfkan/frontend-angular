import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SportManageComponent } from './sport-manage.component';

const routes: Routes = [
    {
        path: '',
        component: SportManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SportManageRoutingModule {}
