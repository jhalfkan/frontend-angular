import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { SportManageRoutingModule } from './sport-manage-routing.module';
import { SportManageComponent } from './sport-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, SportManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [SportManageComponent]
})
export class SportManageModule { }





