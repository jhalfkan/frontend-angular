import {Component, OnInit} from '@angular/core';
import {Sport} from '../../../_models';
import {SportService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-sports',
    templateUrl: './sports.component.html',
    styleUrls: ['./sports.component.scss']
})
export class SportsComponent implements OnInit {
    sportService;
    sportModel;
    activatedRoute;
    status: string;

    constructor(sport: SportService, sportModel: Sport, activatedRoute: ActivatedRoute) {
        this.sportService = sport;
        this.sportModel = sportModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }
}
