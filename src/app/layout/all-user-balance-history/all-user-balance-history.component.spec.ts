import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserBalanceHistoryComponent } from './all-user-balance-history.component';

describe('AllUserBalanceHistoryComponent', () => {
  let component: AllUserBalanceHistoryComponent;
  let fixture: ComponentFixture<AllUserBalanceHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllUserBalanceHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllUserBalanceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
