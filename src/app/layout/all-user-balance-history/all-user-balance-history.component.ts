import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-all-user-balance-history',
  templateUrl: './all-user-balance-history.component.html',
  styleUrls: ['./all-user-balance-history.component.scss']
})
export class AllUserBalanceHistoryComponent implements OnInit {
  public baseURl = 'balance-history/all';
  public heading = ['id', 'podo', 'amount', 'createdAt', 'cardOrBill', 'other'];
  constructor(private datePipe: DatePipe, private translateService: TranslateService) { }

  ngOnInit() {
  }

  public formatDate = (balanceHistoryLines) => {
    for (let i = 0; i < balanceHistoryLines.length; ++i) {
      balanceHistoryLines[i].createdAt = this.datePipe.transform(
        balanceHistoryLines[i].createdAt, 'dd/MM/yyyy HH:mm'
      );
      balanceHistoryLines[i].cardOrBill = this.populateCardIdOrBill(balanceHistoryLines[i])
      balanceHistoryLines[i].amount = this.addAmountSign(balanceHistoryLines[i].amount)
      balanceHistoryLines[i].podo = balanceHistoryLines[i].firstname + " " + balanceHistoryLines[i].lastname
    }
    return balanceHistoryLines
  }

  public populateCardIdOrBill (balanceHistoryLine) {
    if (balanceHistoryLine.cardId) {
      return this.translateService.instant('cardId') + ': ' + balanceHistoryLine.cardId
    }
    return balanceHistoryLine.bill
  }

  public addAmountSign (value) {
    if (value < 0) {
      return value
    }
    return "+ " + value
  }

}
