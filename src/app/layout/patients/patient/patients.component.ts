import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../_models';
import {PatientService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-patients',
    templateUrl: './patients.component.html',
    styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
    patientService;
    patientModel;
    activatedRoute;
    status: string;

    constructor(patient: PatientService, patientModel: Patient, activatedRoute: ActivatedRoute) {
        this.patientService = patient;
        this.patientModel = patientModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }
}
