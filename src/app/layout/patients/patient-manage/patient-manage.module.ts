import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { PatientManageRoutingModule } from './patient-manage-routing.module';
import { PatientManageComponent } from './patient-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdDatepickerI18n } from './datepicker-popup';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, PatientManageRoutingModule, ReactiveFormsModule, FormsModule, NgbModule
  ],
  declarations: [PatientManageComponent, NgbdDatepickerI18n],
  bootstrap: [PatientManageComponent]
})
export class PatientManageModule { }





