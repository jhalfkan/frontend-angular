import { PatientManageModule } from './patient-manage.module';

describe('PatientManageModule', () => {
  let patientManageModule: PatientManageModule;

  beforeEach(() => {
    patientManageModule = new PatientManageModule();
  });

  it('should create an instance', () => {
    expect(patientManageModule).toBeTruthy();
  });
});
