import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientManageComponent } from './patient-manage.component';

const routes: Routes = [
    {
        path: '',
        component: PatientManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PatientManageRoutingModule {}
