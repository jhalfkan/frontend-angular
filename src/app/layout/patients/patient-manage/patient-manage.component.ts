import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {PatientService, UserService} from '../../../_repositories';
import {Patient} from '../../../_models';
import * as $ from 'jquery';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
    selector: 'app-patient-manage',
    templateUrl: './patient-manage.component.html',
    styleUrls: ['./patient-manage.component.scss'],
    providers: [Patient]
})
export class PatientManageComponent implements OnInit {
    translate;
    baseURl;
    patientService;
    patientModel;
    router;
    activatedRoute;
    status: string;
    hashPatient;
    birthdate;
    bmi;
    deleteAction;
    patient = Patient;
    userService;

    constructor(translate: TranslateService, patient: PatientService, patientModel: Patient, routeur: Router, activatedRoute: ActivatedRoute, userService: UserService) {
        this.translate = translate;
        this.patientService = patient;
        this.patientModel = patientModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
        this.userService = userService;
    }

    ngOnInit() {
        console.log('patient-manage');
        this.userService.resetPreferences();
        this.activatedRoute.params.subscribe((params: Params) => {
            let hash = params['hash'];
            this.hashPatient = hash;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (typeof(this.hashPatient) !== 'undefined') {
            this.patientService.getPatient(this.hashPatient).subscribe((response) => {
                this.patient = response[0];
                this.formatDate();
                //this.patient.birthdate = this.patient.birthdate.replace('-', '/');
                //$('#datePickerField').val(this.patient['birthdate']);
                this.bmi = this.calculateBMI(this.patient);
                
                this.userService.getUser(response[0].userHash).subscribe((response) => {
                    this.userService.setMandatoryFields(response[0]);
                });
            });
        } else {
            this.patient = this.patientModel;
            this.bmi = this.calculateBMI(this.patient);
            this.userService.getUser(currentUser.hash).subscribe((response) => {
                this.userService.setMandatoryFields(response[0]);
            });
        }
        
        
    }

    gestionPatient(patient) {
            if (typeof(patient.hash) === 'undefined' || patient.hash === '') {
                patient.birthdate = $('#datePickerField').val();
              /*  let dateValue = $('#datePickerField').val();
                let res = (dateValue as string).split('-');
                patient.birthdate = res[1] + '-' + res[0] + '-' + res[2];
                console.log(res);
                let year = res[2];
                let month = res[1];
                let day = res[0];
                console.log(year);
                console.log(month);
                console.log(day);
                let patientDate = new Date(year, month, day);
                let currentDate = new Date();
                console.log(patientDate);
                console.log(currentDate);
                if ( patientDate > currentDate ) {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Birthdate incorrect'));
                    this.navigateBack();
                } else { */
                    this.patientService.createPatient(this.patient).subscribe((response) => {
                        localStorage.setItem('status', 'success');
                        localStorage.setItem('message', this.translate.instant('Item created with success'));
                        this.navigateBack();

                    }, () => {
                        localStorage.setItem('status', 'danger');
                        localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                        this.navigateBack();
                    });
              //  }
            } else {
                this.patient['birthdate'] = $('#datePickerField').val();
                this.patientService.editPatient(this.patient).subscribe((response) => {
                    localStorage.setItem('status', 'success');
                    localStorage.setItem('message', this.translate.instant('Item created with success'));
                    this.navigateBack();
                }, () => {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                    this.navigateBack();
                });
            }
    }

    delete() {
        let patient = {
            hash: this.hashPatient
        };
        this.patientService.deletePatient(patient).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.navigateBack();
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.navigateBack();
        });
    }

    calculateBMI(patient) {
        let taille = patient.height;
        let poid = patient.weight;
        let bmi = 0;

        if (taille > 0 && poid > 0) {
            bmi = (poid / Math.pow((taille / 100), 2));
        }

        return bmi.toFixed(2);
    }

    navigateBack() {
        if (this.router.url.indexOf('users') !== -1) {
            this.router.navigate(['/users/patients']);
        } else {
            this.router.navigate(['/patients']);
        }
    }

    formatDate() {
        if (this.patient.birthdate) {
            this.patient.birthdate.day =  ("0" + this.patient.birthdate.day).slice(-2);
            this.patient.birthdate.month =  ("0" + this.patient.birthdate.month).slice(-2);
            this.patient.birthdate = this.patient.birthdate.year + '-' + this.patient.birthdate.month + '-' + this.patient.birthdate.day;
        }
    }
}
