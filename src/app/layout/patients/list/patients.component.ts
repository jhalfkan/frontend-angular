import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {PatientService} from '../../../_repositories';

@Component({
    selector: 'app-patients',
    templateUrl: './patients.component.html',
    styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = PatientService.BASEURL + '/';
        this.heading = new Array('id', 'lastname', 'firstname', 'podo');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'patients/patient/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
      //  this.actions.push({'icon': 'fa fa-trash', 'link': 'patients/patient/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
