import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersWaitingLineManageRoutingModule } from './usersWaitingLine-manage-routing.module';
import { UsersWaitingLineManageComponent } from './usersWaitingLine-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersWaitingLineManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [UsersWaitingLineManageComponent]
})
export class UsersWaitingLineManageModule { }





