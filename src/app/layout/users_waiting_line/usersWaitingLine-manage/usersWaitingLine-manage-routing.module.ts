import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersWaitingLineManageComponent } from './usersWaitingLine-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersWaitingLineManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersWaitingLineManageRoutingModule {}
