import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FileService, PatientService, UsersCardsService, UsersWaitingLineService} from '../../../_repositories';
import {Patient, UsersCards, UsersWaitingLine} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-users-patients-manage',
    templateUrl: './usersWaitingLine-manage.component.html',
    styleUrls: ['./usersWaitingLine-manage.component.scss'],
    providers: [UsersWaitingLine]
})
export class UsersWaitingLineManageComponent implements OnInit {
    translate;
    baseURl;
    usersWaitingLineModel = Array<UsersWaitingLine>();
    patient = Patient;
    router;
    activatedRoute;
    status: string;
    hashUsersWaitingLine = Array<any>();
    deleteAction;
    userCardList;

    constructor(translate: TranslateService, private usersWaitingLine: UsersWaitingLineService, routeur: Router, activatedRoute: ActivatedRoute,
                private patientService: PatientService, private userCardService: UsersCardsService, private fileService: FileService) {
        this.translate = translate;
        this.router = routeur;
        this.activatedRoute = activatedRoute;

        $(document).off('click', '.forward-file').on('click', '.forward-file',  (element) => {
            const hash_file = $(element.currentTarget).parent().find('.file_hash').first().val();
            const hash_card = $(element.currentTarget).parent().find('.list_card').first().val();

            this.fileService.attachToCard(hash_file, hash_card).subscribe(result => {
                this.initData();
            });
        });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            const hash = params['hash'];
            this.hashUsersWaitingLine = hash;
        });

        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }

        this.initData();
    }

    delete() {
        this.usersWaitingLine.delete(this.hashUsersWaitingLine).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/users/waitingLine']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/users/waitingLine']);
        });

    }

    initData() {
        if (this.hashUsersWaitingLine !== undefined) {
            this.usersWaitingLine.getUserWaitingLineForPatient(this.hashUsersWaitingLine).subscribe(result => {
                if (typeof(result.result) !== 'undefined' && result.result) {
                    this.usersWaitingLineModel = result.data;
                    console.log(this.usersWaitingLineModel);
                }
            });

            this.patientService.getPatient(this.hashUsersWaitingLine).subscribe(result => {
                console.log(result);
                if (typeof(result) !== 'undefined') {
                    this.patient = result[0];
                    console.log(this.patient);
                }
            });

            this.userCardService.getAll().subscribe(result => {
                if (typeof(result.result) !== 'undefined' && result.result) {
                    this.userCardList = result.datas;
                    console.log(this.userCardList);
                }
            });
        }
    }

}
