import { UsersWaitingLineManageModule } from './usersWaitingLine-manage.module';

describe('UsersWaitingLineManageModule', () => {
  let usersWaitingLineManageModule: UsersWaitingLineManageModule;

  beforeEach(() => {
    usersWaitingLineManageModule = new UsersWaitingLineManageModule();
  });

  it('should create an instance', () => {
    expect(usersWaitingLineManageModule).toBeTruthy();
  });
});
