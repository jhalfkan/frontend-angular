import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersWaitingLineManageComponent } from './usersWaitingLine-manage.component';

describe('UsersWaitingLineManageComponent', () => {
  let component: UsersWaitingLineManageComponent;
  let fixture: ComponentFixture<UsersWaitingLineManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersWaitingLineManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersWaitingLineManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
