import { UsersWaitingLineModule } from './usersWaitingLine.module';

describe('UsersWaitingLineModule', () => {
    let usersWaitingLineModule: UsersWaitingLineModule;

    beforeEach(() => {
        usersWaitingLineModule = new UsersWaitingLineModule();
    });

    it('should create an instance', () => {
        expect(usersWaitingLineModule).toBeTruthy();
    });
});
