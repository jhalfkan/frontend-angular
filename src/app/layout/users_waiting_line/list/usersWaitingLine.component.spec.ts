import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersWaitingLineComponent } from './usersWaitingLine.component';

describe('UsersWaitingLineComponent', () => {
  let component: UsersWaitingLineComponent;
  let fixture: ComponentFixture<UsersWaitingLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersWaitingLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersWaitingLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
