import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersWaitingLineService} from '../../../_repositories';

@Component({
    selector: 'app-users-patients',
    templateUrl: './usersWaitingLine.component.html',
    styleUrls: ['./usersWaitingLine.component.scss']
})
export class UsersWaitingLineComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = UsersWaitingLineService.BASEURL + '/';
        this.heading = new Array('id', 'firstname', 'lastname', 'files');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'users/waitingLine/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'users/waitingLine/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
    }
}
