import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersWaitingLineComponent } from './usersWaitingLine.component';

const routes: Routes = [
    {
        path: '',
        component: UsersWaitingLineComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersWaitingLineRoutingModule {}
