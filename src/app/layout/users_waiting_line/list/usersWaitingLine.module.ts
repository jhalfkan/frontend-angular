import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersWaitingLineRoutingModule } from './usersWaitingLine-routing.module';
import { UsersWaitingLineComponent } from './usersWaitingLine.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersWaitingLineRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [UsersWaitingLineComponent]
})
export class UsersWaitingLineModule {}
