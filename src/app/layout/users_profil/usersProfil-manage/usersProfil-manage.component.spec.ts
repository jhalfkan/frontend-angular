import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersProfilManageComponent } from './usersProfil-manage.component';

describe('UsersProfilManageComponent', () => {
  let component: UsersProfilManageComponent;
  let fixture: ComponentFixture<UsersProfilManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersProfilManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersProfilManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
