import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersProfilManageRoutingModule } from './usersProfil-manage-routing.module';
import { UsersProfilManageComponent } from './usersProfil-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {DayOfTheWeekSelectorComponent} from '../day-of-the-week-selector/day-of-the-week-selector.component';
import {TooltipModule} from '../../../shared/modules';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersProfilManageRoutingModule, ReactiveFormsModule, TooltipModule, FormsModule, NgbModule, NgbAlertModule, UiSwitchModule.forRoot({
          size: 'medium',
          checkedLabel: 'on',
          uncheckedLabel: 'off'
      })
  ],
  declarations: [UsersProfilManageComponent, DayOfTheWeekSelectorComponent]
})
export class UsersProfilManageModule { }





