import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CardService, FileService, UserService, UsersProfilService} from '../../../_repositories';
import {User} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient, HttpEventType, HttpResponse} from '@angular/common/http';

@Component({
    selector: 'app-users-profil-manage',
    templateUrl: './usersProfil-manage.component.html',
    styleUrls: ['./usersProfil-manage.component.scss'],
    providers: [User]
})
export class UsersProfilManageComponent implements OnInit {
    percentDone: number;
    uploadSuccess: boolean;
    urlLogo;

    translate;
    baseURl;
    usersProfilService;
    userService;
    cardService;
    usersProfilModel;
    finishings;
    shippings;
    router;
    status: string;
    idUsersProfil;
    nameUsersProfil;
    currentUser;
    user;
    addressesType;
    selectedDays = [];

    public alerts: Array<any> = [];

    constructor(private http: HttpClient, private fileService: FileService, card: CardService, translate: TranslateService, user: UserService, usersProfil: UsersProfilService, usersProfilModel: User, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.cardService = card;
        this.usersProfilService = usersProfil;
        this.usersProfilModel = usersProfilModel;
        this.router = routeur;
        this.userService = user;
    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
        this.usersProfilService.getUsersProfil().subscribe((response) => {
            this.user = response[0];
            if (this.user.finishing) {
                this.user.finishing = this.user.finishing.id;
            }
            if (this.user.shipping) {
                this.user.shipping = this.user.shipping.id;
            }
            if (this.user.accountBalance) {
                this.user.negativeLimit = this.user.accountBalance.negativeLimit
                this.user.alertLimit = this.user.accountBalance.alertLimit
            }
            this.selectedDays = this.user.shippingDays.split(',');
            this.checkUserValue();
            //this.userService.setMandatoryFields(this.user);

            this.urlLogo = this.usersProfilService.getImageProfil(this.user.hash) + '?timestamp=' + new Date().getTime();
        });
        this.cardService.getAllFinishing().subscribe( (response) => {
            this.finishings = response;
        });
        this.userService.getAddressesType().subscribe((response) => {
            let addressesType = response;
            if (typeof(addressesType) !== 'undefined') {
                this.addressesType = addressesType;
            }
        });
        this.cardService.getAllShipping().subscribe( (response) => {
            this.shippings = response;
        });
    }

    gestionUsersProfil(user) {
        user.shippingDays = this.selectedDays.join(',');
        //this.userService.transformMandatoryFieldsToInteger(this.user, true);
        this.usersProfilService.editProfilUser(user).subscribe((response) => {
            this.closeAlert(alert);
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item edited with success'));
            this.ngOnInit();
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
            this.ngOnInit();
        });
    }

    navigateBack() {
        this.router.navigate(['/']);
    }


    checkUserValue() {
        let company = this.user.companies;
        let address = this.user.address;

        if (company.length === 0) {
            company = {'name': '', 'tvaNumber': ''};
            this.user.companies.push(company);
        }
        if (address.length === 0) {
            address = {'type': '1', 'street': '', 'number': '', 'box': '', 'zip': '', 'city': '', 'country': ''};
            this.user.address = address;
        }
    }

    upload(files: File[]) {
        // pick from one of the 4 styles of file uploads below
        this.uploadAndProgress(files);
    }

    basicUpload(files: File[]){
        var formData = new FormData();
        Array.from(files).forEach(f => formData.append('file', f))
        this.http.post('https://file.io', formData)
            .subscribe(event => {
                console.log('done')
            })
    }

    // this will fail since file.io dosen't accept this type of upload
    // but it is still possible to upload a file with this style
    basicUploadSingle(file: File) {
        this.http.post('https://file.io', file)
            .subscribe(event => {
                console.log('done');
            });
    }

    uploadAndProgress(files: File[]){
        console.log(files)
        let formData = new FormData();
        Array.from(files).forEach(f => formData.append('files', f));

   //     this.fileService.upload(formData)
        this.usersProfilService.upload(formData)
            .subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                } else if (event instanceof HttpResponse) {


                    if (typeof(event.body['result']) !== 'undefined' && event.body['result']) {
                        this.user.newlogo = event.body['data'].id;
                    }
                    this.urlLogo = this.usersProfilService.getImageProfil(this.user.hash) + '?timestamp=' + new Date().getTime();
                    console.log(event.body);
                    this.uploadSuccess = true;
                }
            });
    }

    // this will fail since file.io dosen't accept this type of upload
    // but it is still possible to upload a file with this style
    uploadAndProgressSingle(file: File) {
        this.uploadSuccess = false;
        this.http.post('https://file.io', file, {reportProgress: true, observe: 'events'})
            .subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                } else if (event instanceof HttpResponse) {
                    this.uploadSuccess = true;
                }
            });
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    public copyShippingAddressToBilling() {
        const oldType = { ...this.user.address[1].userAddressType };
        this.user.address[1] = { ...this.user.address[0] };
        this.user.address[1].userAddressType = oldType;
    }

}
