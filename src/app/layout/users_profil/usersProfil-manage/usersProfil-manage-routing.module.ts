import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersProfilManageComponent } from './usersProfil-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersProfilManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersProfilManageRoutingModule {}
