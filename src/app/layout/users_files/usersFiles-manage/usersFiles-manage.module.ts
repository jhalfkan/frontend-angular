import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersFilesManageRoutingModule } from './usersFiles-manage-routing.module';
import { UsersFilesManageComponent } from './usersFiles-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersFilesManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [UsersFilesManageComponent]
})
export class UsersFilesManageModule { }





