import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersFilesService} from '../../../_repositories';
import {UsersFiles} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-users-files-manage',
    templateUrl: './usersFiles-manage.component.html',
    styleUrls: ['./usersFiles-manage.component.scss'],
    providers: [UsersFiles]
})
export class UsersFilesManageComponent implements OnInit {
    translate;
    baseURl;
    usersFilesService;
    usersFilesModel;
    router;
    activatedRoute;
    status: string;
    idUsersFiles;
    nameUsersFiles;

    constructor(translate: TranslateService, usersFiles: UsersFilesService, usersFilesModel: UsersFiles, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.usersFilesService = usersFiles;
        this.usersFilesModel = usersFilesModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idUsersFiles = id;
        });
        if (this.idUsersFiles !== undefined) {
            this.usersFilesService.getUsersFiles(this.idUsersFiles).subscribe((response) => {
                this.nameUsersFiles = response.name;
            });
        }
    }

    gestionUsersFiles(id) {
        if (id === undefined) {
            let usersFiles = {name: this.usersFilesModel.name};
            this.usersFilesService.createUsersFiles(usersFiles).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersFiles']);
            });
        } else {
            let usersFiles = {id: id, name: this.usersFilesModel.name};
            console.log(usersFiles);
            this.usersFilesService.editUsersFiles(usersFiles).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersFiles']);
            });
        }
    }
}
