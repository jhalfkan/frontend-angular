import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersFilesManageComponent } from './usersFiles-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersFilesManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersFilesManageRoutingModule {}
