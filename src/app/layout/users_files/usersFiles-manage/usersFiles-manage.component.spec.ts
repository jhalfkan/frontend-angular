import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFilesManageComponent } from './usersFiles-manage.component';

describe('UsersFilesManageComponent', () => {
  let component: UsersFilesManageComponent;
  let fixture: ComponentFixture<UsersFilesManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFilesManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFilesManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
