import {Component, OnInit} from '@angular/core';
import {UsersFiles} from '../../../_models';
import {UsersFilesService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-usersFiles',
    templateUrl: './usersFiles.component.html',
    styleUrls: ['./usersFiles.component.scss']
})
export class UsersFilesComponent implements OnInit {
    usersFilesService;
    usersFilesModel;
    activatedRoute;
    status: string;

    constructor(usersFiles: UsersFilesService, usersFilesModel: UsersFiles, activatedRoute: ActivatedRoute) {
        this.usersFilesService = usersFiles;
        this.usersFilesModel = usersFilesModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteUsersFiles() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.usersFilesModel.name};

/*        this.usersFilesService.deleteUsersFiles(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
