import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFilesComponent } from './usersFiles.component';

describe('UsersFilesComponent', () => {
  let component: UsersFilesComponent;
  let fixture: ComponentFixture<UsersFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
