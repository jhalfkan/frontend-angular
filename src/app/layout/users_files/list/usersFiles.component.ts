import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersFilesService} from '../../../_repositories';

@Component({
    selector: 'app-usersFiles',
    templateUrl: './usersFiles.component.html',
    styleUrls: ['./usersFiles.component.scss']
})
export class UsersFilesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = UsersFilesService.BASEURL + '/';
        this.heading = new Array('id', 'name', 'size', 'type', 'path');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'users/files/file/:hash', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'ngClick': 'deleteUsersFiles(:id)', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
    }
}
