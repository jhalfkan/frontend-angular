import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersFilesRoutingModule } from './usersFiles-routing.module';
import { UsersFilesComponent } from './usersFiles.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersFilesRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [UsersFilesComponent]
})
export class UsersFilesModule {}
