import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsStatusComponent } from './cardsStatus.component';

const routes: Routes = [
    {
        path: '',
        component: CardsStatusComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CardsStatusRoutingModule {}
