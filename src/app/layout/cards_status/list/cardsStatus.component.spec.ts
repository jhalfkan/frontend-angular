import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsStatusComponent } from './cardsStatus.component';

describe('CardsStatusComponent', () => {
  let component: CardsStatusComponent;
  let fixture: ComponentFixture<CardsStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
