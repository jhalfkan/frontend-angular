import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardsStatusRoutingModule } from './cardsStatus-routing.module';
import { CardsStatusComponent } from './cardsStatus.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, CardsStatusRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [CardsStatusComponent]
})
export class CardsStatusModule {}
