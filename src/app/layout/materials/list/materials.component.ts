import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MaterialService} from '../../../_repositories';

@Component({
    selector: 'app-messages',
    templateUrl: './materials.component.html',
    styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = MaterialService.BASEURL + '/';
        this.heading = new Array('id', 'name', 'stock', 'stockMin', 'isPosturo','isTong', 'position','activated', 'price');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'materials/material/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'materials/material/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
