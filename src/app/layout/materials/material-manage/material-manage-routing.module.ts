import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaterialManageComponent } from './material-manage.component';

const routes: Routes = [
    {
        path: '',
        component: MaterialManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MaterialManageRoutingModule {}
