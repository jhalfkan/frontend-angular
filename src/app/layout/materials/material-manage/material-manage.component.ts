import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MaterialService} from '../../../_repositories';
import {Materials} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-material-manage',
    templateUrl: './material-manage.component.html',
    styleUrls: ['./material-manage.component.scss'],
    providers: [Materials]
})
export class MaterialManageComponent implements OnInit {
    translate;
    baseURl;
    materialService;
    materialModel;
    router;
    activatedRoute;
    status: string;
    deleteAction;
    idMaterial;
    material: Materials = new Materials();
    nbreItem;
    count;

    constructor(translate: TranslateService, material: MaterialService, materialModel: Materials, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.materialService = material;
        this.materialModel = materialModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idMaterial = id;
        });
        this.materialService.getPositionsMaterial().subscribe((response) => {
            let nbreItem = [];
            let count = response;
            if (!this.idMaterial) {
                count ++;
            }
            for (let i = 1; i <= count; i++) {
                nbreItem.push(i);
            }
            this.count = count;
            this.nbreItem = nbreItem;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (this.idMaterial !== undefined) {
            this.materialService.getMaterial(this.idMaterial).subscribe((response) => {
                this.material = response;
            });
        }
    }

    navigateBack() {
        this.router.navigate(['/materials']);
    }

    gestionMaterial(material) {
        if (typeof(material.id) === 'undefined') {
            this.materialService.createMaterial(material).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/materials']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/materials']);
            });
        } else {
            this.materialService.editMaterial(material).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/materials']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/materials']);
            });
        }
    }

    delete() {
        this.materialService.deleteMaterial(this.material).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/materials']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/materials']);
        });
    }
}
