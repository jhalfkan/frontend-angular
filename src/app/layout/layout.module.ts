import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {NgbAlertModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import {MessagesModule} from '../shared/modules';
import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faShoePrints } from '@fortawesome/free-solid-svg-icons';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgSelectModule } from '@ng-select/ng-select';
import { CurrentUserBalanceHistoryComponent } from './current-user-balance-history/current-user-balance-history.component';
import { DatatableModule } from '../shared/modules/datatable/datatable.module';
import { AllUserBalanceHistoryComponent } from './all-user-balance-history/all-user-balance-history.component';
import { UserBalanceHistoryComponent } from './user-balance-history/user-balance-history.component';
import { ManagePodsComponent } from './manage-pods/manage-pods.component';
import { LinkPodoCadComponent } from './link-podo-cad/link-podo-cad.component';

library.add(faShoePrints);

@NgModule({
    imports: [
        NgSelectModule,
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        FormsModule,
        ReactiveFormsModule,
        MessagesModule,
        AngularEditorModule,
        HttpClientModule,
        FontAwesomeModule,
        UiSwitchModule,
        NgbAlertModule,
        DatatableModule
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, CurrentUserBalanceHistoryComponent, AllUserBalanceHistoryComponent, UserBalanceHistoryComponent, ManagePodsComponent, LinkPodoCadComponent],
    providers: [
        DatePipe,
    ],
    exports: []
})
export class LayoutModule {}
