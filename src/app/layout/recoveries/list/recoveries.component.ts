import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {RecoveryService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-recoveries',
    templateUrl: './recoveries.component.html',
    styleUrls: ['./recoveries.component.scss']
})
export class RecoveriesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    router;
    activatedRoute;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, activatedRoute: ActivatedRoute, router: Router) {
        this.translate = translate;
        this.baseURl = RecoveryService.BASEURL + '/';


        this.heading = new Array('id', 'name', 'price');
        this.actions = new Array();
        this.activatedRoute = activatedRoute;
        this.router = router;

        this.actions.push({'icon': 'fa fa-edit', 'link': 'recoveries/recovery/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'recoveries/recovery/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
