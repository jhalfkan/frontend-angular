import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecoveriesComponent } from './recoveries.component';

const routes: Routes = [
    {
        path: '',
        component: RecoveriesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecoveriesRoutingModule {}
