import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveriesComponent } from './recoveries.component';

describe('RecoveriesComponent', () => {
  let component: RecoveriesComponent;
  let fixture: ComponentFixture<RecoveriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
