import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { RecoveriesRoutingModule } from './recoveries-routing.module';
import { RecoveriesComponent } from './recoveries.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, RecoveriesRoutingModule, PageHeaderModule, DatatableModule, NgbAlertModule],
    declarations: [RecoveriesComponent]
})
export class RecoveriesModule {}
