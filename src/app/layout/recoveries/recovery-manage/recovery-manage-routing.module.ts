import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecoveryManageComponent } from './recovery-manage.component';

const routes: Routes = [
    {
        path: '',
        component: RecoveryManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecoveryManageRoutingModule {}
