import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {RecoveryService} from '../../../_repositories';
import {Recovery} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-recovery-manage',
    templateUrl: './recovery-manage.component.html',
    styleUrls: ['./recovery-manage.component.scss'],
    providers: [Recovery]
})
export class RecoveryManageComponent implements OnInit {
    translate;
    baseURl;
    recoveryService;
    recoveryModel;
    router;
    activatedRoute;
    idRecovery;
    recovery;
    deleteAction;

    constructor(translate: TranslateService, recovery: RecoveryService, recoveryModel: Recovery, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.recoveryService = recovery;
        this.recoveryModel = recoveryModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idRecovery = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idRecovery) !== 'undefined') {
            this.recoveryService.getRecovery(this.idRecovery).subscribe((response) => {
                this.recovery = response;
            });
        } else {
            this.recovery = this.recoveryModel;
        }
    }

    navigateBack() {
        this.router.navigate(['/recoveries']);
    }

    gestionRecovery(recovery) {
        if (typeof(recovery.id) === 'undefined') {
            this.recoveryService.createRecovery(this.recovery).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/recoveries']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/recoveries']);
            });
        } else {
            this.recoveryService.editRecovery(this.recovery).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/recoveries']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/recoveries']);
            });
        }
    }

    delete() {
        this.recoveryService.deleteRecovery(this.recovery).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/recoveries']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/recoveries']);
        });
    }
}
