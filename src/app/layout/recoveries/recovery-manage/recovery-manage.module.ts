import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { RecoveryManageRoutingModule } from './recovery-manage-routing.module';
import { RecoveryManageComponent } from './recovery-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, RecoveryManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [RecoveryManageComponent]
})
export class RecoveryManageModule { }





