import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoveryManageComponent } from './recovery-manage.component';

describe('RecoveryManageComponent', () => {
  let component: RecoveryManageComponent;
  let fixture: ComponentFixture<RecoveryManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoveryManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoveryManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
