import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CardService, FileService, UserService, UsersProfilService} from '../../../_repositories';
import {User} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {HttpClient, HttpEventType, HttpResponse} from '@angular/common/http';
import * as $ from "jquery";

@Component({
    selector: 'app-user-manage',
    templateUrl: './user-manage.component.html',
    styleUrls: ['./user-manage.component.scss'],
    providers: [User]
})
export class UserManageComponent implements OnInit {
    urlLogo;
    forceshowImage;
    translate;
    baseURl;
    userService;
    userModel;
    router;
    activatedRoute;
    finishings;
    shippings;
    cardService;
    hashUser;
    deleteAction;
    addressesType;
    user: User;
    roles;
    actualType;

    uploadSuccess;
    percentDone;
    usersProfilService;
    selectedDays = [];
    shippingId;
    finishingId;
    cads = [];
    podoCadHash;

    constructor(private http: HttpClient, private fileService: FileService, card: CardService, translate: TranslateService, user: UserService, usersProfil: UsersProfilService, userModel: User, routeur: Router, activatedRoute: ActivatedRoute) {
        this.usersProfilService = usersProfil;
        this.translate = translate;
        this.cardService = card;
        this.userService = user;
        this.userModel = userModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;

        this.userService.getRoles().subscribe( (response) => {
            this.roles = response;
        });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let hash = params['hash'];
            this.hashUser = hash;
        });
        this.userService.getAddressesType().subscribe((response) => {
            let addressesType = response;
            if (typeof(addressesType) !== 'undefined') {
                this.addressesType = addressesType;
            }
        });
        this.userService.getPodoCad().subscribe(
            response => {
              this.cads = response.datas;
            }
        );
        this.cardService.getAllFinishing().subscribe( (response) => {
            this.finishings = response;
        });
        this.cardService.getAllShipping().subscribe((response) => {
            this.shippings = response;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.hashUser) !== 'undefined') {
            this.userService.getUser(this.hashUser).subscribe((response) => {
                this.user = response[0];
                if (this.user.podoCad) {
                    this.podoCadHash = this.user.podoCad;
                }
                this.selectedDays = response[0].shippingDays.split(',');
                if (response[0].finishing) {
                    this.finishingId = response[0].finishing.id;
                }
                if (response[0].shipping) {
                    this.shippingId = response[0].shipping.id;
                }
                if (this.user.accountBalance) {
                    this.user.negativeLimit = this.user.accountBalance.negativeLimit
                    this.user.alertLimit = this.user.accountBalance.alertLimit
                }
                this.userService.resetMandatoryFields();
                this.userService.setMandatoryFields(this.user);
                console.log('user set');
                this.checkUserValue();
            });
        } else {
            this.user = this.userModel;
            this.userService.resetMandatoryFields();
        }

        if (typeof(this.hashUser) !== 'undefined') {
            this.urlLogo = this.usersProfilService.getImageProfil(this.hashUser);
            console.log( this.urlLogo);
        }
    }

    navigateBack() {
        this.router.navigate(['/users']);
    }

    gestionUser() {
        this.user.podoCadHash = this.podoCadHash;
        this.user.shippingDays = this.selectedDays.join(',');
        this.user.finishing = this.finishingId;
        this.user.shipping = this.shippingId;
        console.log(this.user)
        const isUpdating = typeof(this.user.hash) !== 'undefined';
        this.userService.transformMandatoryFieldsToInteger(this.user, isUpdating);
        if (!isUpdating) {
            this.user = this.formatAddress();
            this.userService.createUser(this.user).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/users']);
            }, (error) => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', error.error);
                this.router.navigate(['/users']);
            });
        } else {
            this.userService.editUser(this.user).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/users']);
            }, (error) => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', error.error);
                this.router.navigate(['/users']);
            });
        }
    }

    delete() {
        let user = {
            hash: this.hashUser
        };
        this.userService.deleteUser(user).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/users']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/users']);
        });
    }

    checkUserValue() {
        let company = this.user.companies;
        let address = this.user.address;

        if (company.length === 0) {
            company = {'name': '', 'tvaNumber': ''};
            this.user.companies.push(company);
        }
        if (address.length === 0) {
            address = {'type': '1', 'street': '', 'number': '', 'box': '', 'zip': '', 'city': '', 'country': ''};
            this.user.address.push(address);
        }
    }

    upload(files: File[]) {
        // pick from one of the 4 styles of file uploads below
        this.uploadAndProgress(files);
    }

    basicUpload(files: File[]){
        var formData = new FormData();
        Array.from(files).forEach(f => formData.append('file', f))
        this.http.post('https://file.io', formData)
            .subscribe(event => {
                console.log('done');
            });
    }

    // this will fail since file.io dosen't accept this type of upload
    // but it is still possible to upload a file with this style
    basicUploadSingle(file: File) {
        this.http.post('https://file.io', file)
            .subscribe(event => {
                console.log('done');
            });
    }

    uploadAndProgress(files: File[]){
        console.log(files)
        let formData = new FormData();
        Array.from(files).forEach(f => formData.append('files', f));


        if (typeof(this.hashUser) === 'undefined') {
            this.fileService.upload(formData)
            // this.usersProfilService.upload(formData)
                .subscribe(event => {
                    if (event.type === HttpEventType.UploadProgress) {
                        this.percentDone = Math.round(100 * event.loaded / event.total);
                    } else if (event instanceof HttpResponse) {


                        if (typeof(event.body['result']) !== 'undefined' && event.body['result']) {
                            this.user.newlogo = event.body['data'].id;
                            console.log(event.body['data'].hash);

                            this.fileService.download(event.body['data'].hash).subscribe(response => {
                                console.log(response);
                                if (response.ok) {
                                    this.forceshowImage = true;
                                    const data = window.URL.createObjectURL(response.body);
                                    this.urlLogo = data;
                                    setTimeout(() => {
                                        $('#image_logo').attr('src', data);
                                    }, 100);

                                }

                            }, error => {
                                console.log(error);
                            });
                        }
                        console.log(event.body);
                        this.uploadSuccess = true;
                    }
                });
        } else {
            // this.fileService.upload(formData)
            this.usersProfilService.uploadSpecUser(this.hashUser, formData)
                .subscribe(event => {
                    if (event.type === HttpEventType.UploadProgress) {
                        this.percentDone = Math.round(100 * event.loaded / event.total);
                    } else if (event instanceof HttpResponse) {


                        if (typeof(event.body['result']) !== 'undefined' && event.body['result']) {
                            this.user.newlogo = event.body['data'].id;


                        }

                        this.urlLogo = this.usersProfilService.getImageProfil(this.user.hash) + '?timestamp=' + new Date().getTime();
                         console.log(event.body);
                        this.uploadSuccess = true;
                    }
                });
        }


    }

    // this will fail since file.io dosen't accept this type of upload
    // but it is still possible to upload a file with this style
    uploadAndProgressSingle(file: File) {
        this.uploadSuccess = false;
        this.http.post('https://file.io', file, {reportProgress: true, observe: 'events'})
            .subscribe(event => {
                if (event.type === HttpEventType.UploadProgress) {
                    this.percentDone = Math.round(100 * event.loaded / event.total);
                } else if (event instanceof HttpResponse) {
                    this.uploadSuccess = true;
                }
            });
    }

    formatAddress() {
        this.user.address = [];
        let address_cp = {'type': '2', 'street': this.user.address_cp_street, 'number': this.user.address_cp_number, 'box': this.user.address_cp_box, 'zip': this.user.address_cp_zip, 'city': this.user.address_cp_city, 'country': this.user.address_cp_country};
        let address_pr = {'type': '1', 'street': this.user.address_pr_street, 'number': this.user.address_pr_number, 'box': this.user.address_pr_box, 'zip': this.user.address_pr_zip, 'city': this.user.address_pr_city, 'country': this.user.address_pr_country};
        this.user.address.push(address_cp);
        this.user.address.push(address_pr);

        return this.user;
    }

    copyShippingAddressToBillingInCreation() {
        this.user.address_cp_type = this.user.address_pr_type;
        this.user.address_cp_street = this.user.address_pr_street;
        this.user.address_cp_number = this.user.address_pr_number;
        this.user.address_cp_box = this.user.address_pr_box;
        this.user.address_cp_zip = this.user.address_pr_zip;
        this.user.address_cp_city = this.user.address_pr_city;
        this.user.address_cp_country = this.user.address_pr_country;
    }

    copyShippingAddressToBillingInEdition() {
        const oldType = { ...this.user.address[1].userAddressType };
        this.user.address[1] = { ...this.user.address[0] };
        this.user.address[1].userAddressType = oldType;
    }
}
