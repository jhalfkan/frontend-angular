import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UserManageRoutingModule } from './user-manage-routing.module';
import { UserManageComponent } from './user-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import {DayOfTheWeekSelectorComponent} from './day-of-the-week-selector/day-of-the-week-selector.component';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UserManageRoutingModule, ReactiveFormsModule, FormsModule, UiSwitchModule.forRoot({
          size: 'medium',
          checkedLabel: 'on',
          uncheckedLabel: 'off'
      })
  ],
  declarations: [UserManageComponent, DayOfTheWeekSelectorComponent]
})
export class UserManageModule { }





