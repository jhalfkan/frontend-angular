import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../_repositories';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  baseURl;
  actions;
  heading;
  public alerts: Array<any> = [];

  constructor() {
      this.baseURl = UserService.BASEURL;
      this.heading = new Array('id', 'firstname', 'lastname', 'totalCards', 'balance');
      this.actions = new Array();

      this.actions.push({
          'icon': 'fa fa-edit',
          'link': 'users/user/:hash/edit',
          'button': 'btn btn-info btn-sm',
          'action': 'edit'
      });
      this.actions.push({
          'icon': 'fa fa-file',
          'link': 'cards?user=:hash',
          'button': 'btn btn-primary btn-sm',
          'action': 'viewCards'
      });
      this.actions.push({
          'icon': 'fa fa-trash',
          'link': 'users/user/:hash/delete',
          'button': 'btn btn-danger btn-sm',
          'action': 'delete'
      });
      this.actions.push({
          'icon': 'fa fa-history',
          'link': 'user-balance-history/:hash',
          'button': 'btn btn-primary btn-sm',
          'action': 'balance-history'
      })
      this.actions.push({
          'icon': 'fa fa-money',
          'link': 'manage-pods/:hash',
          'button': 'btn btn-info btn-sm',
          'action': 'manage-pods'
      })
      this.actions.push({
        'icon': 'fa fa-file-excel-o',
        'button': 'btn btn-secondary btn-sm',
        'action': 'pods-excels'
      })
  }

  ngOnInit() {
      if (localStorage.getItem('status')) {
          this.alerts.push({
              type: localStorage.getItem('status'),
              message: localStorage.getItem('message')
          });
          localStorage.removeItem('status');
          localStorage.removeItem('message');
      }
  }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
