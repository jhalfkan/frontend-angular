import {Component, OnInit} from '@angular/core';
import {User} from '../../../_models';
import {UserService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
    userService;
    userModel;
    activatedRoute;
    status: string;

    constructor(user: UserService, userModel: User, activatedRoute: ActivatedRoute) {
        this.userService = user;
        this.userModel = userModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteUser() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.userModel.name};

/*        this.userService.deleteUser(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
