import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FilesRoutingModule } from './files-routing.module';
import { FilesComponent } from './files.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, FilesRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [FilesComponent]
})
export class FilesModule {}
