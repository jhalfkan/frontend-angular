import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FileService} from '../../../_repositories';

@Component({
    selector: 'app-files',
    templateUrl: './files.component.html',
    styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = FileService.BASEURL + '/';
        this.heading = new Array('id', 'name', 'size', 'type', 'path');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'files/file/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'ngClick': 'deleteFile(:id)', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
    }
}
