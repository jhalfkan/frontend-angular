import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FilesRoutingModule } from './files-routing.module';
import { FilesComponent } from './files.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, FilesRoutingModule, PageHeaderModule],
    declarations: [FilesComponent]
})
export class FilesModule {}
