import {Component, OnInit} from '@angular/core';
import {File} from '../../../_models';
import {FileService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-files',
    templateUrl: './files.component.html',
    styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit {
    fileService;
    fileModel;
    activatedRoute;
    status: string;

    constructor(file: FileService, fileModel: File, activatedRoute: ActivatedRoute) {
        this.fileService = file;
        this.fileModel = fileModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteFile() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.fileModel.name};

/*        this.fileService.deleteFile(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
