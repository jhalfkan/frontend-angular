import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FileManageRoutingModule } from './file-manage-routing.module';
import { FileManageComponent } from './file-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, FileManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [FileManageComponent]
})
export class FileManageModule { }





