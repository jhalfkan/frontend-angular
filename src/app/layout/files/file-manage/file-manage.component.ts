import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FileService} from '../../../_repositories';
import {File} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-file-manage',
    templateUrl: './file-manage.component.html',
    styleUrls: ['./file-manage.component.scss'],
    providers: [File]
})
export class FileManageComponent implements OnInit {
    translate;
    baseURl;
    fileService;
    fileModel;
    router;
    activatedRoute;
    status: string;
    idFile;
    nameFile;

    constructor(translate: TranslateService, file: FileService, fileModel: File, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.fileService = file;
        this.fileModel = fileModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idFile = id;
        });
        if (this.idFile !== undefined) {
            this.fileService.getFile(this.idFile).subscribe((response) => {
                this.nameFile = response.name;
            });
        }
    }

    gestionFile(id) {
        if (id === undefined) {
            let file = {name: this.fileModel.name};
            this.fileService.createFile(file).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/files']);
            });
        } else {
            let file = {id: id, name: this.fileModel.name};
            console.log(file);
            this.fileService.editFile(file).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/files']);
            });
        }
    }
}
