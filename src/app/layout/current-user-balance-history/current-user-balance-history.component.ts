import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-current-user-balance-history',
  templateUrl: './current-user-balance-history.component.html',
  styleUrls: ['./current-user-balance-history.component.scss']
})
export class CurrentUserBalanceHistoryComponent implements OnInit {
  public baseURl = 'balance-history';
  public heading = ['amount', 'createdAt', 'cardOrBill', 'other'];
  constructor(private datePipe: DatePipe, private translateService: TranslateService) { }

  ngOnInit() {
  }

  public formatDate = (balanceHistoryLines) => {
    for (let i = 0; i < balanceHistoryLines.length; ++i) {
      balanceHistoryLines[i].createdAt = this.datePipe.transform(
        balanceHistoryLines[i].createdAt, 'dd/MM/yyyy HH:mm'
      );
      balanceHistoryLines[i].cardOrBill = this.populateCardIdOrBill(balanceHistoryLines[i])
      balanceHistoryLines[i].amount = this.addAmountSign(balanceHistoryLines[i].amount)
    }
    return balanceHistoryLines
  }

  public populateCardIdOrBill (balanceHistoryLine) {
    if (balanceHistoryLine.cardId) {
      return this.translateService.instant('cardId') + ': ' + balanceHistoryLine.cardId
    }
    return balanceHistoryLine.bill
  }

  public addAmountSign (value) {
    if (value < 0) {
      return value
    }
    return "+ " + value
  }

}
