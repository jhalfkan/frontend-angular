import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentUserBalanceHistoryComponent } from './current-user-balance-history.component';

describe('CurrentUserBalanceHistoryComponent', () => {
  let component: CurrentUserBalanceHistoryComponent;
  let fixture: ComponentFixture<CurrentUserBalanceHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentUserBalanceHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentUserBalanceHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
