import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {AuthenticationService} from '../../../_services';
import {SidebarComponent} from '../sidebar/sidebar.component';
import {first} from 'rxjs/operators';
import {LoginService} from '../../../_repositories';
import {DatatableService} from '../../../shared/services/DatatableService';

@Component({
    providers: [SidebarComponent],
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = 'push-right';
    username: string = '';
    isDemoAccount: boolean;

    constructor(
        private translate: TranslateService,
        private dataTableService: DatatableService,
        public router: Router,
        private authentificat: AuthenticationService,
        private sidebarComponent: SidebarComponent,
        private loginService: LoginService
    ) {
        this.username = authentificat.getFirstname() + ' ' + authentificat.getLastname();
        this.isDemoAccount = authentificat.getRoles().includes('ROLE_DEMO')
    }

    ngOnInit() {
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        this.sidebarComponent.reducMenu(!this.sidebarComponent.getStatutMenu(), true);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this.loginService.logout().pipe(first())
            .subscribe(
                data => {
                    localStorage.removeItem('currentUser');
                },
                error => {
                    localStorage.removeItem('currentUser');
                });

    }

    changeLang(language: string) {
        this.translate.use(language);
        localStorage.setItem('lang', language);

        this.dataTableService.filter('realooadHeading', {});
        setTimeout(() => {
            this.dataTableService.filter('Reinitialize', {});
        }, 100);
    }

    public getCurrentBalance(): Number {
        return this.authentificat.getCurrentBalance()
    }
}
