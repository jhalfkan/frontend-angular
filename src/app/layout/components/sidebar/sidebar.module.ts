import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faShoePrints,  } from '@fortawesome/free-solid-svg-icons';
import { UiSwitchModule } from 'ngx-ui-switch';

library.add(faShoePrints);

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        NgbDropdownModule,
        FormsModule,
        ReactiveFormsModule,
        AngularEditorModule,
        HttpClientModule,
        FontAwesomeModule,
        UiSwitchModule
    ],
    declarations: []
})
export class LayoutModule {}
