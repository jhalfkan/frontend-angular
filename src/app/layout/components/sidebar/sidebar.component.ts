import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {AuthenticationService} from '../../../_services';
import * as $ from 'jquery';
import {DatatableService} from '../../../shared/services/DatatableService';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    isActive: boolean = false;
    username: string = '';
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    userRoles: Array<string> = [];

    constructor(private translate: TranslateService, private dataTableService: DatatableService, public router: Router, authentificat: AuthenticationService) {
        this.userRoles = authentificat.getRoles();
        this.username = authentificat.getFirstname() + ' ' + authentificat.getLastname();
    }

    ngOnInit() {
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleTexteSideBar() {

    }

    getStatutMenu() {
        let statut = localStorage.getItem('menu_short');
        return (statut === '1');
    }

    reducMenu(action: boolean = true , save_statut: boolean = false) {
        if(action) {
            if(save_statut){
                localStorage.setItem('menu_short', '1');
            }

            $('body').removeClass(this.pushRightClass);
            $('.sidebar').addClass('sidebar_short');
            $('.main-container').addClass('main-container_large');

            console.log('add reduct');
        }
        else {
            if(save_statut){
                localStorage.setItem('menu_short', '0');
            }
            $('body').addClass(this.pushRightClass);
            $('.sidebar').removeClass('sidebar_short');
            $('.main-container').removeClass('main-container_large');
        }
    }



    public toggleSidebar() {
        this.reducMenu(!this.getStatutMenu(), true);
    }

    public toggleSidebarSmallScreen() {
        if (window.innerWidth < 992) {
            this.reducMenu(!this.getStatutMenu(), true);
        }
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
        localStorage.setItem('lang', language);
        this.dataTableService.filter('realooadHeading', {});
    }

    onLoggedout() {
        localStorage.removeItem('currentUser');
    }
}
