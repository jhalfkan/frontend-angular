import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersCardsTemplatesComponent } from './usersCardsTemplates.component';

describe('UsersCardsTemplatessComponent', () => {
  let component: UsersCardsTemplatesComponent;
  let fixture: ComponentFixture<UsersCardsTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersCardsTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersCardsTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
