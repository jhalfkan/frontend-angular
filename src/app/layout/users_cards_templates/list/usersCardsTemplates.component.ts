import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersCardsTemplatesService} from '../../../_repositories';

@Component({
    selector: 'app-usersCardsTemplates',
    templateUrl: './usersCardsTemplates.component.html',
    styleUrls: ['./usersCardsTemplates.component.scss']
})
export class UsersCardsTemplatesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = UsersCardsTemplatesService.BASEURL + '/';
        this.heading = new Array('id', 'nameTemplate');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'users/cards/templates/template/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'users/cards/templates/template/:hash/delete', 'button': 'btn btn-fiche delete btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
    }
}
