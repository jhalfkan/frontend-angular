import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersCardsTemplatesRoutingModule } from './usersCardsTemplates-routing.module';
import { UsersCardsTemplatesComponent } from './usersCardsTemplates.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersCardsTemplatesRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [UsersCardsTemplatesComponent]
})
export class UsersCardsTemplatesModule {}
