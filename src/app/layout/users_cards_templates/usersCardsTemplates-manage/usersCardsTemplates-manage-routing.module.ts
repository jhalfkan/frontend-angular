import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersCardsTemplatesManageComponent } from './usersCardsTemplates-manage.component';

const routes: Routes = [
    {
        path: '',
        component: UsersCardsTemplatesManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersCardsTemplatesManageRoutingModule {}
