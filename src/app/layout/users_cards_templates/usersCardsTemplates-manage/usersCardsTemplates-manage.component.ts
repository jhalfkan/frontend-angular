import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersCardsTemplatesService} from '../../../_repositories';
import {UsersCardsTemplates} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-usersCardsTemplates-manage',
    templateUrl: './usersCardsTemplates-manage.component.html',
    styleUrls: ['./usersCardsTemplates-manage.component.scss'],
    providers: [UsersCardsTemplates]
})
export class UsersCardsTemplatesManageComponent implements OnInit {
    translate;
    baseURl;
    usersCardsTemplatesService;
    usersCardsTemplatesModel;
    router;
    activatedRoute;
    status: string;
    idUsersCardsTemplates;
    nameUsersCardsTemplates;

    constructor(translate: TranslateService, usersCardsTemplates: UsersCardsTemplatesService, usersCardsTemplatesModel: UsersCardsTemplates, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.usersCardsTemplatesService = usersCardsTemplates;
        this.usersCardsTemplatesModel = usersCardsTemplatesModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idUsersCardsTemplates = id;
        });
        if (this.idUsersCardsTemplates !== undefined) {
            this.usersCardsTemplatesService.getUsersCardsTemplates(this.idUsersCardsTemplates).subscribe((response) => {
                this.nameUsersCardsTemplates = response.name;
            });
        }
    }

    gestionUsersCardsTemplates(id) {
        if (id === undefined) {
            let usersCardsTemplates = {name: this.usersCardsTemplatesModel.name};
            this.usersCardsTemplatesService.createUsersCardsTemplates(usersCardsTemplates).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersCardsTemplates']);
            });
        } else {
            let usersCardsTemplates = {id: id, name: this.usersCardsTemplatesModel.name};
            console.log(usersCardsTemplates);
            this.usersCardsTemplatesService.editUsersCardsTemplates(usersCardsTemplates).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/usersCardsTemplates']);
            });
        }
    }
}
