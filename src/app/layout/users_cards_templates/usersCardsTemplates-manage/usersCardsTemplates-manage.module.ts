import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersCardsTemplatesManageRoutingModule } from './usersCardsTemplates-manage-routing.module';
import { UsersCardsTemplatesManageComponent } from './usersCardsTemplates-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, UsersCardsTemplatesManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [UsersCardsTemplatesManageComponent]
})
export class UsersCardsTemplatesManageModule { }





