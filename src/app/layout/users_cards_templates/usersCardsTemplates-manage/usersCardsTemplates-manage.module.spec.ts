import { UsersCardsTemplatesManageModule } from './usersCardsTemplates-manage.module';

describe('UsersCardsTemplatesManageModule', () => {
  let usersCardsTemplatesManageModule: UsersCardsTemplatesManageModule;

  beforeEach(() => {
    usersCardsTemplatesManageModule = new UsersCardsTemplatesManageModule();
  });

  it('should create an instance', () => {
    expect(usersCardsTemplatesManageModule).toBeTruthy();
  });
});
