import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersCardsTemplatesManageComponent } from './usersCardsTemplates-manage.component';

describe('UsersCardsTemplatesManageComponent', () => {
  let component: UsersCardsTemplatesManageComponent;
  let fixture: ComponentFixture<UsersCardsTemplatesManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersCardsTemplatesManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersCardsTemplatesManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
