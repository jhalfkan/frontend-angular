import {Component, OnInit} from '@angular/core';
import {UsersCardsTemplates} from '../../../_models';
import {UsersCardsTemplatesService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-usersCardsTemplates',
    templateUrl: './usersCardsTemplates.component.html',
    styleUrls: ['./usersCardsTemplates.component.scss']
})
export class UsersCardsTemplatesComponent implements OnInit {
    usersCardsTemplatesService;
    usersCardsTemplatesModel;
    activatedRoute;
    status: string;

    constructor(usersCardsTemplates: UsersCardsTemplatesService, usersCardsTemplatesModel: UsersCardsTemplates, activatedRoute: ActivatedRoute) {
        this.usersCardsTemplatesService = usersCardsTemplates;
        this.usersCardsTemplatesModel = usersCardsTemplatesModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteUsersCardsTemplates() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.usersCardsTemplatesModel.name};

/*        this.usersCardsTemplatesService.deleteUsersCardsTemplates(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
