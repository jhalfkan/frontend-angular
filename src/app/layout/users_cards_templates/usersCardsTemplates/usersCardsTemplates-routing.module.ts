import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersCardsTemplatesComponent } from './usersCardsTemplates.component';

const routes: Routes = [
    {
        path: '',
        component: UsersCardsTemplatesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersCardsTemplatesRoutingModule {}
