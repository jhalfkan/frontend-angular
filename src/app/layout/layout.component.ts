import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LayoutService} from '../_services/LayoutService';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    translate;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService, private _layoutService: LayoutService) {
        this.translate = translate;
        const browserLang = this.translate.getBrowserLang();
        let languageSave = localStorage.getItem('lang');

        if (languageSave != null && languageSave.match(/en|fr|es/)) {
            this.translate.use(languageSave);
        }
        else {
            this.translate.use(browserLang.match(/en|fr|es/) ? browserLang : 'fr');
        }

        this._layoutService.listen().subscribe((m: any) => {
            switch (m.key) {
                case 'addAlert':
                    this.addAlert(m.value.status, m.value.message);
                    break;

                case 'removeAllAlert':
                    this.alerts = [];
                    break;
            }
        });

    }

    ngOnInit() {
    }

    addAlert(status, message) {
        this.alerts.push({
            type: status,
            message: message
        });
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
