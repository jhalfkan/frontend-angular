import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { UsersCardsStatusRoutingModule } from './usersCardsStatus-routing.module';
import { UsersCardsStatusComponent } from './usersCardsStatus.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, UsersCardsStatusRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [UsersCardsStatusComponent]
})
export class UsersCardsStatusModule {}
