import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UsersCardsStatusService} from '../../../_repositories';

@Component({
    selector: 'app-cards-status',
    templateUrl: './usersCardsStatus.component.html',
    styleUrls: ['./usersCardsStatus.component.scss']
})
export class UsersCardsStatusComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    status;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = UsersCardsStatusService.BASEURL + '/';
        this.heading = new Array('id', 'name');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'cards/card/:hash', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-eye', 'link': 'cards/card/:hash/cards', 'button': 'btn btn-primary btn-sm', 'action': 'checkCards'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'cards/card/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});
    }

    ngOnInit() {
    }
}
