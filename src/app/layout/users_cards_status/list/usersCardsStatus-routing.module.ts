import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersCardsStatusComponent } from './usersCardsStatus.component';

const routes: Routes = [
    {
        path: '',
        component: UsersCardsStatusComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UsersCardsStatusRoutingModule {}
