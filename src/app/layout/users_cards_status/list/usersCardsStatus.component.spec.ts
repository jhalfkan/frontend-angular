import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersCardsStatusComponent } from './usersCardsStatus.component';

describe('UsersCardsStatusComponent', () => {
  let component: UsersCardsStatusComponent;
  let fixture: ComponentFixture<UsersCardsStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersCardsStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersCardsStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
