import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { StatusRoutingModule } from './status-routing.module';
import { StatusComponent } from './status.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, StatusRoutingModule, PageHeaderModule],
    declarations: [StatusComponent]
})
export class StatusModule {}
