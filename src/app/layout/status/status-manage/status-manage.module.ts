import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { StatusManageRoutingModule } from './status-manage-routing.module';
import { StatusManageComponent } from './status-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, StatusManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [StatusManageComponent]
})
export class StatusManageModule { }





