import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {StatusService} from '../../../_repositories';
import {Status} from '../../../_models';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-status-manage',
    templateUrl: './status-manage.component.html',
    styleUrls: ['./status-manage.component.scss'],
    providers: [Status]
})
export class StatusManageComponent implements OnInit {
    translate;
    baseURl;
    statusService;
    statusModel;
    router;
    activatedRoute;
    idStatus;
    status;
    deleteAction;

    constructor(translate: TranslateService, status: StatusService, statusModel: Status, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.statusService = status;
        this.statusModel = statusModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idStatus = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (typeof(this.idStatus) !== 'undefined') {
            this.statusService.getStatus(this.idStatus).subscribe((response) => {
                this.status = response;
            });
        } else {
            this.status = this.statusModel;
        }
    }

    navigateBack() {
        this.router.navigate(['/status']);
    }

    gestionStatus(id) {
        if (id === undefined) {
            this.statusService.createStatus(this.status).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/status']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/status']);
            });
        } else {
            this.statusService.editStatus(this.status).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/status']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/status']);
            });
        }
    }

    delete() {
        this.statusService.deleteStatus(this.status).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/status']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/status']);
        });
    }
}
