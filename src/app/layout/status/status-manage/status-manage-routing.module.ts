import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatusManageComponent } from './status-manage.component';

const routes: Routes = [
    {
        path: '',
        component: StatusManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StatusManageRoutingModule {}
