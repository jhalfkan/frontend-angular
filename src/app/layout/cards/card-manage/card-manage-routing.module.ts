import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardManageComponent } from './card-manage.component';

const routes: Routes = [
    {
        path: '',
        component: CardManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CardManageRoutingModule {}
