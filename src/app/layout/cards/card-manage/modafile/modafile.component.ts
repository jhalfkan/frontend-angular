import {Component, Input} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FileUploader } from 'ng2-file-upload';
import * as $ from 'jquery';
import {CardService, FileService} from '../../../../_repositories';
import {AuthenticationService} from '../../../../_services';
import {UsersWaitingLine} from '../../../../_models';

// const URL = '/api/';
const URL = 'http://api.gespodo.localhost.test/app_dev.php/api/cards/2c0f2f58cbb772cb8450dabb054f1313fa344674bc697a94/file/upload';

@Component({
    selector: 'app-modal-file',
    templateUrl: './modafile.component.html',
    styleUrls: ['./modafile.component.scss']
})


export class ModalFileComponent {
    @Input() hashCard;
    @Input() hashPatient;

    waitingList = Array<UsersWaitingLine>();
    closeResult: string;
    files: Array<any> = Array<any>();
    loop;
    loopwaitingFile;
    public uploader: FileUploader;

    constructor(private fileService: FileService, private modalService: NgbModal, private cardService: CardService, private auth: AuthenticationService) {
    }

    ngAfterViewInit() {
        this.uploader = new FileUploader({url: this.cardService.uploadfilesRoute(this.hashCard), headers: [{
                name: 'X-Auth-Token',
                value: this.auth.getToken()
            }]});
    }

    open(content) {
        this.getFiles(this.hashCard);
        this.loop = setInterval( () => {
            this.getFiles(this.hashCard);
        }, 10000);


        this.getWaitingLine();
        this.loopwaitingFile = setInterval( () => {
            this.getWaitingLine();
        }, 10000);

        this.modalService.open(content, { size: 'lg', centered: true }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
            clearTimeout( this.loop);
            clearTimeout( this.loopwaitingFile);
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            clearTimeout( this.loop);
            clearTimeout( this.loopwaitingFile);
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    getWaitingLine() {
        if(this.hashPatient) {
            this.cardService.getWaitingLine(this.hashPatient).subscribe(result => {
                console.log(result);
                if (typeof(result.result) !== 'undefined' && result.result) {
                    this.waitingList = result.data;

                    console.log(this.waitingList);
                }

        });
        }
    }


    delete(hashfiles) {
        this.fileService.delete(hashfiles).subscribe(response => {
            this.getFiles(this.hashCard);
        }, error => {
            this.getFiles(this.hashCard);
        });
    }

    forwardToCard(hash) {
        console.log('forward');
        this.fileService.attachToCard(hash, this.hashCard).subscribe(result => {
            console.log(result);
        });
    }

    download(filename, hashfiles) {
        document.body.style.cursor = 'progress';
        $('.btn').css('cursor', 'progress');

        this.fileService.download(hashfiles).subscribe(response => {
            const data = window.URL.createObjectURL(response.body);
            var link = document.createElement('a');
            link.href = data;
            link.target = "_blank";


            link.download = filename;
            document.body.appendChild(link);
            link.click();
            setTimeout(function() {
                // For Firefox it is necessary to delay revoking the ObjectURL
                document.body.removeChild(link);
                window.URL.revokeObjectURL(data);

                document.body.style.cursor = 'default';
                $('.btn').css('cursor', 'pointer');
            },  100);


            document.body.style.cursor = 'default';
            $('.btn').css('cursor', 'pointer');
        }, error => {
            document.body.style.cursor = 'default';
            $('.btn').css('cursor', 'pointer');
        });
    }

    getFiles(hashcard) {
    //    console.log(this.uploader);

        return this.cardService.getFiles(hashcard).subscribe((response) => {
            if (response.data.length !== 0) {
                this.files = response.data;
            } else {
                this.files = new Array<any>();
            }

            console.log(this.files);
        });
    }
}
