import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardManageRoutingModule } from './card-manage-routing.module';
import { CardManageComponent } from './card-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import {NgbAlertModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from './modal/modal.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import {FileUploadModule} from 'ng2-file-upload';
import {AddPatientModalComponent} from './modal-add-patient/addPatientModal.component';
import {NgbdDatepickerI18n} from './datepicker-popup';
import {ModalFileModule} from './modafile/modafile.module';
import { NgSelectModule } from '@ng-select/ng-select';
import {DayOfTheWeekSelectorComponent} from './day-of-the-week-selector/day-of-the-week-selector.component';
import {TooltipModule} from '../../../shared/modules';


@NgModule({
  imports: [
      NgbModule,
      CommonModule, Ng2Charts, NgSelectModule, FileUploadModule, TranslateModule, ModalFileModule, CardManageRoutingModule, ReactiveFormsModule, FormsModule, QRCodeModule, TooltipModule, NgbAlertModule, UiSwitchModule.forRoot({
          size: 'medium',
          checkedLabel: 'oui',
          uncheckedLabel: 'non'
      })
  ],
  declarations: [CardManageComponent, ModalComponent, AddPatientModalComponent, NgbdDatepickerI18n, DayOfTheWeekSelectorComponent]
})
export class CardManageModule { }






