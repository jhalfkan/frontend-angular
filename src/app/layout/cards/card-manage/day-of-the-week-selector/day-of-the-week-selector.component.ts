import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-day-of-the-week-selector',
  templateUrl: './day-of-the-week-selector.component.html',
  styleUrls: ['./day-of-the-week-selector.component.scss']
})
export class DayOfTheWeekSelectorComponent implements OnInit {

  @Input() selectedDays = [];
  @Input() shouldAvoidUpdatingDays;
  daysOfTheWeek = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

  constructor() { }

  ngOnInit() {
  }

  isThisDaySelected(day) {
    if (this.selectedDays.includes(day)) {
      return true;
    }
    return false;
  }

  addOrRemoveThisDay(day) {
    console.log(this.shouldAvoidUpdatingDays)
    if (this.shouldAvoidUpdatingDays) {
      return;
    }
    if (!this.isThisDaySelected(day)) {
      this.addThisDay(day);
    } else {
      this.removeThisDay(day);
    }
  }

  addThisDay(day) {
    this.selectedDays.push(day);
    if (this.selectedDays.length > 2) {
      this.selectedDays.shift();
    }
  }

  removeThisDay(day) {
    for (let i = 0 ; i < this.selectedDays.length ; ++i) {
      if (this.selectedDays[i] === day) {
        this.selectedDays.splice(i, 1);

      }
    }
  }

}
