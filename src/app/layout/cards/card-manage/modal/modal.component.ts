import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {ModalCopyService} from '../../../../_services/modal/modalCopyService';
import {ActivatedRoute} from '@angular/router';
import {CardService} from '../../../../_repositories';
import * as $ from 'jquery';

@Component({
    selector: 'app-modal-dupplication',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    closeResult: string;
    changedFields: Array<string> = [];
    difference_p_d;

    constructor(private modalService: NgbModal, private modalCopyService: ModalCopyService,
                private activatedRoute: ActivatedRoute, private cardService: CardService) {

    }

    ngOnInit() {
    }

    open(content) {
        let loadRightFoot = JSON.parse(localStorage.getItem('rightFoot'));
        this.compareFoot(loadRightFoot);
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    compareFoot(list_default_valeur_p_d) {
        let difference_p_d = new Array();
        let list_element = $( "[id$='_p_d']");
        this.changedFields = [];
        $(list_element).each(function (i, el) {
            let actuValElement = $(el).val();
            let key = $(el).attr('attr.data-key');
            let elementCompare = list_default_valeur_p_d[key];

            // FIX VALEUR VIDE DECHARGE PD
            let dech = $(el).parent('div').parent('div').find('label').html();
            if (dech == 'Décharge PD' && actuValElement == '') {
                actuValElement = 'null';
            }
            // ENDFIX

            if (elementCompare != null) {
                elementCompare = elementCompare.toString();
            } else {
                elementCompare = 'null';
            }

            if (actuValElement !== elementCompare) {
                difference_p_d.push($(el));
            }
        });

        let changed = new Array();
        $(difference_p_d).each(function (i, el) {
            let name_field = $(el).parent('div').parent('div').find('label').html();
            changed.push(name_field);
        });
console.log(changed);
        this.changedFields = changed;
        this.difference_p_d = difference_p_d;
    }
    copyLeftToRightCrushing() {
        this.modalCopyService.filter('copyCrushing', {});
    }

    copyLeftToRight() {
        this.modalCopyService.filter('copy', {});
    }
}
