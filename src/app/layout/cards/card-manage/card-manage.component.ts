import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroupDirective} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
    CardService, SportService, PathologyService, UserService, MaterialService,
    StatusService, UsersPatientsService, ShoesModelsService, CardsTemplatesService, RecoveryService, PatientService
} from '../../../_repositories';
import {Card} from '../../../_models';
import * as $ from 'jquery';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ModalCopyService} from '../../../_services/modal/modalCopyService';
import {AuthenticationService} from '../../../_services';
import { PriceService } from '../../../_services/price.service';

export class NgxQrCode {
    text: string;
}

@Component({
    selector: 'app-card-manage',
    templateUrl: './card-manage.component.html',
    styleUrls: ['./card-manage.component.scss'],
    providers: [Card]
})
export class CardManageComponent implements OnInit {
    @ViewChild('cardForm', { static: false }) cardForm: FormGroupDirective;
    cardNoConfirmedOnEdition = true; // permet de s'avoir si une fiche n'est pas encore validé lorsque on la récupére pour lédition.

    // >> mode <<
    addAction;
    editAction;
    templateMode = false;
    duplicateAction;
    deleteAction;
    PageForNormalUser;
    // #########

    baseURl;
    cardServiceClass = CardService;
    cardService;
    sportService;
    userService;
    pathologyService;
    cardModel;
    router;
    activatedRoute;
    footSpecs;
    hashCard;
    card;
    shoeSize;
    userFinishing;
    nameCard;
    template_id;
    hcAlert = false;

    // >>> LIST <<<
    sports: Array<any> = [];
    pathologies: Array<any> = [];
    podologues: Array<any> = [];
    userPatients: Array<any> = [];
    waitingPatients: Array<any> = [];
    userFields: any = {};
    recoveries;
    shoesType;
    bardePronatriceArray;
    materials;
    therapeuticFieldPrices = [];
    // #############

    // LIST NUMERMOTE
    hemicoupoleArray;
    detorsionArray;
    epaisseurSemelleArray;
    hauteurTalonC1Array;
    postingArrArray;
    shoesModelService;
    postingAvArray;
    dechargeArray;
    toElevenArray;
    toSixArray;
    toFourArray;
    toFiveArray;
    toSixteenArray;
    twotoFourArray;
    toTwentyoneArray;
    toThirtyoneArray;
    // ##############


    patientSport;
    pain2;
    bmi;
    hashTemp;
    shoesModel;
    painMessage;
    patientFromList;
    qrCodeInfo = null;
    shippings;
    templates;
    finishings;
    flangeColors;
    waiting;
    userRole;
    tempCard;
    tempFootSpecs;
    status;
    currentUser = null;

    sacRayonSelected = [[], []];

    selectedDays = [];

    //Templates
    templateUsersLists = {
        linkedUsers: [],
        availableUsers: []
    };
    userToBeUnlinked = null;
    userToBeLinked = null;
    isLinking = false;
    isUnlinking = false;
    isPublicTemplate = true;
    isShowingSendProdModal = false;
    shoeSizeScales = ['EU', 'UK', 'US'];

    shouldDisplaySavedAlert = false;
    automaticSaveTimer = null;
    shouldAvoidUpdatingDays = false;
    isPodsSectionDisplayed = true;
    public alerts: Array<any> = [];

    public qrdata: string = null;

    constructor(
        private translate: TranslateService,
        card: CardService,
        cardModel: Card,
        routeur: Router,
        activatedRoute: ActivatedRoute,
        sportService: SportService,
        pathologyService: PathologyService,
        userService: UserService,
        private materialService: MaterialService,
        private statusService: StatusService,
        private usersPatientsService: UsersPatientsService,
        shoesModelService: ShoesModelsService,
        private _modalCopyService: ModalCopyService,
        private recoveryService: RecoveryService,
        private templateService: CardsTemplatesService,
        private patientService: PatientService,
        private authenticationService: AuthenticationService,
        private priceService: PriceService
    ) {
        this.currentUser = this.authenticationService.getCurrentUser();
        this.translate = translate;
        this.cardService = card;
        this.waiting = false;
        this.cardModel = cardModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
        this.sportService = sportService;
        this.materialService = materialService;
        this.pathologyService = pathologyService;
        this.userService = userService;
        this.shoesModelService = shoesModelService;
        this.card = this.setEmptyCard();
        this.footSpecs = this.setEmptyFootSpecs();

        this._modalCopyService.listen().subscribe((m: any) => {
            switch (m.key) {
                case 'actuPatient':
                    this.actuPatient(m.value);
                    break;
                case 'copy':
                    this.copyLeftToRight(m.value);
                    break;
                case 'copyCrushing':
                    this.copyLeftToRightCrushing(m.value);
                    break;
            }
        });
    }

    ngOnInit () {
        this.initArrays();
        this.initSacRayons(0);
        this.initSacRayons(1);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // >>>>>>>>> URL <<<<<<<<
        this.activatedRoute.params.subscribe((params: Params) => {
            const hash = params['hash'];
            this.hashCard = hash;
        });

        this.activatedRoute.queryParams.subscribe((params: Params) => {
            const hashPatientFromList = params['patient'];
            this.patientFromList = hashPatientFromList;
        });

        this.defineActionsBasedOnUrl();
        this.handlePodsSectionVisibility();
        this.userService.getUser(this.currentUser.hash).subscribe((response) => {
            //on charge les préférences utilisateurs uniquement en mode ajout
            if (this.addAction) {
                this.card.finishing_id = response[0].finishing.id;
                this.card.shipping_id = response[0].shipping.id;
                if (response[0].shippingDays) {
                    this.selectedDays = response[0].shippingDays.split(',');
                }
            }
        });

        if (this.addAction) {
            this.usersPatientsService.getAllUserPatient().subscribe((response) => {
                this.userPatients = response;
            });

            this.usersPatientsService.getAllUserWaitingPatient().subscribe((response) => {
                this.waitingPatients = response;
            });
        }
        // ###### PATIENT #######

        // >>>>> QRCODE <<<<<<<<<
        if (typeof (this.hashCard) === 'undefined') {
            this.cardService.createCard(this.card, this.footSpecs).subscribe((response) => {
                this.hashTemp = response.hash;
                this.qrCodeInfo = {
                    token: this.currentUser.token,
                    hash_card: this.hashTemp,
                    num_card: response.idCard,
                    demo: false
                };

                this.qrdata = JSON.stringify(this.qrCodeInfo);
            });
        } else {
            this.cardService.getCard(this.hashCard).subscribe((response) => {
                this.qrCodeInfo = {
                    token: this.currentUser.token,
                    hash_card: this.hashCard,
                    num_card: response.card_id,
                    demo: false
                };

                this.qrdata = JSON.stringify(this.qrCodeInfo);
            });
        }
        // ######### QRCODE #########

        // >>>>>>> CARD <<<<<<<<<<<
        if (typeof(this.hashCard) !== 'undefined') {
            if (!this.templateMode) {
                this.cardService.getPatientSport(this.hashCard).subscribe((response) => {
                    this.patientSport = response[0];
                });

                this.cardService.getCard(this.hashCard).subscribe((response) => {

                    // >>> USER FIELD <<<
                    this.userService.getFields(response.user_hash).subscribe((response) => {
                        this.userFields = response.data;
                        this.userService.setMandatoryFieldsForCardManage(this.userFields);
                    });
                    // ### USER FIELD ###

                    this.nameCard = response.name;
                    this.card = response;
                    console.log(this.card)
                    // définition des jours choisis pour la fiche
                    if (this.card.card_data_shippingDays) {
                        this.selectedDays = this.card.card_data_shippingDays.split(',');
                    }

                    this.pain2 = this.card.card_data_painlevel2;

                    // == duplicate
                    if (this.duplicateAction) {
                        this.card.status_id = 1;
                    }
                    // #########

                    this.cardNoConfirmedOnEdition = (this.card.status_id === CardService.STATUS_NO_CONFIRMED);

                    this.bmi = this.calculateBMI(this.card.card_data_height, this.card.card_data_weight);
                    this.cardService.getFootSpecs(this.card.card_data_id).subscribe((footResponse) => {
                        this.footSpecs = footResponse;
                        this.stringToSacRayons(0, this.footSpecs[0].sacC2);
                        this.stringToSacRayons(1, this.footSpecs[1].sacC2);
                        localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
                    });

                    // >>>>>>> VISUELLE <<<<<<<
                    this.checkClosedStatus(this.card.status_id);
                    setTimeout(() => {
                        this.checkClosedStatus(this.card.status_id);
                        this.shoestypeChange(false);
                      //  this.shoesChange();
                        this.recoveryChange();
                        this.materialChange();
                        this.flangeChange();
                    }, 2000);

                    setTimeout(() => {
                        this.checkClosedStatus(this.card.status_id);
                    }, 4000);
                    // ####### VISUELLE #######

                    // ON CACHE DES MODELS DE SEMELLE EN FONCTION DU TYPE DE PRODUIT
                    if (this.duplicateAction) {
                        if (this.card.shoeType_id == CardService.TYPE_PRODUCT_SEMMELLE) {
                            this.card.shoeModel_id = CardService.MODEL_SEMMELLE_Second_pair;
                        }
                    }
                    // ##########################################

                });
            } else {

                // >>> USER FIELD <<<
                this.userService.getFields(this.currentUser.hash).subscribe((response) => {
                    this.userFields = response.data;
                    this.userService.setMandatoryFieldsForCardManage(this.userFields);

                });
                // ### USER FIELD ###

                this.fetchTemplateUsersLists();
                // >>>>>>>> TEMPLATE <<<<<<<<<<
                this.templateService.getTemplate(this.hashCard).subscribe( (response) => {
                    this.card = response;
                    this.card.podo_hash = response.user_hash;
                    this.isPublicTemplate = this.card.template_public;
                    this.cardService.getFootSpecs(this.card.card_data_id).subscribe((footResponse) => {
                        this.footSpecs = footResponse;
                        this.stringToSacRayons(0, this.footSpecs[0].sacC2);
                        this.stringToSacRayons(1, this.footSpecs[1].sacC2);
                        localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
                    });

                    setTimeout(() => {
                        this.shoestypeChange();
                       // this.shoesChange();
                    }, 2000);
                });
                // ######## TEMPLATE #########
            }
        } else {
            // >>> USER FIELD <<<
            this.userService.getFields(this.currentUser.hash).subscribe((response) => {
                this.userFields = response.data;
                this.userService.setMandatoryFieldsForCardManage(this.userFields);
            });
            // ### USER FIELD ###

            this.card = this.setEmptyCard();
            if (this.patientFromList) {
                this.card.card_patient_hash = this.patientFromList;
            }
            this.card.card_data_shoesSize = 40;
            this.card.card_data_shoesSizeScale = 'EU';
            this.card.podo_hash = null;
            this.footSpecs = this.setEmptyFootSpecs();
            localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
        }

        if (!this.PageForNormalUser) {
            this.userService.getAll().subscribe( (response) => {
                this.podologues = response.datas;
            });
        }
        // ######### CARD ###########
        this.cardService.getAllResources().subscribe(response => {
            this.sports = response.sports;
            this.templates = response.templates;
            this.materials = response.materials;
            this.shippings = response.shippings;
            this.recoveries = response.recoveries;
            this.finishings = response.finishings;
            this.status = response.status;
            this.flangeColors = response.flangeColors;
            this.pathologies = response.pathologies;
            this.shoesType = response.shoeTypes;
            this.shoesModel = response.shoeModels;
            this.therapeuticFieldPrices = response.therapeuticFieldPrices;
            this.handleShoeModels();
        });

        this.userRole = this.checkrole(this.currentUser);
        this.initAutomaticSave();
    }

    defineActionsBasedOnUrl () {
        if (this.router.url.indexOf('users') !== -1) {
            this.PageForNormalUser = true;
        }

        if (this.router.url.indexOf('templates') !== -1) {
            this.templateMode = true;
        }

        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }

        if (this.router.url.indexOf('add') !== -1) {
            this.addAction = true;
        }

        if (this.router.url.indexOf('edit') !== -1) {
            this.editAction = true;
        }

        if (this.router.url.indexOf('duplicate') !== -1) {
            this.duplicateAction = true;
        }
    }

    handlePodsSectionVisibility () {
        if (this.hashCard !== undefined && !this.editAction && !this.duplicateAction) {
            this.isPodsSectionDisplayed = false;
        }
    }

    handleShoeModels () {
        setTimeout(() => {
            if (this.card.shoeModel_id == CardService.MODEL_SEMMELLE_Second_pair) {
                this.shoesModel =  this.shoesModel.filter(
                    function(data) {
                        return data.id == CardService.MODEL_SEMMELLE_Second_pair;
                    }
                );
            } else {
                // ON CACHE DES MODELS DE SEMELLE EN FONCTION DU TYPE DE PRODUIT
                if (this.duplicateAction) {
                    this.shoesModel = this.shoesModel.filter(function (data) {
                        return !(
                            data.id == CardService.MODEL_SEMMELLE_New_pair_Choice1
                            || data.id == CardService.MODEL_SEMMELLE_New_pair_Choice2
                            || data.id == CardService.MODEL_SEMMELLE_New_pair_Choice3
                        );
                    });

                } else {
                    // supprime Seconde paire de la liste
                    this.shoesModel = this.shoesModel.filter(function (data) {
                        return data.id != CardService.MODEL_SEMMELLE_Second_pair;
                    });
                }
            }
        }, 2000);
    }

    initArrays() {
        this.hemicoupoleArray = this.createArray(10, 41, 1, 'pos');
        this.detorsionArray = this.createArray(-10, 31, 1, 'pos');
        this.epaisseurSemelleArray = this.createArray(1, 10.5, 0.5, 'pos');
        this.hauteurTalonC1Array = this.createArray(-5, 26, 1, 'pos');
        this.toElevenArray = this.createArray(1, 11, 1, 'pos');
        this.postingArrArray = this.createArray(2, 11, 1, 'pos');
        this.postingAvArray = this.createArray(0, 7, 1, 'pos');
        this.toSixArray = this.createArray(1, 6, 1, 'pos');
        this.toFourArray = this.createArray(1, 4, 1, 'pos');
        this.toFiveArray = this.createArray(1, 5, 1, 'pos');
        this.toSixteenArray = this.createArray(1, 16, 1, 'pos');
        this.twotoFourArray = this.createArray(2, 5, 1, 'pos');
        this.toTwentyoneArray = this.createArray(1, 21, 1, 'pos');
        this.toThirtyoneArray = this.createArray(1,31,1, 'pos');
        this.bardePronatriceArray = this.createArray(-1, -11, 1, 'neg');
        this.dechargeArray = this.createArray(-1, -7, 1, 'neg');
    }

    handleNavigation(shouldNavigate = true) {
        if (!shouldNavigate) {
            this.displaySavedAlert();
            return;
        }
        if (this.router.url.indexOf('templates') !== -1) {
            if (this.router.url.indexOf('users') !== -1) {
                this.router.navigate(['/users/cards/templates']);
            } else {
                this.router.navigate(['/cards/templates']);
            }
        } else {
            if (this.router.url.indexOf('users') !== -1) {
                this.router.navigate(['/users/cards']);
            } else {
                this.router.navigate(['/cards']);
            }
        }
    }

    displaySavedAlert() {
        this.shouldDisplaySavedAlert = true;
        setTimeout(() => {
            this.shouldDisplaySavedAlert = false;
        }, 5000);
    }

    sendToProduction() {
        this.card.closedStatus = true;
        this.gestionCard();
    }

    initAutomaticSave() {
        this.automaticSaveTimer = setInterval(() => {
            if (this.isShowingSendProdModal) {
                return
            }
            if (!this.editAction && !this.addAction) {
                return;
            }
            if(!this.card.card_patient_hash && !this.card.patient_lastname) {
                return;
            }
            this.save();
        }, 30 * 1000);
    }

    save() {
        if (this.isShowingSendProdModal) {
            return;
        }
        this.card.closedStatus = false;
        this.gestionCard(false);
    }

    gestionCard(shouldNavigate = true) {
        this.footSpecs[0].sacC2 = this.sacRayonsToString(0);
        this.footSpecs[1].sacC2 = this.sacRayonsToString(1);

        if (!this.templateMode) {
            this.card.shipping_days = this.selectedDays.join(',');
            if (typeof(this.hashCard) === 'undefined' || this.router.url.indexOf('duplicate') !== -1) {
                if (this.router.url.indexOf('duplicate') !== -1) {
                    this.card.card_data_parent_hash = this.hashCard;
                }
                if (this.card.closedStatus == true) {
                    this.card.status_id = 2;
                } else {
                    this.card.status_id = 1;
                }

                this.card.tempHash = this.hashTemp;
                this.cardService.createCard(this.card, this.footSpecs).subscribe((response) => {
                    localStorage.setItem('status', 'success');
                    localStorage.setItem('message', this.translate.instant('Item created with success'));
                    this.assignAnswer(response);
                    this.handleNavigation(shouldNavigate);
                }, () => {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                    this.handleNavigation(shouldNavigate);
                });
            } else {
                this.cardService.editCard(this.card, this.footSpecs, this.hashCard).subscribe((response) => {
                    localStorage.setItem('status', 'success');
                    localStorage.setItem('message', this.translate.instant('Item edited with success'));
                    this.handleNavigation(shouldNavigate);
                }, () => {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                    this.handleNavigation(shouldNavigate);
                });
            }
        } else {
            if (this.card.shoeType_id == 'null') {
                this.card.shoeType_id = null;
                this.card.shoeType_name = null;
                this.card.shoeModel_name = null;
                this.card.shoeModel_position = null;
                this.card.shoeModel_id = null;
            }

            this.card.public = this.isPublicTemplate;

            if (typeof(this.hashCard) === 'undefined') {
                let isUserMode = true;
                console.log('add tempalte');
                if (this.router.url.indexOf('users') !== -1) {
                    this.card.aTmpt = null;
                    this.card.public = false;
                } else {
                    this.card.aTmpt = true;
                    isUserMode = false;
                }
                this.templateService.createTemplate(this.card, this.footSpecs).subscribe((response) => {
                    console.log(response);
                    localStorage.setItem('status', 'success');
                    localStorage.setItem('message', this.translate.instant('Item created with success'));
                    if(isUserMode) {
                        let idUser = this.currentUser.id;
                        let templateHash = response.hash;
                        this.templateService.linkUserToTemplate(idUser, templateHash).subscribe((response) => {
                            this.handleNavigation(shouldNavigate);
                        }, (error) => {
                            this.handleNavigation(shouldNavigate);
                        })
                    } else {
                        this.handleNavigation(shouldNavigate);
                    }
                }, () => {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                    this.handleNavigation(shouldNavigate);
                });
            } else {
                console.log('edit template');
                this.templateService.editTemplate(this.card, this.footSpecs, this.hashCard).subscribe((response) => {
                    localStorage.setItem('status', 'success');
                    localStorage.setItem('message', this.translate.instant('Item edited with success'));
                    this.handleNavigation(shouldNavigate);
                }, () => {
                    localStorage.setItem('status', 'danger');
                    localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                    this.handleNavigation(shouldNavigate);
                });
            }
        }
    }

    assignAnswer (response) {
        this.hashCard = response.data[0];
        this.hashTemp = null;
        this.addAction = false;
        this.editAction = true;
        this.footSpecs[0].id = response.footSpecIds[0];
        this.footSpecs[1].id = response.footSpecIds[1];
        this.formatPatient();
    }

    formatPatient () {
        for (let i = 0; i < this.userPatients.length; ++i) {
            if (this.card.card_patient_hash == this.userPatients[i].hash) {
                this.card.patient_lastname = this.userPatients[i].lastname;
                this.card.patient_firstname = this.userPatients[i].firstname;
                this.card.patient_email = this.userPatients[i].email;
                this.card.patient_birthdate = this.userPatients[i].birthdate;
                this.card.patient_gender = this.userPatients[i].gender;
                this.card.user_lastname = this.currentUser.lastname;
                this.card.user_firstname = this.currentUser.firstname;
            }
        }
    }

    delete() {
        let card = {
            hash: this.hashCard
        };

        if (!this.templateMode) {
            this.cardService.deleteCard(card).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item deleted with success'));
                this.handleNavigation();
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
                this.handleNavigation();
            });
        } else {
            this.templateService.delete(this.hashCard).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item deleted with success'));
                this.handleNavigation();
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
                this.handleNavigation();
            });
        }

    }

    calculateBMI(height, weight) {
        let bmi = 0;

        if (height > 0 && weight > 0) {
            bmi = (weight / Math.pow((height / 100), 2));
        }

        this.bmi = bmi.toFixed(2);

        return this.bmi;
    }

    /**
     *
     * @param start
     * @param end
     * @param inc
     * @param op
     */
    createArray(start, end, inc, op) {
        let tab = [];

        if (op === 'pos') {
            while (start < end) {
                tab.push(start);
                start += inc;
            }
        } else {
            while (start > end) {
                tab.push(start);
                start -= inc;
            }
        }

        return tab;
    }

    checkrole(user) {
        let userRole = false;
        if ($.inArray("ROLE_ADMIN", user.roles) !== -1) {
            userRole = true;
        }
        return userRole;
    }

    checkClosedStatus(status) {
        if ((status == '2' && !(this.userRole && this.router.url.indexOf('users') == -1)) || (this.router.url.indexOf('duplicate') !== -1)) {
            this.card.closedStatus = 1;
            this.shouldAvoidUpdatingDays = true;
            $('#editFormCard').addClass('closedStatus'); // VOIR SCSS
            if (this.router.url.indexOf('duplicate') !== -1) {
                $('#editFormCard').addClass('beingDuplicated');
                this.shouldAvoidUpdatingDays = false;
            }
            $('#editFormCard').find('input, select, textarea').filter(':not(#materials, #finishing, #shipping, #freeText, #recovery, #pratique, #sport, #pathology, #shoesize, #weight, #height)').prop('readOnly', true);
            $('app-modal-dupplication').hide();
        } else {
            this.card.closedStatus = 0;
        }
    }

    setEmptyFootSpecs() {
        return [
            {
                "arcC1": null,
                "arcC2": "centree",
                "arrPiedC1": null,
                "arrPiedC2": null,
                "avPiedC1": null,
                "avPiedC2": null,
                "axeDetorsion": "median",
                "bardePronatrice": null,
                "bandePronatriceType": "totale",
                "brc": null,
                "brc23Ext": null,
                "extMorton": null,
                "extMortonInterDigital": null,
                "brc23Int": null,
                "cluffy": null,
                "coinPronateur": null,
                "coinPronateurMention": "long",
                "coinSousCuboidien": null,
                "coinSupinateur": null,
                "coinSupinateurMention": "long",
                "dechargeC1": null,
                "dechargeC2": null,
                "dechargeC3": "without_filling",
                "detorsion": "plat",
                "epaisseurSemelles": 3,
                "foot": 0,
                "hauteurCuvettes": "medium",
                "hauteurHemicoupole": "Combler",
                "hauteurTalonC1": 0,
                "hauteurTalonC2": "trois_quart",
                "hauteurTalonC3": "cad",
                "id": null,
                "postingArrP": null,
                "postingAvP": 0,
                "posturoBa": null,
                "posturoBi": null,
                "posturoBpi": null,
                "posturoEai": null,
                "posturoEca": null,
                "posturoEmi": null,
                "posturoEpi": null,
                "rayon": null,
                "ruler": false,
                "sacC1": null,
                "sacC2": 1,
                "sommetCourbure": "ant",
                "sousDia": null
            }, {
                "arcC1": null,
                "arcC2": "centree",
                "arrPiedC1": null,
                "arrPiedC2": null,
                "avPiedC1": null,
                "avPiedC2": null,
                "axeDetorsion": "median",
                "bardePronatrice": null,
                "bandePronatriceType": "totale",
                "brc": null,
                "brc23Ext": null,
                "extMorton": null,
                "extMortonInterDigital": null,
                "brc23Int": null,
                "cluffy": null,
                "coinPronateur": null,
                "coinPronateurMention": "long",
                "coinSousCuboidien": null,
                "coinSupinateur": null,
                "coinSupinateurMention": "long",
                "dechargeC1": null,
                "dechargeC2": null,
                "dechargeC3": "without_filling",
                "detorsion": "plat",
                "epaisseurSemelles": 3,
                "foot": 1,
                "hauteurCuvettes": "medium",
                "hauteurHemicoupole": "Combler",
                "hauteurTalonC1": 0,
                "hauteurTalonC2": "trois_quart",
                "hauteurTalonC3": "cad",
                "id": null,
                "postingArrP": null,
                "postingAvP": 0,
                "posturoBa": null,
                "posturoBi": null,
                "posturoBpi": null,
                "posturoEai": null,
                "posturoEca": null,
                "posturoEmi": null,
                "posturoEpi": null,
                "rayon": null,
                "ruler": false,
                "sacC1": null,
                "sacC2": 1,
                "sommetCourbure": "ant",
                "sousDia": null
            }];
    }

    setEmptyCard() {
        return {
            "card_data_freeText": "",
            "card_data_height": null,
            "card_data_weight": null,
            "card_data_id": null,
            "card_data_largeur": "normal",
            "card_data_oldProtection": "0",
            "card_data_oldType": "0",
            "card_data_parent_id": null,
            "card_data_shippingAddress": "",
            "card_data_shoesSize": 25,
            "card_id": null,
            "closedStatus": null,
            "flange_color_id": null,
            "material_id": null,
            "patho_id": null,
            "recovery_id": null,
            "shipping_id": null,
            "shoeModel_id": 1,
            "shoeModel_name": null,
            "shoeModel_position": null,
            "shoeType_id": 1,
            "shoeType_name": null,
            "sport": {"id": null, "frequency": null, "sport_id": null, "sport_name": null},
            "frequency": null,
            "id": null,
            "sport_id": null,
            "sport_name": null,
            "status_id": null,
            "total_files": null,
            "user_id": null,
            "card_data_painlevel1": '0',
            "card_data_painlevel2": '0',
            "card_data_painlevel3": '0'
        };

    }

    shouldDisplayTherapeuticChoices() {
        return this.card.shoeModel_id != this.cardServiceClass.MODEL_SEMMELLE_New_pair_Choice2
    }

    shouldDisplayEpaisseur() {
        return this.userFields.epaisseur && !this.templateMode || this.templateMode
    }

    copyLeftToRightCrushing(m) {
        let footSpecsTemp = this.footSpecs;

        $.each(footSpecsTemp[0], function(index, value) {
            if (index !== 'id') {
                if (index !== 'foot') {
                    footSpecsTemp[1][index] = value;
                }
            }});

        this.footSpecs = footSpecsTemp;
        localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
    }

    copyLeftToRight(m) {
        let footSpecsTemp = this.footSpecs;
        let loadRightFoot = JSON.parse(localStorage.getItem('rightFoot'));

        $.each(footSpecsTemp[0], function(index, value) {
            if (index !== 'id') {
                if (index !== 'foot') {
                    if (loadRightFoot[index] === footSpecsTemp[1][index]) {
                        footSpecsTemp[1][index] = value;
                    }
                }
            }});

        this.footSpecs = footSpecsTemp;

        localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
    }

    setTemplate(event) {
        let selectedTemplate = this.templates.find( x => x.id == this.template_id);
        console.log(selectedTemplate);
        this.templateService.getTemplate(selectedTemplate.hash).subscribe( (response) => {
            this.tempCard = response;
            this.cardService.getFootSpecs(this.tempCard.card_data_id).subscribe((footResponse) => {
                this.tempFootSpecs = footResponse;
                this.compareValue(this.tempCard, 'card');
                this.compareValue(this.tempFootSpecs, 'footspec');
                this.stringToSacRayons(0, this.footSpecs[0].sacC2);
                this.stringToSacRayons(1, this.footSpecs[1].sacC2);
                localStorage.setItem('rightFoot', JSON.stringify(this.footSpecs[1]));
            });

            this.shoestypeChange();
        });
    }

    compareValue(temp, type) {
        let cardField;
        let foot;

        if (type === 'footspec') {
            Object.keys(temp).forEach(key => {
                foot = temp[key];
                Object.keys(foot).forEach(i => {

                    if (foot[i] != null) {
                        this.footSpecs[key][i] = foot[i];
                    }
                });
            });
        } else {
            cardField = ['shoeModel_id', 'shoeModel_name', 'shoeModel_position', 'shoeType_id', 'shoeType_name'];

            Object.keys(temp).forEach(key => {
                if (temp[key] != null && cardField.includes(key)) {
                    this.card[key] = temp[key];
                }
            });
        }
    }

    updatePainLevel(event) {
        this.cardService.editCard(this.card, this.footSpecs, this.hashCard).subscribe((response) => {
            this.painMessage = { text: 'Item edited with success', color: 'green'};
        }, () => {
            this.painMessage = { text: 'ERROR: Item not edited because an error', color: 'red'};
        });
        setTimeout(() => {
            this.painMessage = {};
        }, 6000);
    }

    recoveryChange() {
        let recoveryValue = this.card.recovery_id;

        if (!(recoveryValue == 'null' || recoveryValue == null)) { // suppression des 2 premiers élements ('Recouvrement non collé','Recouvrement collé)
            const newElement = new Array<any>();
            this.finishings = this.finishings.forEach((element, index) => {
                if (element.id > 2) {
                    newElement.push(element);
                }
            });
            this.finishings = newElement;

            if (this.card.finishing_id != 3) {
                this.card.finishing_id = 4;
            }

        } else { // 'Biseaux/débords','Détouré','Recouvrement non collé','Recouvrement collé
            this.cardService.getAllFinishing().subscribe( (response) => {
                this.finishings = response;
            });
        }
    }

    flangeChange() {
        if (this.card.shoeType_id == 3) {
            if (this.card.flange_color_id == CardService.FLANGUE_NOIR) { // NOIR
                this.card.material_id = CardService.MATERIEL_NOIR;
            } else if (this.card.flange_color_id == CardService.FLANGUE_BRUIN) { // BRUIN
                this.card.material_id = CardService.MATERIEL_BRUIN;
            }
        }
    }

    materialChange() {
        // 10,18 sport/ sport plus
        // 9 CONFORT
        // 8, 19 DIABETO/ Dabeto plus
        // 20  40° Plus
        const values = new Array('10', '18', '9', '8', '19', '20');

        if (values.indexOf(this.card.material_id) !== -1) { // OUI
            this.hemicoupoleArray = this.createArray(10, 41, 1, 'pos');
            this.hcAlert = false;
        } else {
            this.hemicoupoleArray = this.createArray(10, 31, 1, 'pos');

            let hemipg = isNaN(this.footSpecs[0].hauteurHemicoupole) ? 0 : parseInt(this.footSpecs[0].hauteurHemicoupole);
            let hemipd = isNaN(this.footSpecs[1].hauteurHemicoupole) ? 0 : parseInt(this.footSpecs[1].hauteurHemicoupole);
            let sizePG = hemipg + parseInt(this.footSpecs[0].hauteurTalonC1);
            let sizePD = hemipd + parseInt(this.footSpecs[1].hauteurTalonC1);

            if (this.footSpecs[0].hauteurHemicoupole > 30 || this.footSpecs[1].hauteurHemicoupole > 30 || sizePG > 30 || sizePD > 30) {
                this.hcAlert = true;
            }


            this.footSpecs[0].hauteurHemicoupole = (this.footSpecs[0].hauteurHemicoupole < 31) ? this.footSpecs[0].hauteurHemicoupole : 'Combler';
            this.footSpecs[1].hauteurHemicoupole = (this.footSpecs[1].hauteurHemicoupole < 31) ? this.footSpecs[1].hauteurHemicoupole : 'Combler';
        }

        if (this.card.shoeType_id == CardService.TYPE_PRODUCT_SANDALES) {
            if (this.card.material_id == CardService.MATERIEL_NOIR) { // NOIR
                this.card.flange_color_id = CardService.FLANGUE_NOIR;
            } else if (this.card.material_id == CardService.MATERIEL_BRUIN) { // BRUIN
                this.card.flange_color_id = CardService.FLANGUE_BRUIN;
            }
        }
    }

    shoestypeChange(resetMat = true) {
        //on reset les matériaux quand on change de types de semelles
        if (resetMat) {
            this.card.material_id = null;
        }
            const val = this.card.shoeType_id;


        for (let i = 0; i < 4; i += 0.5) {
            $('#epaisseur_semelles_p_g option[value="' + i + '"]').removeAttr('disabled').show();
            $('#epaisseur_semelles_p_d option[value="' + i + '"]').removeAttr('disabled').show();
        }

        $('#materials option').each(function (index, el) {
            $(el).removeAttr('disabled').show();
        });

        switch (val) {
            case '1': // SEMELLE
                this.card.shoeModel_id = CardService.MODEL_SEMMELLE_New_pair_Choice1;
                this.updateShoeThickness(3);
                break;
            case '2': // POSTURO
                this.card.shoeModel_id = CardService.MODEL_POSTURO_NEW_PAIR;
                this.updateShoeThickness(3);
                break;
            case '3': // SANDALES
                this.card.shoeModel_id = CardService.MODEL_SANDALE_CANNES;
                this.updateShoeThickness(3);
                break;
            case '4': // TONG
                this.card.shoeModel_id = CardService.MODEL_TONG_MODELE_X;
                this.updateShoeThickness(6);
                break;
            case '5':
                this.card.shoeModel_id = 12;
                this.updateShoeThickness(3);
                break;
            case '6':
                this.card.shoeModel_id = 14;
                this.updateShoeThickness(6);
                break;
        }

        if (val == CardService.TYPE_PRODUCT_POSTURO) { // hide
        } else { // show
            $('#materials option').each(function (index, el) {
                if ((el['value'] == 21 || el['value'] == 22)) { // si semelle posturo
                    $(el).attr('disabled', 'disabled').addClass('d-none');
                } else {
                    $(el).removeAttr('disabled').removeClass('d-none');
                }
            });

            if (val == CardService.TYPE_PRODUCT_SANDALES || val == CardService.TYPE_PRODUCT_TONG) {

                for (let i = 0; i < 4; i += 0.5) {
                    $('#epaisseur_semelles_p_g option[value="'+i+'"]').attr('disabled', 'disabled').hide();
                    $('#epaisseur_semelles_p_d option[value="'+i+'"]').attr('disabled', 'disabled').hide();
                }

            }

            if (val == CardService.TYPE_PRODUCT_SANDALES) {
                $('#flangeColor option').each(function (index, el) {
                    if (!(typeof(el['value']) !== 'undefined' && el['value'] == CardService.FLANGUE_NOIR || el['value'] == CardService.FLANGUE_BRUIN)) {
                        $(el).attr('disabled', 'disabled').hide();
                    } else {
                        $(el).removeAttr('disabled').show();
                    }
                });

                const material_id = this.card.material_id;
                if (!(material_id == CardService.MATERIEL_BRUIN || material_id == CardService.MATERIEL_NOIR)) {
                    this.card.material_id = 'null';
                }

                const flange_color_id = this.card.flange_color_id;
                if (!(flange_color_id == CardService.FLANGUE_NOIR || flange_color_id == CardService.FLANGUE_BRUIN)) {
                    this.card.flange_color_id = 'null';
                }

                $('#materials option').each(function (index, el) {
                    if (!(el['value'] == CardService.MATERIEL_BRUIN || el['value'] == CardService.MATERIEL_NOIR)) {
                        $(el).attr('disabled', 'disabled').hide();
                    } else {
                        $(el).removeAttr('disabled').show();
                    }
                });

                this.materialChange();
            } else if (val == CardService.TYPE_PRODUCT_TONG) {

                const flange_color_id = this.card.flange_color_id;
                if ((flange_color_id == CardService.FLANGUE_NOIR || flange_color_id == CardService.FLANGUE_BRUIN)) {
                    this.card.flange_color_id = null;
                }

                $('#flangeColor option').each(function (index, el) {
                    if ((el['value'] == CardService.FLANGUE_NOIR || el['value'] == CardService.FLANGUE_BRUIN)) {
                        $(el).attr('disabled', 'disabled').hide();
                    } else {
                        $(el).removeAttr('disabled').show();
                    }
                });
                this.materialChange();
            }
        }

        setTimeout(() => {
            // >>>>>>vérification des images <<<<<<<<
            let list_image = new Array<string>();

            $(document).find('.fiches_modeles_sandales_image').each((index, element) => {
                list_image.push(this.imageExist($(element).attr('src')));
            });

            $(document).find('.fiches_modeles_sandales_image').each((index, element) => {
                $(element).attr('src', list_image[index]);
            });
            // #####################################
        }, 500);
    }

    updateShoeThickness(value) {
        console.log(value);
        console.log(this.footSpecs[0].epaisseurSemelles);
        this.footSpecs[0].epaisseurSemelles = value;
        this.footSpecs[1].epaisseurSemelles = value;
    }

    imageExist(url) {
        let http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        return (http.status != 404) ? url : 'assets/images/no_image.svg';
    }

    switchWaiting(waiting) {
        if (!waiting) {
            this.waiting = true;
        } else {
            this.waiting = false;
        }
    }

    actuPatient(data = null) {
        this.usersPatientsService.getAllUserPatient().subscribe((response) => {
            this.userPatients = response;

            if (typeof(data['selectId']) !== 'undefined' && data['selectId'] != null) {
                this.card.card_patient_hash = data['selectId'];
            }
        });
    }

    patientchange(event) {
        this.patientService.getPatient(event).subscribe( (response) => {
            this.card.card_data_height = response[0]['height'];
            this.card.card_data_weight = response[0]['weight'];
            this.card.card_data_shoesSize = response[0]['shoes_size'];
            this.calculateBMI(this.card.card_data_height, this.card.card_data_weight);
        });
    }

    keypress(event) {
        // LORSQUE L'ON PRESSE LA TOUCHE ENTER, on passe au champ suivant
        if (event.keyCode == 13 && event.target.type !== 'submit' && event.target.type !== 'textarea') {
            let self = $(':focus');
            let form = $('form:visible').first();

            event.preventDefault();
            let focusable = form.find('input,select,textarea').filter(':visible:not([disabled]):not([readonly])');
            focusable.eq(focusable.index(self) + (event.shiftKey ? -1 : 1)).focus();

            return false;
        }
    }

    filterTemplatesOfType(isPublic) {
        if (typeof(this.templates) !== 'undefined') {
            if (isPublic) {
                return this.templates.filter(x => x.public == 1);
            } else {
                return this.templates.filter(x => x.public == 0);
            }
        }
    }

    resetMessageHcAlert() {
        let hemipg = isNaN(this.footSpecs[0].hauteurHemicoupole) ? 0 : parseInt(this.footSpecs[0].hauteurHemicoupole);
        let hemipd = isNaN(this.footSpecs[1].hauteurHemicoupole) ? 0 : parseInt(this.footSpecs[1].hauteurHemicoupole);
        let sizePG = hemipg + parseInt(this.footSpecs[0].hauteurTalonC1);
        let sizePD = hemipd + parseInt(this.footSpecs[1].hauteurTalonC1);

        if ( sizePG < 31 && sizePD < 31) {
            this.hcAlert = false;
        } else {
            const values = new Array('10', '18', '9', '8', '19', '20');
            if (values.indexOf(this.card.material_id) !== -1) {
                this.hcAlert = false;
            } else {
                this.hcAlert = true;
            }
        }
    }

    shouldDisplayPainLevelForm(): boolean {
        let shouldDisplay = false;
        if (this.addAction) {
            if (this.userFields.painlevel1) {
                shouldDisplay = true;
            }
        } else if (this.editAction) {
            if (this.userFields.painlevel1 || this.userFields.painlevel2 || this.userFields.painlevel3) {
                shouldDisplay = true;
            }
        }
        return shouldDisplay;
    }

    booleanToText(value): string {
        if (value) {
            return 'Yes';
        } else {
            return 'No';
        }
    }

    initSacRayons(foot) {
        for (let i = 0 ; i < this.toSixArray.length ; ++i) {
            this.sacRayonSelected[foot][this.toSixArray[i]] = false;
        }
    }

    sacRayonsToString(foot) {
        let response = [];
        for (let i = 0 ; i < this.toSixArray.length ; ++i) {
            if (this.sacRayonSelected[foot][this.toSixArray[i]]) {
                response.push((this.toSixArray[i]) + 'R');
            }
        }
        return response.join(',');
    }

    stringToSacRayons(foot, sacC2) {
        if (typeof sacC2 == "string") {
            let sacC2Array = sacC2.split(',');
            for (let i = 0 ; i <= this.toSixArray.length ; ++i) {
                this.sacRayonSelected[foot][this.toSixArray[i]] = false;
                if (sacC2Array.includes(this.toSixArray[i] + 'R')) {
                    this.sacRayonSelected[foot][this.toSixArray[i]] = true;
                }
            }
        }
    }

    filteredMaterials() {
        if (!this.materials) {
            return [];
        }
        const filteredMat = [];
        for (let mat of this.materials) {
            if (this.card.shoeModel_id == CardService.MODEL_POSTURO_NEW_PAIR) {
                if (mat.isPosturo) {
                    filteredMat.push(mat);
                }
            } else if(this.card.shoeModel_id == CardService.MODEL_TONG_MODELE_X || this.card.shoeModel_id == CardService.MODEL_TONG_MODELE_Y) {
                if (mat.isTong) {
                    filteredMat.push(mat);
                }
            } else {
                filteredMat.push(mat);
            }

        }

        return filteredMat;
    }

    fetchTemplateUsersLists() {
        this.templateService.fetchLinkableUsersToTemplate(this.hashCard).subscribe((response) => {
            this.templateUsersLists = response.data;
        });
    }

    unlinkUser() {
        if (this.userToBeUnlinked) {
            this.isUnlinking = true;
            const hash = this.getCardHash();
            this.templateService.unlinkUserToTemplate(this.userToBeUnlinked, hash).subscribe((response) => {
                this.templateUsersLists = response.data;
                this.isUnlinking = false;
                this.userToBeUnlinked = null;
            },
            (error) => {
                this.isUnlinking = false;
            });

        }
    }

    linkUser() {
        if (this.userToBeLinked) {
            this.isLinking = true;
            const hash = this.getCardHash();
            this.templateService.linkUserToTemplate(this.userToBeLinked, hash).subscribe((response) => {
                this.templateUsersLists = response.result;
                this.isLinking = false;
                this.userToBeLinked = null;
            },
            (error) => {
                this.isLinking = false;
            });
        }
    }

    getCardHash() {
        if (this.hashCard) {
            return this.hashCard;
        } else if (this.hashTemp) {
            return this.hashTemp;
        }
        return null;
    }

    isDemoUser () {
        if (!this.currentUser) {
            return true
        }
        if (this.currentUser.roles.includes('ROLE_DEMO')) {
            return true
        }
        return false
    }

    calculatePrice () {
        this.priceService.setSelectedResources(
            this.card.shipping_id,
            this.card.finishing_id,
            this.card.shoeModel_id,
            this.card.material_id,
            this.card.recovery_id,
            this.card.shoeType_id,
        );
        this.priceService.setResourceLists(
            this.shippings, this.finishings, this.shoesModel, this.filteredMaterials(), this.recoveries
        );
        this.priceService.setTherapeuticFieldsTables(this.footSpecs, this.therapeuticFieldPrices);
        return this.priceService.calculatePrice();
    }

    shouldDisplaySaveButton () {
        return (
            this.duplicateAction
            || this.templateMode
            || (!this.userRole && this.cardNoConfirmedOnEdition)
            || this.userRole
        );
    }

    shouldDisplaySendToProductionButton () {
        return (
            this.duplicateAction
            || this.templateMode
            || (!this.userRole && this.cardNoConfirmedOnEdition)
            || this.userRole
        ) && !this.isDemoUser()
        && this.card.status_id == 1;
    }

    isFormValid () {
        if (!this.cardForm) {
            return false;
        }
        return this.cardForm.form.valid
    }

    isUsinageModeActivated () {
        return [2, 12, 13].includes(this.card.shoeModel_id);
    }

    getShoeSizes () {
        if (this.card.card_data_shoesSizeScale === 'EU') {
            return this.createArray(23, 56, 1, 'pos');
        }
        if (this.card.card_data_shoesSizeScale === 'UK') {
            return this.createArray(3, 14.5, 0.5, 'pos');
        }
        if (this.card.card_data_shoesSizeScale === 'US') {
            return this.createArray(3, 14.5, 0.5, 'pos');
        }
        return [];
    }

    ngOnDestroy () {
        clearInterval(this.automaticSaveTimer);
    }
}
