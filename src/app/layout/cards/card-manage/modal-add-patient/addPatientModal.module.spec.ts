import { AddPatientModalModule } from './addPatientModal.module';

describe('CardsModule', () => {
    let modalModule: AddPatientModalModule;

    beforeEach(() => {
        modalModule = new AddPatientModalModule();
    });

    it('should create an instance', () => {
        expect(modalModule).toBeTruthy();
    });
});
