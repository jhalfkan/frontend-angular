import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import {PatientService, UserService} from '../../../../_repositories';
import {Patient} from '../../../../_models';
import {TranslateService} from '@ngx-translate/core';
import {ModalCopyService} from '../../../../_services/modal/modalCopyService';

@Component({
    selector: 'app-modal-addpatient',
    templateUrl: './addPatientModal.component.html',
    styleUrls: ['./addPatientModal.component.scss']
})
export class AddPatientModalComponent implements OnInit {
    modalReference = null;
    closeResult: string;
    hashPatient;
    patientModal: Patient;
    bmi;
    alertType = '';
    alert = '';
    patient;

    constructor(private modalService: NgbModal, private patientService: PatientService, private translate: TranslateService, private modalCopyService: ModalCopyService, private userService: UserService) {
        this.patientModal = new Patient;
    }

    open(content) {
        this.modalReference = this.modalService.open(content, { size: 'lg', centered: true });
        this.modalReference.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }


    ngOnInit() {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.patient = this.patientModal;
        this.bmi = this.calculateBMI(this.patient);
        this.userService.getUser(currentUser.hash).subscribe((response) => {
            this.userService.setMandatoryFields(response[0]);
        });
    }

    gestionPatient(patientModal) {
            patientModal.birthdate = $('#datePickerField').val();
         /*   let dateValue = $('#datePickerField').val();
            let res = (dateValue as string).split('-');
            patientModal.birthdate = res[1] + '-' + res[0] + '-' + res[2];
            let patientDate = new Date(patientModal.birthdate);
            let currentDate = new Date();
            if ( patientDate > currentDate ) {
                this.alertType = 'danger';
                this.alert = this.translate.instant('ERROR: Birthdate incorrect');

                setTimeout(() => {
                    this.alert = '';
                }, 6000);
            } else { */
                this.patientService.createPatient(this.patientModal).subscribe((response) => {
                    this.alertType = 'success';
                    this.alert = this.translate.instant('Item created with success');

                    const selectId = response['data']['hash'];
                    setTimeout(() => {
                        this.alert = '';
                        this.navigateBack(selectId);
                    }, 2000);

                }, () => {
                    this.alertType = 'danger';
                    this.alert = this.translate.instant('ERROR: Item not created because an error');

                    setTimeout(() => {
                        this.alert = '';
                    }, 2000);
                });
          //  }
    }

    calculateBMI(patientModal) {
        const taille = patientModal.height;
        const poid = patientModal.weight;
        let bmi = 0;

        if (taille > 0 && poid > 0) {
            bmi = (poid / Math.pow((taille / 100), 2));
        }

        return bmi.toFixed(2);
    }

    navigateBack(selectId = null) {
        this.patientModal = new Patient;
        this.modalReference.close();
        this.modalCopyService.filter('actuPatient', {selectId: selectId});
    }

    formatDate() {
        if (this.patient.birthdate) {
            this.patient.birthdate.day =  ("0" + this.patient.birthdate.day).slice(-2);
            this.patient.birthdate.month =  ("0" + this.patient.birthdate.month).slice(-2);
            this.patient.birthdate = this.patient.birthdate.year + '-' + this.patient.birthdate.month + '-' + this.patient.birthdate.day;
        }
    }


}
