import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardsRoutingModule } from './cards-routing.module';
import { CardsComponent } from './cards.component';
import { PageHeaderModule } from '../../../shared/index';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, CardsRoutingModule, PageHeaderModule],
    declarations: [CardsComponent]
})
export class CardsModule {}
