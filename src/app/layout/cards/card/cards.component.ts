import {Component, OnInit} from '@angular/core';
import {Card} from '../../../_models';
import {CardService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-cards',
    templateUrl: './cards.component.html',
    styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
    cardService;
    cardModel;
    activatedRoute;
    status: string;

    constructor(card: CardService, cardModel: Card, activatedRoute: ActivatedRoute) {
        this.cardService = card;
        this.cardModel = cardModel;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
    }

    deleteCard() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            console.log(id);
        });
        // let id = {name: this.cardModel.name};

/*        this.cardService.deleteCard(id).subscribe((response) => {
            console.log(response.code);
            this.status = 'success';
        });*/
    }
}
