import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CardService, StatusService} from '../../../_repositories';
import {first} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators, FormsModule} from '@angular/forms';
import {ModalUndoService} from '../../../_services/modal/modalUndoService';
import {UpdateStatusModalService} from '../../../shared/services/UpdateStatusModalService';
import {DatatableService} from '../../../shared/services/DatatableService';
import {AuthenticationService} from '../../../_services';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  baseURl;
  actions;
  heading;
  order;
  filterStatus: string|number = "";
  paramQuery;
  status: Array<any> = [];
  public alerts: Array<any> = [];
  userRoles;

  isDownloading = false;

  constructor(private activatedRoute: ActivatedRoute, private authentificat: AuthenticationService, private _dataTableService: DatatableService, private _modalUndo: ModalUndoService, private statusService: StatusService, private router: Router, private cardService: CardService, public _updateStatusModalService: UpdateStatusModalService) {
      this.baseURl = CardService.BASEURL + '/';
      this.userRoles = authentificat.getRoles();

      this.heading = [
        'id', 'lastname', 'firstname', 'shoeModelName'
      ];

      this.actions = new Array();
      this.order = [[ 0, 'DESC' ]];

      this.statusService.getAll().pipe(first())
          .subscribe(
              data => {
                  if (data.result === true) {
                      this.status = data.datas;
                      //this.status = [{id: '', name : 'Tous'}].concat(data.datas);
                  //      this.status.unshift({id: '', name: ''});
                  }

                  if (typeof(this.paramQuery.status) !== 'undefined') {
                      const filterStatus = parseInt(this.paramQuery.status);
                      if (filterStatus !== 0) {
                        this.filterStatus = parseInt(this.paramQuery.status);
                      }

                  }
              },
              error => {

              });

      this.activatedRoute.queryParams.pipe(first()).subscribe(params => {
          this.paramQuery = params;
// check if is Internet Explorer
          let isIE = /msie\s|trident\//i.test(window.navigator.userAgent);

          if (this.heading.indexOf('users') == -1  && !isIE) {
              this.heading = [
                'id', 'patient', 'podo'
              ];
          }

          if (typeof (this.paramQuery.status) !== 'undefined' && (this.paramQuery.status === '3' || this.paramQuery.status === '6') && this.heading.indexOf('verified') === -1 && this.userRoles.indexOf('ROLE_ADMIN') !== -1) {
              this.heading.push('verified');
          }
          this.includeRequiredHeading(this.paramQuery.status);
      });

      this.actions.push({'icon': 'fa fa-eye', 'link': 'cards/card/:hash', 'button': 'btn btn-secondary btn-sm', 'action': 'view'});
      this.actions.push({'icon': 'fa fa-edit', 'link': 'cards/card/:hash/edit', 'button': 'btn btn-fiche edit btn-sm', 'action': 'edit'});
      this.actions.push({'icon': 'fa fa-file-pdf-o', 'button': 'btn btn-fiche pdf btn-sm', 'action': 'pdf'});
      this.actions.push({'icon': 'fa fa-trash', 'link': 'cards/card/:hash/delete', 'button': 'btn btn-fiche delete btn-sm', 'action': 'delete'});
      this.actions.push({'icon': 'fa fa-file', 'link': 'cards/card/:hash/report', 'button': 'btn btn-fiche report btn-sm', 'action': 'report'});
      if (!this.isAdmin()) {
        return;
      }
      this.actions.push({
        'icon': 'fa fa-link',
        'link': 'link-podo-cad/:hash',
        'button': 'btn btn-info btn-sm',
        'action': 'link-podo-cad'
    })
  }

      ngOnInit() {
          if (localStorage.getItem('status')) {
              this.alerts.push({
                  type: localStorage.getItem('status'),
                  message: localStorage.getItem('message')
              });
              localStorage.removeItem('status');
              localStorage.removeItem('message');
          }
      }


    filterCardByStatus() {
        this._dataTableService.paramQuery = { status: this.filterStatus};
        this.includeRequiredHeading(this.filterStatus);
        if (typeof(this.filterStatus) !== 'undefined' && this.filterStatus !== "") {
            this.router.navigate(['/cards'], { queryParams: { status: this.filterStatus } });
        } else {
            this.router.navigate(['/cards'], {});
        }

        if ((this.filterStatus === 3 || this.filterStatus === 6)  && this.heading.indexOf('verified') == -1 && this.userRoles.indexOf('ROLE_ADMIN') !== -1) {
            this.heading.push('verified');
        } else if (this.heading.indexOf('verified') != -1 && !(this.filterStatus === 3 || this.filterStatus === 6)) {
            this.heading.splice(this.heading.indexOf('verified'), 1);
        }
        this._dataTableService.heading = this.heading;
        this._dataTableService.filter('realooadHeading', this.heading);
        if (this.filterStatus === "") {
            this._dataTableService.filter('Reinitialize', {});
            this._modalUndo.filter('Reinitialize', {'status' : null});
        } else {
            this._dataTableService.filter('Reinitialize', {'status' : this.filterStatus});
            this._modalUndo.filter('Reinitialize', {'status' : this.filterStatus});
        }



    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

    public isAuthorized() {
        if (this.filterStatus === 0) {
            return false;
        }
        const roles = this.authentificat.getRoles();
        return this._dataTableService.isAuthorized(roles, this.filterStatus);
    }

    public isAdmin() {
      const roles = this.authentificat.getRoles();
      return roles.includes("ROLE_ADMIN");
    }

    public downloadDesignFiles() {
        this.isDownloading = true;
        this.cardService.downloadDesignFiles().subscribe(response => {
            if(response.ok){
                let file = new Blob([response.body], {type: 'application/pdf'});
                let downloadUrl = URL.createObjectURL(response.body);
                window.open(downloadUrl);
            }
            this.isDownloading = false;
        }, error => {
            console.log('download failed');
            this.isDownloading = false;
        });
    }

    public isStatus(value) {
        let paramStatus = this.activatedRoute.snapshot.queryParamMap.get('status');
        return paramStatus == value;
    }

    public includeRequiredHeading (status) {
      console.log(status)
      this.heading = [
        'id', 'patient', 'podo'
      ];
      if (typeof status === 'undefined' || status === '') {
        this.addInHeadingIfNotExists('dateClosing');
        this.addInHeadingIfNotExists('dateInprocess');
        this.addInHeadingIfNotExists('dateSent');
        this.addInHeadingIfNotExists('dateBilled');
        this.addInHeadingIfNotExists('podoCad');
        this.addInHeadingIfNotExists('shoeModelName')
        return;
      }
      status = status.toString();
      if (['2', '3', '4', '5', '6'].includes(status)) {
          this.addInHeadingIfNotExists('dateClosing');
      }
      if (['3', '4', '5', '6'].includes(status)) {
        this.addInHeadingIfNotExists('dateInprocess');
      }
      if (['4', '5'].includes(status)) {
        this.addInHeadingIfNotExists('dateSent');
      }
      if (['5'].includes(status)) {
        this.addInHeadingIfNotExists('dateBilled');
      }
      if (['2', '3', '4', '5', '6'].includes(status)) {
        this.addInHeadingIfNotExists('podoCad');
      }
      if (['2'].includes(status)) {
        this.addInHeadingIfNotExists('shippingDays');
      }
      this.addInHeadingIfNotExists('shoeModelName');
    }

    private addInHeadingIfNotExists (column) {
      if (this.heading.includes(column)) {
        return;
      }
      this.heading.push(column);
    }
}
