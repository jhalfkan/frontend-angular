import { Component, Input } from '@angular/core';
import { UpdateStatusModalService } from '../../../../shared/services/UpdateStatusModalService';
import { CardService } from '../../../../_repositories';
import { DatatableService } from '../../../../shared/services/DatatableService'
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../../_services/authentication.service';

@Component({
    selector: 'app-update-status-modal',
    templateUrl: './update-status-modal.component.html',
    styleUrls: ['./update-status-modal.component.scss']
})
export class UpdateStatusModalComponent {
    @Input() status;
    isNextStatusLoading = false;
    isChangeStatusLoading = false;
    constructor(
        public _updateStatusModalService: UpdateStatusModalService,
        private cardService: CardService,
        private _dataTableService: DatatableService,
        private auth: AuthenticationService
    ) {
    }

    public closeModal () {
        this._updateStatusModalService.shouldDisplay = false;
        this._updateStatusModalService.card = null;
    }

    public changeStatus () {
        let card = this._updateStatusModalService.card;
        this.isChangeStatusLoading = true;
        this.cardService.changeStatus(card.hash, card.status_id).pipe(first())
        .subscribe(data => {
            this._dataTableService.loadDatas();
            this.isChangeStatusLoading = false;
            this._updateStatusModalService.shouldDisplay = false;
        },
        error => {
            this.isChangeStatusLoading = false;
        });
    }

    public nextStatus () {
        let card = this._updateStatusModalService.card;
        if (this.isNextStatusLoading) {
            return;
        }
        this.isNextStatusLoading = true;
        this.cardService.nextStepForStatus(card.hash).pipe(first())
        .subscribe(data => {
            this._dataTableService.loadDatas();
            this.isNextStatusLoading = false;
            this._updateStatusModalService.shouldDisplay = false;
        },
        error => {
            this.isNextStatusLoading = false;
        });
    }

    public isAdmin() {
        const roles = this.auth.getRoles();
        return roles.includes('ROLE_ADMIN');
    }

}
