import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, FormsModule, ReactiveFormsModule],
    declarations: []
})
export class UpdateStatusModalModule {}
