import { UpdateStatusModalModule } from './update-status-modal.module';

describe('CardsModule', () => {
    let updateStatusModalModule: UpdateStatusModalModule;

    beforeEach(() => {
        updateStatusModalModule = new UpdateStatusModalModule();
    });

    it('should create an instance', () => {
        expect(updateStatusModalModule).toBeTruthy();
    });
});
