import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardsRoutingModule } from './cards-routing.module';
import { CardsComponent } from './cards.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalComponent} from './modal/modal.component';
import { UpdateStatusModalComponent } from './update-status-modal/update-status-modal.component'
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, CardsRoutingModule, PageHeaderModule, DatatableModule, FormsModule, ReactiveFormsModule, NgbAlertModule],
    declarations: [CardsComponent, ModalComponent, UpdateStatusModalComponent]
})
export class CardsModule {}
