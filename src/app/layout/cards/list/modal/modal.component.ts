import {Component} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {ModalUndoService} from '../../../../_services/modal/modalUndoService';
import {ActivatedRoute} from '@angular/router';
import {CardService} from '../../../../_repositories';
import {DatatableService} from '../../../../shared/services/DatatableService';
import * as $ from 'jquery';


@Component({
    selector: 'app-modal-undo',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
    closeResult: string;
    paramQuery: any;    // parameter from url
    listDataUndo: Array<object> = new Array<object>();

    constructor(private modalService: NgbModal, private _modalUndoService: ModalUndoService, private activatedRoute: ActivatedRoute, private cardService: CardService, private _dataTableService: DatatableService) {
        this.activatedRoute.queryParams.pipe().subscribe(params => {
            this.paramQuery = params;
        });

        this.refresh(this.paramQuery.status);

        this._modalUndoService.listen().subscribe((m: any) => {
            if (m.key === 'Reinitialize') {
                this.reload(m.key, m.value);
            }
        });
    }

    open(content) {
        this.modalService.open(content).result.then((result) => {
            this._dataTableService.filter('Reinitialize', {'status' : this.paramQuery.status});
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this._dataTableService.filter('Reinitialize', {'status' : this.paramQuery.status});
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    reload(event, value) {
        this.paramQuery = value;
        this.refresh(this.paramQuery.status);

    }

    refresh(status) {
        if (typeof(status) !== 'undefined' && status !== null) {
            this.cardService.getUndoList(status).subscribe(value => {
                if (typeof(value.datas) !== 'undefined') {
                    this.listDataUndo = value.datas;
                } else {
                    this.listDataUndo = [];
                }
            }, error => {
                console.log(error);
            });
        } else {
            this.listDataUndo = [];
        }

    }

    previewUndo(hash) {
        document.body.style.cursor = 'progress';
        $('.btn').css('cursor', 'progress');

        this.cardService.previousStepForStatus(hash).subscribe(value => {
            if (typeof(value.result) !== 'undefined' && value.result) {
                this.refresh(this.paramQuery.status);
            }
            this._dataTableService.loadDatas();
            document.body.style.cursor = 'default';
            $('.btn').css('cursor', 'pointer');
        }, error => {
            document.body.style.cursor = 'default';
            $('.btn').css('cursor', 'pointer');
        });
    }
}
