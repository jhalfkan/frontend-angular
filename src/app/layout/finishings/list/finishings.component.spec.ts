import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishingsComponent } from './finishings.component';

describe('FinishingComponent', () => {
  let component: FinishingsComponent;
  let fixture: ComponentFixture<FinishingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
