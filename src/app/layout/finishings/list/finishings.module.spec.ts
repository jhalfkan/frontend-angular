import { FinishingsModule } from './finishings.module';

describe('FinishingsModule', () => {
    let finishingsModule: FinishingsModule;

    beforeEach(() => {
        finishingsModule = new FinishingsModule();
    });

    it('should create an instance', () => {
        expect(finishingsModule).toBeTruthy();
    });
});
