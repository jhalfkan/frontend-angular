import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinishingsComponent } from './finishings.component';

const routes: Routes = [
    {
        path: '',
        component: FinishingsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinishingsRoutingModule {}
