import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FinishingsRoutingModule } from './finishings-routing.module';
import { FinishingsComponent } from './finishings.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';
import {NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, FinishingsRoutingModule, PageHeaderModule, DatatableModule, NgbAlertModule],
    declarations: [FinishingsComponent]
})
export class FinishingsModule {}
