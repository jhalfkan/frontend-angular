import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishingManageComponent } from './finishing-manage.component';

describe('SportManageComponent', () => {
  let component: FinishingManageComponent;
  let fixture: ComponentFixture<FinishingManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishingManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishingManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
