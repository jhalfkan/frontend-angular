import { FinishingsManageModule } from './finishing-manage.module';

describe('SportManageModule', () => {
  let finishingsManageModule: FinishingsManageModule;

  beforeEach(() => {
    finishingsManageModule = new FinishingsManageModule();
  });

  it('should create an instance', () => {
    expect(finishingsManageModule).toBeTruthy();
  });
});
