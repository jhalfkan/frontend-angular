import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinishingManageComponent } from './finishing-manage.component';

const routes: Routes = [
    {
        path: '',
        component: FinishingManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinishingManageRoutingModule {}
