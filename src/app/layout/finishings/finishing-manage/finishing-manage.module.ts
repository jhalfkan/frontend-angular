import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FinishingManageRoutingModule } from './finishing-manage-routing.module';
import { FinishingManageComponent } from './finishing-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, FinishingManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [FinishingManageComponent]
})
export class FinishingManageModule { }





