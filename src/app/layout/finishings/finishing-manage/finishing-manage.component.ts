import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FinishingService} from '../../../_repositories';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-finishing-manage',
    templateUrl: './finishing-manage.component.html',
    styleUrls: ['./finishing-manage.component.scss']
})
export class FinishingManageComponent implements OnInit {
    translate;
    baseURl;
    finishingService;
    shoeModel;
    router;
    activatedRoute;
    id;
    finishing;

    constructor(translate: TranslateService, finishingService: FinishingService, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.finishingService = finishingService;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.id = id;
        });
        if (typeof(this.id) !== 'undefined') {
            this.finishingService.get(this.id).subscribe((response) => {
                this.finishing = response;
            });
        }
    }

    navigateBack() {
        this.router.navigate(['/finishings']);
    }

    edit() {
        this.finishingService.edit(this.finishing).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item edited with success'));
            this.router.navigate(['/finishings']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
            this.router.navigate(['/finishings']);
        });
    }
}
