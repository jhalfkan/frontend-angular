import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FlangesColorService} from '../../../_repositories';

@Component({
    selector: 'app-messages',
    templateUrl: './flangesColors.component.html',
    styleUrls: ['./flangesColors.component.scss']
})
export class FlangesColorsComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;
    public alerts: Array<any> = [];

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = FlangesColorService.BASEURL + '/';
        this.heading = new Array('id', 'name','activated');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'flangesColors/flangesColor/:id/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'flangesColors/flangesColor/:id/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
        if (localStorage.getItem('status')) {
            this.alerts.push({
                type: localStorage.getItem('status'),
                message: localStorage.getItem('message')
            });
            localStorage.removeItem('status');
            localStorage.removeItem('message');
        }
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }

}
