import { FlangesColorsModule } from './flangesColors.module';

describe('FlangesColorsModule', () => {
    let flangesColorModule: FlangesColorsModule;

    beforeEach(() => {
        flangesColorModule = new FlangesColorsModule();
    });

    it('should create an instance', () => {
        expect(flangesColorModule).toBeTruthy();
    });
});
