import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlangesColorsComponent } from './flangesColors.component';

const routes: Routes = [
    {
        path: '',
        component: FlangesColorsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FlangesColorsRoutingModule {}
