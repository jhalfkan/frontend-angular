import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlangesColorsComponent } from './flangesColors.component';

describe('FlangesColorComponent', () => {
  let component: FlangesColorsComponent;
  let fixture: ComponentFixture<FlangesColorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlangesColorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlangesColorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
