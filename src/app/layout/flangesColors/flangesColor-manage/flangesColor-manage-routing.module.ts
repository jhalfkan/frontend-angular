import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlangesColorManageComponent } from './flangesColor-manage.component';

const routes: Routes = [
    {
        path: '',
        component: FlangesColorManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FlangesColorManageRoutingModule {}
