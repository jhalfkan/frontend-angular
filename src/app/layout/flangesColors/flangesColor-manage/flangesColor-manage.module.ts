import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { FlangesColorManageRoutingModule } from './flangesColor-manage-routing.module';
import { FlangesColorManageComponent } from './flangesColor-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UiSwitchModule} from 'ngx-ui-switch';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, FlangesColorManageRoutingModule, ReactiveFormsModule, FormsModule, UiSwitchModule.forRoot({
          size: 'medium',
          checkedLabel: 'oui',
          uncheckedLabel: 'non'
      })
  ],
  declarations: [FlangesColorManageComponent]
})
export class FlangesColorManageModule { }





