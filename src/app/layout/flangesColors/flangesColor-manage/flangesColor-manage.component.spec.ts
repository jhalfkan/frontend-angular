import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlangesColorManageComponent } from './flangesColor-manage.component';

describe('FlangesColorManageComponent', () => {
  let component: FlangesColorManageComponent;
  let fixture: ComponentFixture<FlangesColorManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlangesColorManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlangesColorManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
