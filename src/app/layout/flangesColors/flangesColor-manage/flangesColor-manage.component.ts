import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FlangesColorService} from '../../../_repositories';
import {FlangesColor} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-flangesColor-manage',
    templateUrl: './flangesColor-manage.component.html',
    styleUrls: ['./flangesColor-manage.component.scss'],
    providers: [FlangesColor]
})
export class FlangesColorManageComponent implements OnInit {
    translate;
    baseURl;
    flangesColorService;
    flangesColorModel;
    router;
    activatedRoute;
    status: string;
    deleteAction;
    idFlangesColor;
    flangesColor = new FlangesColor();
    nbreItem;
    count;

    constructor(translate: TranslateService, flangesColor: FlangesColorService, flangesColorModel: FlangesColor, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.flangesColorService = flangesColor;
        this.flangesColorModel = flangesColorModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idFlangesColor = id;
        });
        if (this.router.url.indexOf('delete') !== -1) {
            this.deleteAction = true;
        }
        if (this.idFlangesColor !== undefined) {
            this.flangesColorService.getFlangesColor(this.idFlangesColor).subscribe((response) => {
                this.flangesColor = response;
            });
        }
    }

    navigateBack() {
        this.router.navigate(['/flangesColors']);
    }

    gestionFlangesColor(flangesColor) {
        if (typeof(flangesColor.id) === 'undefined') {
            this.flangesColorService.createFlangesColor(flangesColor).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item created with success'));
                this.router.navigate(['/flangesColors']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not created because an error'));
                this.router.navigate(['/flangesColors']);
            });
        } else {
            this.flangesColorService.editFlangesColor(flangesColor).subscribe((response) => {
                localStorage.setItem('status', 'success');
                localStorage.setItem('message', this.translate.instant('Item edited with success'));
                this.router.navigate(['/flangesColors']);
            }, () => {
                localStorage.setItem('status', 'danger');
                localStorage.setItem('message', this.translate.instant('ERROR: Item not edited because an error'));
                this.router.navigate(['/flangesColors']);
            });
        }
    }

    delete() {
        this.flangesColorService.deleteFlangesColor(this.flangesColor).subscribe((response) => {
            localStorage.setItem('status', 'success');
            localStorage.setItem('message', this.translate.instant('Item deleted with success'));
            this.router.navigate(['/flangesColors']);
        }, () => {
            localStorage.setItem('status', 'danger');
            localStorage.setItem('message', this.translate.instant('ERROR: Item not deleted because an error'));
            this.router.navigate(['/flangesColors']);
        });
    }
}
