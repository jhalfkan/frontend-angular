import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_repositories/user.service';
import { BalanceHistoryService } from '../../_repositories/balanceHistory.service';
import { Router } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-manage-pods',
  templateUrl: './manage-pods.component.html',
  styleUrls: ['./manage-pods.component.scss']
})
export class ManagePodsComponent implements OnInit {
  public podiatrists = [];
  public selectedPodiatristHash;
  public amount: number;
  public isAmountNegative: boolean = false;
  public bill: string;
  public other: string;
  public errorMessage: string = null;
  public isLoading: boolean = true;
  public isSubmitLoading: boolean = false;

  constructor(
    private userService: UserService,
    private balanceHistoryService: BalanceHistoryService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.fetchPodiatrists();
    this.activatedRoute.params.subscribe(params => {
      if (params['hash']) {
        this.selectedPodiatristHash = params['hash'];
      }
    });
  }

  public fetchPodiatrists() {
    this.isLoading = true;
    return this.userService.getAll().subscribe(response => {
      this.podiatrists = response.datas;
      this.podiatrists.map(podiatrist => podiatrist.name = podiatrist.firstname + ' ' + podiatrist.lastname);
      this.isLoading = false;
    })
  }

  public validate() {
    const balanceHistoryLine = {
      podiatristHash: this.selectedPodiatristHash,
      amount: this.retrieveAmount(),
      bill: this.bill,
      other: this.other
    }
    this.isSubmitLoading = true;
    this.balanceHistoryService.insertBalanceHistory(balanceHistoryLine).subscribe(
      () => {
        this.isSubmitLoading = false;
        this.navigate();
      },
      error => {
        this.isSubmitLoading = false;
        this.errorMessage = "error server"
        if (error.error.error === "The account has unsufficient funds") {
          this.errorMessage = "unsufficient_funds"
        }
      }
    )
  }

  public navigate() {
    this.router.navigate(['/all-users-balance-history']);
  }

  public retrieveAmount () {
    if (this.isAmountNegative) {
      return -this.amount;
    }
    return this.amount;
  }

}
