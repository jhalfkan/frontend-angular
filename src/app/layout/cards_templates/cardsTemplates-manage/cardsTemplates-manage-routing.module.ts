import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsTemplatesManageComponent } from './cardsTemplates-manage.component';

const routes: Routes = [
    {
        path: '',
        component: CardsTemplatesManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CardsTemplatesManageRoutingModule {}
