import { CardsTemplatesManageModule } from './cardsTemplates-manage.module';

describe('CardsTemplatesManageModule', () => {
  let cardsTemplatesManageModule: CardsTemplatesManageModule;

  beforeEach(() => {
    cardsTemplatesManageModule = new CardsTemplatesManageModule();
  });

  it('should create an instance', () => {
    expect(cardsTemplatesManageModule).toBeTruthy();
  });
});
