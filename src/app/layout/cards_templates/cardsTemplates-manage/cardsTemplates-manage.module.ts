import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardsTemplatesManageRoutingModule } from './cardsTemplates-manage-routing.module';
import { CardsTemplatesManageComponent } from './cardsTemplates-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, CardsTemplatesManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [CardsTemplatesManageComponent]
})
export class CardsTemplatesManageModule { }





