import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CardsTemplatesService} from '../../../_repositories';
import {CardsTemplates} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-cards-templates-manage',
    templateUrl: './cardsTemplates-manage.component.html',
    styleUrls: ['./cardsTemplates-manage.component.scss'],
    providers: [CardsTemplates]
})
export class CardsTemplatesManageComponent implements OnInit {
    translate;
    baseURl;
    cardsTemplatesService;
    cardsTemplatesModel;
    router;
    activatedRoute;
    status: string;
    idCardsTemplates;
    nameCardsTemplates;

    constructor(translate: TranslateService, cardsTemplates: CardsTemplatesService, cardsTemplatesModel: CardsTemplates, routeur: Router, activatedRoute: ActivatedRoute) {
        this.translate = translate;
        this.cardsTemplatesService = cardsTemplates;
        this.cardsTemplatesModel = cardsTemplatesModel;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            this.idCardsTemplates = id;
        });
        if (this.idCardsTemplates !== undefined) {
            this.cardsTemplatesService.getCardsTemplates(this.idCardsTemplates).subscribe((response) => {
                this.nameCardsTemplates = response.name;
            });
        }
    }

    gestionCardsTemplates(id) {
        if (id === undefined) {
            let cardsTemplates = {name: this.cardsTemplatesModel.name};
            this.cardsTemplatesService.createCardsTemplates(cardsTemplates).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/cards/templates']);
            });
        } else {
            let cardsTemplates = {id: id, name: this.cardsTemplatesModel.name};
            console.log(cardsTemplates);
            this.cardsTemplatesService.editCardsTemplates(cardsTemplates).subscribe((response) => {
                console.log(response.code);
                this.status = 'success';
                this.router.navigate(['/cards/templates']);
            });
        }
    }
}
