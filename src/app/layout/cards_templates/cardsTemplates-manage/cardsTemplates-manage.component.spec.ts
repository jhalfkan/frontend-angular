import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsTemplatesManageComponent } from './cardsTemplates-manage.component';

describe('CardsTemplatesManageComponent', () => {
  let component: CardsTemplatesManageComponent;
  let fixture: ComponentFixture<CardsTemplatesManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsTemplatesManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsTemplatesManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
