import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { CardsTemplatesRoutingModule } from './cardsTemplates-routing.module';
import { CardsTemplatesComponent } from './cardsTemplates.component';
import { PageHeaderModule } from '../../../shared/index';
import {DatatableModule} from '../../../shared/modules';

@NgModule({
    imports: [CommonModule, Ng2Charts, TranslateModule, CardsTemplatesRoutingModule, PageHeaderModule, DatatableModule],
    declarations: [CardsTemplatesComponent]
})
export class CardsTemplatesModule {}
