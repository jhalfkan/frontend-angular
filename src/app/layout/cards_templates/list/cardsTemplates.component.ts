import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {CardsTemplatesService} from '../../../_repositories';

@Component({
    selector: 'app-cardsTemplates',
    templateUrl: './cardsTemplates.component.html',
    styleUrls: ['./cardsTemplates.component.scss']
})
export class CardsTemplatesComponent implements OnInit {
    translate;
    baseURl;
    actions;
    heading;

    constructor(translate: TranslateService) {
        this.translate = translate;
        this.baseURl = CardsTemplatesService.BASEURL + '/';
        this.heading = new Array('id', 'nameTemplate', 'public');
        this.actions = new Array();

        this.actions.push({'icon': 'fa fa-edit', 'link': 'cards/templates/template/:hash/edit', 'button': 'btn btn-info btn-sm', 'action': 'edit'});
        this.actions.push({'icon': 'fa fa-trash', 'link': 'cards/templates/template/:hash/delete', 'button': 'btn btn-danger btn-sm', 'action': 'delete'});

    }

    ngOnInit() {
    }

}
