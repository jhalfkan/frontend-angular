import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsTemplatesComponent } from './cardsTemplates.component';

describe('CardsTemplatesComponent', () => {
  let component: CardsTemplatesComponent;
  let fixture: ComponentFixture<CardsTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
