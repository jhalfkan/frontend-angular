import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsTemplatesComponent } from './cardsTemplates.component';

const routes: Routes = [
    {
        path: '',
        component: CardsTemplatesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CardsTemplatesRoutingModule {}
