import { CardsTemplatesModule } from './cardsTemplates.module';

describe('CardsTemplatesModule', () => {
    let cardsTemplatesModule: CardsTemplatesModule;

    beforeEach(() => {
        cardsTemplatesModule = new CardsTemplatesModule();
    });

    it('should create an instance', () => {
        expect(cardsTemplatesModule).toBeTruthy();
    });
});
