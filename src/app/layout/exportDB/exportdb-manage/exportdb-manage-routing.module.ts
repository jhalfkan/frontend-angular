import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExportDBManageComponent } from './exportdb-manage.component';

const routes: Routes = [
    {
        path: '',
        component: ExportDBManageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExportDBManageRoutingModule {}
