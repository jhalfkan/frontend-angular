import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import {TranslateModule} from '@ngx-translate/core';

import { ExportDBManageRoutingModule } from './exportdb-manage-routing.module';
import { ExportDBManageComponent } from './exportdb-manage.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, Ng2Charts, TranslateModule, ExportDBManageRoutingModule, ReactiveFormsModule, FormsModule
  ],
  declarations: [ExportDBManageComponent]
})
export class ExportDBManageModule { }





