import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ExportDBService} from '../../../_repositories';
import {ExportDB} from '../../../_models';
import {NgForm} from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as $ from 'jquery';

@Component({
    selector: 'app-exportdb-manage',
    templateUrl: './exportdb-manage.component.html',
    styleUrls: ['./exportdb-manage.component.scss'],
    providers: [ExportDB]
})
export class ExportDBManageComponent implements OnInit {
    translate;
    baseURl;
    router;
    activatedRoute;
    status: string;
    selectValue;

    items: any[] = [
                {id: '1', name: 'export complet with users and cards'},
                {id: '2', name: 'List cards'},
                {id: '3', name: 'List users'},
                {id: '4', name: 'List sandales'},
                {id: '5', name: 'List reports'},
                {id: '6', name: 'List templates'},
                {id: '7', name: 'List messages'},
                {id: '8', name: 'List materiaux'},
                {id: '9', name: 'List pathologie'},
                {id: '10', name: 'List recouvrement'},
                {id: '11', name: 'List sports'},
                {id: '12', name: 'List gabarits'},
                {id: '13', name: 'List users demo'},
                {id: '14', name: 'Billing'},
                {id: '15', name: 'Stats'},
                {id: '16', name: 'pods_history'},
                {id: '17', name: 'pods_accounts'},
            ];

    constructor(translate: TranslateService, private exportdb: ExportDBService, routeur: Router, activatedRoute: ActivatedRoute) {
        this.selectValue  = '1';
        this.translate = translate;
        this.router = routeur;
        this.activatedRoute = activatedRoute;
    }

    ngOnInit() {

    }

    public downloadXls() {
        document.body.style.cursor = 'progress';
        $('.downloadFile').css('cursor', 'progress');

        console.log(this.selectValue);
       this.exportdb.getAll(this.selectValue).subscribe(response => {
           console.log(response);
           if(response.ok){
              // var file = new Blob([response.body], {type: 'application/vnd.ms-excel'});
              // let downloadUrl = URL.createObjectURL(response.body);
              // window.open(downloadUrl);


               // For other browsers:
               // Create a link pointing to the ObjectURL containing the blob.
               const data = window.URL.createObjectURL(response.body);
               var link = document.createElement('a');
               link.href = data;
               link.target = "_blank";

               this.translate.get(this.items[this.selectValue -1].name).subscribe((translated: string) => {
                   link.download = 'exportDB_' + translated.replace(' ','_') + '.xls';
                   document.body.appendChild(link);
                   link.click();
                   setTimeout(function() {
                       // For Firefox it is necessary to delay revoking the ObjectURL
                       document.body.removeChild(link);
                       window.URL.revokeObjectURL(data);

                       document.body.style.cursor = 'default';
                       $('.downloadFile').css('cursor', 'pointer');
                   },  100);
               });





               document.body.style.cursor = 'default';
               $('.downloadFile').css('cursor', 'pointer');
           }

       }, error => {
           console.log(error);
           document.body.style.cursor = 'default';
           $('.downloadFile').css('cursor', 'pointer');
       });
    }
}
