import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportDBManageComponent } from './exportdb-manage.component';

describe('ExportDBManageComponent', () => {
  let component: ExportDBManageComponent;
  let fixture: ComponentFixture<ExportDBManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportDBManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportDBManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
