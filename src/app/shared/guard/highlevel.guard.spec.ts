import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {HighlevelGuard} from './highlevel.guard';

describe('HighlevelGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ RouterTestingModule ],
            providers: [HighlevelGuard]
        });
    });

    it('should ...', inject([HighlevelGuard], (guard: HighlevelGuard) => {
        expect(guard).toBeTruthy();
    }));
});
