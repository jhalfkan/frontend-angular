import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {CheckConnectionGuard} from './checkConnection.guard';

describe('CheckConnectionGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ RouterTestingModule ],
            providers: [CheckConnectionGuard]
        });
    });

    it('should ...', inject([CheckConnectionGuard], (guard: CheckConnectionGuard) => {
        expect(guard).toBeTruthy();
    }));
});
