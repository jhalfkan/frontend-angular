import { Component, OnInit, Input } from '@angular/core';
import { RouterModule } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
    @Input() heading: string;
    @Input() icon: string;
    @Input() link_new: string;
    translate;

    constructor(translate: TranslateService) {
        this.translate = translate;
    }

    ngOnInit() {}
}
