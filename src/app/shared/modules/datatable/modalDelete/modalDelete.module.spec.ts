import { ModalDeleteModule } from './modalDelete.module';

describe('CardsModule', () => {
    let modalModule: ModalDeleteModule;

    beforeEach(() => {
        modalModule = new ModalDeleteModule();
    });

    it('should create an instance', () => {
        expect(modalModule).toBeTruthy();
    });
});
