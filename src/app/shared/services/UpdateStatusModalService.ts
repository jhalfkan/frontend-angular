import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UpdateStatusModalService {
    public shouldDisplay = false;
    public card = null;
    constructor() {
    }
}
