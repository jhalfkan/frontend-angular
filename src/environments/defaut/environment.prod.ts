export const environment = {
    production: true,
    URLAPI: 'https://api.gespodo.com/api',
    DEMO: 'https://api.gespodo.com/demo',
    DEMO_DOWNLOAD: 'https://api.gespodo.com/demo/user/files/download',
    CHECK_TOKEN: true,
    PLAYSTORE_APP_URL: 'https://play.google.com/store/apps/details?id=com.gespodo',
    REDIRECTION_SUBSCRIBE: 'https://www.gespodo.com/contact',
    URL_CONDITION_GENERAL: 'https://www.gespodo.com/fr/CGU_FOOTSCAN3D'
};
