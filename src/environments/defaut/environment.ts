// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    URLAPI: 'http://api.gespodo.localhost.test/app_dev.php/api',
    DEMO: 'http://api.gespodo.localhost.test/app_dev.php/demo',
    DEMO_DOWNLOAD: 'http://api.gespodo.localhost.test/app_dev.php/demo/user/files/download',
    CHECK_TOKEN: false,
    PLAYSTORE_APP_URL: 'https://play.google.com/store/apps/details?id=com.gespodo',
    REDIRECTION_SUBSCRIBE: 'https://www.gespodo.com/contact',
    URL_CONDITION_GENERAL: 'https://www.gespodo.com/fr/CGU_FOOTSCAN3D'
};