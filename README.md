# MISE EN PROD
ng build --configuration=production
rm -rf public && cp -R dist public

# MISE EN STAGING
ng build --configuration=staging
rm -rf public && cp -R dist public

# angular.gespodo.com
Angular gespodo

### version de nodeJS
nodeJS 8.11.3
Angular CLI: 6.0.0

### Installation

Voici les commandes pour installer le projet

```bash
$ git clone https://github.com/webdigit/angular.gespodo.com.git
$ cd angular.gespodo.com
$ npm install
$ npm start
```
### construire le dossier dist
ng build

BUILD FOR STAGING (voir angular.json)
ng build --configuration=staging

BUILD FOR PROD (voir angular.json)
ng build --configuration=production

### Code scaffolding
Run ng generate component component-name to generate a new component. You can also use ng generate directive/pipe/service/class/module.

### Running unit tests
Run ng test to execute the unit tests via Karma.

### Running end-to-end tests
Run ng e2e to execute the end-to-end tests via Protractor. Before running the tests make sure you are serving the app via ng serve.

### Runniong prod
ng build --prod --optimization=false